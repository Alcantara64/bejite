/**
 * Created by mac on 6/13/16.
 */

var Utils = {

    notify: function (msg,title) {
        var modal = $('#notify'),header = "";
        if(typeof title == "undefined"){
            header = "Notification"
        }else{
            header = title;
        }
        modal.modal('show');
        $('.modal-title',modal).html(header);
        $('.modal-body',modal).html(msg);
    },
    showAjaxStatus : function (msg) {
        $('li a.ajax-loader span.ajax-status').html(msg);
        $('li a.ajax-loader').fadeIn();
    },
    hideAjaxStatus : function () {
        $('li a.ajax-loader').fadeOut();
    }
};
