/**
 * Created by mac on 6/7/16.
 */
$(function () {

    $('#site-settings').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Saving Settings... Please Wait...");
        },
        success : function (data) {
            if(data.status == 'success'){
                if(data.img !='')
                {
                    $('.site-logo').attr('src','/uploads/logo/'+data.img);
                }
                Utils.hideAjaxStatus();
                Utils.notify(data.feedback,"Settings saved");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            Utils.notify(err,"Fix the following errors and continue");
        }
    });
    $('#add-page').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Saving Page... Please Wait...");
        },
        success : function (data) {
            if(data.status == 'success'){
                $('#list').tab('show');
                refreshAdminPageLists();
                Utils.hideAjaxStatus();
                Utils.notify(data.feedback,"Page Saved");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            Utils.notify(err,"Fix the following errors and continue");
        },
        resetForm : true
    });

    $('#update-page').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Updating Page... Please Wait...");
        },
        success : function (data) {
            if(data.status == 'success'){
                $('#list').tab('show');
                refreshAdminPageLists();
                Utils.hideAjaxStatus();
                Utils.notify(data.feedback,"Page Updated");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            Utils.notify(err,"Fix the following errors and continue");
        }
    });
    $('#add-slider').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Saving Slider... Please Wait...");
        },
        success : function (data) {
            if(data.status == 'success'){
                $('#list').tab('show');
                Utils.hideAjaxStatus();
                refreshAdminSliderLists();
                Utils.notify(data.feedback,"Slider Saved");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors and continue");
        },
        resetForm : true
    });
    $('#update-slider').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Updating Slider... Please Wait...");
        },
        success : function (data) {
            if(data.status == 'success'){
                $('#list').tab('show');
                refreshAdminSliderLists();
                Utils.hideAjaxStatus();
                Utils.notify(data.feedback,"Slider Updated");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors and continue");
        }
    });
    $('#admin-login').ajaxForm({
        beforeSubmit : function () {
            $('#login-error').html("");
            $('button','#admin-login')
                .attr('disabled','disabled')
                .css('font-size','14px')
                .html('<span class="fa fa-spin fa-spinner"></span> Attempting login... Wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                window.location.href = '/admin';
            }else{
                $('#login-error').html('<div class="alert alert-danger">'+data.feedback+'</div>');
                $('button','#admin-login')
                    .removeAttr('disabled')
                    .css('font-size','15px')
                    .html('Sign In');
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            $('#login-error').html('<div class="alert alert-danger">'+err+'</div>');
            $('button','#admin-login')
                .removeAttr('disabled')
                .css('font-size','15px')
                .html('Sign In');
        }
    });
    $('#change-photo-btn').on('click',function () {
        document.getElementById('photo-holder').click();
    });
    $('#photo-holder').on('change',function () {
        $('#change-photo').ajaxSubmit({
            beforeSubmit : function () {
                $('button','#change-photo')
                    .attr('disabled','disabled')
                    .html('<span class="fa fa-spin fa-spinner"></span> Uploading photo...');
            },
            success : function (data) {
                if(data.status == 'success'){
                    $('.profile-photo').attr('src','/users/'+data.img);
                    $('button','#change-photo')
                        .removeAttr('disabled')
                        .html('<span class="glyphicon glyphicon-cloud-upload"></span> Upload Photo');
                }
            },
            error : function (data) {
                var $errors = $.parseJSON(data.responseText),err = '';
                $.each($errors,function (index,error) {
                    err += '<div>'+index+' : '+error;
                });
                Utils.notify(err,"Fix the following errors");
            }
        });
    });
    $('#profile-settings').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus();
            $('button','#profile-settings')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving... Please Wait...');
        },
        success : function (data) {
            if(data.status == 'success') {
                $('button', '#profile-settings')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            }
            Utils.hideAjaxStatus();
            Utils.notify("Profile data was updated successfully","Profile Data Updated")
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            $('button', '#profile-settings')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors");
        }
    });
    $('#admin-change-password').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Changing Password, Please Wait...");
            $('button','#admin-change-password')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Please Wait...');
        },
        success : function (data) {
            if(data.status == 'success') {
                $('button','#admin-change-password')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Change Password');
                Utils.hideAjaxStatus();
                Utils.notify("Password was changed successfully","Password Changed")
            }else{
                $('button','#admin-change-password')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Change Password');
                Utils.hideAjaxStatus();
                Utils.notify("Your Password is incorrect. Please try again","Password Password")
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            $('button', '#admin-change-password')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Change Password');
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors");
        }
    });
    $('#add-post').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus("Creating Post, Please Wait...");
            $('button.post','#add-post')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Please Wait...');
        },
        success : function (data) {
            if(data.status == 'success') {
                refreshPosts();
                $('button.post','#add-post')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-plus"></span> Create');
                Utils.hideAjaxStatus();
                Utils.notify(data.feedback,"Post Created")
            }else{
                $('button.post','#add-post')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-plus"></span> Create');
                Utils.hideAjaxStatus();
                Utils.notify("Ooops Something went wrong, Please try again","Ooops");
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            $('button', '#admin-change-password')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-plus"></span> Create');
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors");
        },
        resetForm : true
    });
    $('#edit-post').ajaxForm({
        beforeSubmit : function () {
            Utils.showAjaxStatus();
            $('button.custom-button','#edit-post')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving... Please Wait...');
        },
        success : function (data) {
            if(data.status == 'success') {
                $('button.custom-button', '#edit-post')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            }
            $('#list').tab('show');
            refreshPosts();
            Utils.hideAjaxStatus();
            Utils.notify("Post data was updated successfully","Post Data Updated")
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err = '';
            $.each($errors,function (index,error) {
                err += '<div>'+index+' : '+error;
            });
            $('button.custom-button', '#edit-post')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            Utils.hideAjaxStatus();
            Utils.notify(err,"Fix the following errors");
        }
    });
    var refreshAdminPageLists = function () {
        Utils.showAjaxStatus("Loading Pages... Please wait...");
        $.getJSON('/admin/pages/ajax/list-pages',function (data) {
            var $template = '',i = 0;
            $.each(data.pages,function (index,page) {
                var status = '<td><span class="label label-success">'+page.status+'</span></td>';
                if(page.status == 'draft'){
                    status = '<td><span class="label label-danger">'+page.status+'</span></td>'
                }
                i++;
                $template += '<tr id="row-'+page.id+'">' +
                    '<td>'+i+'</td>' +
                    '<td>'+page.title+'</td>' +
                    '<td><span class="label label-primary">'+page.title+'</span></td>' +
                    '<td>'+page.page_order+'</td>' +
                    '<td><span class="label label-primary">'+page.position+'</span></td>' +
                    status +
                    '<td>' +
                    '<button type="button" class="btn btn-primary btn-xs" onclick="window.location=\''+'/admin/pages/'+page.id+'/edit'+'\'"><span class="glyphicon glyphicon-pencil"></span> Edit</button> ' +
                    '<button type="button" class="btn btn-danger btn-xs" onclick="javascript:deletePage('+page.id+');"><span class="glyphicon glyphicon-trash"></span> Delete</button>' +
                    '</td>' +
                    '</tr>';
            });
            $('#pages-container').html($template);
            $('#list').tab('show');
            Utils.hideAjaxStatus();
        });
    }, refreshAdminSliderLists = function () {
        Utils.showAjaxStatus("Loading Sliders...");
        $.getJSON('/admin/sliders/ajax/list',function (data) {
            var $template = '', i = 0;
            $.each(data.sliders,function (index,slider) {
                var status = '<td><span class="label label-success">'+slider.status+'</span></td>';
                if(slider.status == 'draft'){
                    status = '<td><span class="label label-danger">'+slider.status+'</span></td>'
                }
                i++;
                $template += '<tr id="row-'+slider.id+'">' +
                    '<td>'+i+'</td>' +
                    '<td><img class="carousel-preview" src="/sliders/'+slider.img+'"></td>' +
                    '<td>'+slider.heading+'</td>' +
                    '<td>'+slider.text+'</td>' +
                    '<td><span class="label label-primary">'+slider.display_order+'</span></td>' +
                    status +
                    '<td>' +
                    '<button type="button" class="btn btn-primary btn-xs" onclick="window.location=\''+'/admin/sliders/'+slider.id+'/edit'+'\'"><span class="glyphicon glyphicon-pencil"></span> Edit</button> ' +
                    '<button type="button" class="btn btn-danger btn-xs" onclick="javascript:deleteSlider('+slider.id+');"><span class="glyphicon glyphicon-trash"></span> Delete</button>' +
                    '</td>' +
                    '</tr>';
            });
            $('#sliders-container').html($template);
            Utils.hideAjaxStatus();
        });
    },refreshPosts = function () {
        Utils.showAjaxStatus("Refreshing Posts...");
        $.getJSON('/admin/blog/ajax/posts',function (data) {
            var $template = '', i = 0;
            $.each(data.posts,function (index,post) {
                var status = '<td><span class="label label-success">'+post.status+'</span></td>';
                if(post.status == 'draft'){
                    status = '<td><span class="label label-danger">'+post.status+'</span></td>'
                }
                i++;
                $template += '<tr id="row-'+post.id+'">' +
                    '<td>'+i+'</td>' +
                    '<td><img class="carousel-preview" src="/posts/'+post.thumb+'"></td>' +
                    '<td>'+post.title+'</td>' +
                    status +
                    '<td>'+post.created_at+'</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-primary btn-xs" onclick="window.location=\''+'/admin/blog/posts/'+post.id+'/edit'+'\'"><span class="glyphicon glyphicon-pencil"></span> Edit</button> ' +
                    '<button type="button" class="btn btn-danger btn-xs" onclick="javascript:deletePost('+post.id+');"><span class="glyphicon glyphicon-trash"></span> Delete</button>' +
                    '</td>' +
                    '</tr>';
            });
            $('#posts-container').html($template);
            Utils.hideAjaxStatus();
        });
    }
    var editorConfigurations = {
        core: {},
        plugins: {
            btnsDef: {
                image: {
                    dropdown: ['insertImage', 'upload', 'base64', 'noEmbed'],
                    ico: 'insertImage'
                }
            },
            btns: [
                ['viewHTML'],
                ['undo', 'redo'],
                ['formatting'],
                'btnGrp-design',
                ['link'],
                ['image'],
                'btnGrp-justify',
                'btnGrp-lists',
                ['foreColor', 'backColor'],
                ['preformatted'],
                ['horizontalRule'],
                ['fullscreen']
            ],
            plugins: {
                // Add imagur parameters to upload plugin
                upload: {
                    serverPath: 'https://api.imgur.com/3/image',
                    fileFieldName: 'image',
                    headers: {
                        'Authorization': 'Client-ID 9e57cb1c4791cea'
                    },
                    urlPropertyName: 'data.link'
                }
            }
        }
    };
    $('.form-validate').validationEngine();
    $('.editor').trumbowyg(editorConfigurations['plugins']);

});

var deleteSlider = function (id) {
    Utils.showAjaxStatus("Deleting slider ...");
    $.get('/admin/sliders/'+id+'/delete',function () {
        $('#row-'+id).fadeOut();
        Utils.hideAjaxStatus();
    });
}, deletePage = function (id) {
    Utils.showAjaxStatus("Deleting Page ...");
    $.get('/admin/pages/'+id+'/delete',function () {
        $('#row-'+id).fadeOut();
        Utils.hideAjaxStatus();
    });
}, deletePost = function (id) {
    Utils.showAjaxStatus("Deleting Post, Please wait ...");
    $.get('blog/posts/'+id+'/delete',function () {
        $('#row-'+id).fadeOut();
        Utils.hideAjaxStatus();
    });
}