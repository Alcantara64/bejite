$(function(){
    getActiveUrl();
    function getActiveUrl()
    {
        var url = window.location.href;
        var items = $('#sidebar a.list-group-item');
        $.each(items,function(index,elem){
            if($(this).attr('href').toLowerCase() == url.toLowerCase()){
                $('#sidebar a.list-group-item').removeClass('active');
               $(this).addClass('active');
            }
        });
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);
    $('#flash-overlay-modal').modal();
    $('.auto-grow').autogrow();
    $(".form-validate").validationEngine();

    $('#photo-btn').on('change',UploadProfilePhoto);
    $('#logo-file').on('change',UploadEmployerLogo);

    $('#photo-dialog').on('click',function(){
        document.getElementById('open-dialog').click();
    });
    $('.dialog').on('click',function(){
        var target = $(this).data('target');
        document.getElementById(target).click();
    });
    //Post box
    $('.post-box-container').find('button.cancel').click(function () {
        $('.post-box-container').fadeOut();
    });
    $('#post-txt').focus(function(){
        $('.post-box-container').fadeIn();
    });
    $('#open-dialog').on('change',function(){
        $('#post-photo-form').ajaxSubmit({
            success: function(data){
                $('#post-txt').append("<img src='"+data+"' style='max-width: 100%;max-height:450px;'>");
            },
            error: function(){
                alert('There are some errors with the image you are trying to upload');
            }
        });
    });
    $('#post_btn').on('click',function(){
        var text = $("#post-txt").html();
        $.post('/post/new',{post:text},function(){
            $('#post-txt').html("");
            $(this).removeClass('focus-txt');
            $(this).addClass('normal-txt');
            $('#post-controls').addClass('hide');
            refreshFeeds();
        })
    });
    function UploadProfilePhoto()
    {
        var di = $('.filedialog');
        di.html('Process, please wait...');
        $('#profile-photo-form').ajaxSubmit({
            success : function(data){
                di.html('<span class="glyphicon glyphicon-cloud-upload"></span> Choose file');
                $('.change-avatar').attr('src',data);
                $('#continue-btn').removeAttr('disabled');
            },
            error: function(){
                $('#upload-error').removeClass('hide');
            }
        });
    }
    function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    }
    if(window.location.pathname == '/home'){
        //refreshFeeds();
    }
    var Messages = {
        userId : $('#current_user').val(),
        getChatMessages : function (id) {
            $.getJSON("/messages/user/" + id +'/'+this.userId, function (data) {
                var li = '';
                if(data.messages == ""){
                    $('ul#topic_messages').html("");
                    return false;
                }
                $.each(data.messages, function (i, message) {
                    var msgStyle = 'others';
                    if(Number(message.id) == Number(Messages.userId)){
                        msgStyle = 'self';
                    }
                    console.log(Messages.userId);
                    console.log(message.id);
                    li += '<li class="'+msgStyle+'"><div class="chat-photo"><img src="/users/'+message.photo+'" class="avatar-xx circle-img"> </div><div class="chat-content"><label class="name"><a href="">' + message.fullnames +'</a></label><div class="message">' + message.content + '</div><div class="clear"></div></div><div class="clearfix"></div> </li>';
                });
                $('ul#topic_messages').html(li);
                if (data.messages.length > 0) {
                    Messages.scrollToBottom('msg_container_topic');
                }
            }).done(function () {

            }).fail(function () {
                Utils.notify('Sorry! Unable to fetch conversation','Ooops');
            }).always(function () {
            });

            // attaching the chatroom id to send button
            $('#send_to_topic').attr('data-receiver', id);
        },
        scrollToBottom : function (cls) {
            $('.' + cls).scrollTop($('.' + cls + ' ul li').last().position().top + $('.' + cls + ' ul li').last().height());
        }
    }
    var fetchChatList  = function () {
        var userId = $('#current_user').val();
        $.getJSON('/user/'+userId+'/connections',function (data) {
            var $template = '';
            $.each(data.connections,function (index,user) {
                var onlineStatus = '';
                if(user.is_online == 'yes'){
                    onlineStatus = '<div class="status-online pull-right"></div>';
                }
                $template += '<div class="connection-item">' +
                    '<div class="pull-left" style="margin-right: 10px;">' +
                    '<a href="javascript:void(0)" onclick="javascript:chatWith(\''+user.id+'\',\''+user.fullnames+'\');">' +
                    '<img class="avatar-xx circle-img" src="/users/'+user.photo+'">' +
                    '</a>' +
                    '</div>' +
                    '<div class="pull-left fullnames">' +
                    '<a href="javascript:void(0)" onclick="javascript:chatWith(\''+user.id+'\',\''+user.fullnames+'\');">' +
                    user.fullnames +
					'<p class="pull"<div >' +
                    '<a href="javascript:void(0)" onclick="javascript:chatWith(\''+user.id+'\',\''+user.fullnames+'\');">' +
                    user.account_type +
                    '</a>' +
                    '</div></p>' +
                    onlineStatus +
                    '<div class="clearfix"></div>' +
                    '</div>';
            });
            $('#connection-list').html($template );
        });
    }

    if(window.location.pathname == '/messages'){
        Messages.getChatMessages($('#topics li:first').attr('id'));
        $('ul#topics li').on('click', function () {
            $('ul#topics li').removeClass('selected');
            $(this).addClass('selected');
            Messages.getChatMessages($(this).prop('id'));
        });

        $('#send_to_topic').on('click', function () {
            postMessage();
        });
    }
    var scrollPage = 1;
    $(window).scroll(function () {
        if($(window).scrollTop() + $(window).height() == $(document).height()){
            scrollPage++;
            if(scrollPage > 1){
                loadFeeds(scrollPage);
            }
        }
    });
    var $profileFeed =  $('.profile-feed-only');
    var isProfilePage = document.getElementById('isProfilePage');
    if(isProfilePage != null){
        loadProfilePosts();
    }
    if(parseInt($('#current_user').val()) != 0){
        fetchChatList();
        var stillActive = setInterval(function () {
            fetchChatList();
        }, 60000);
    }
    var connectionList = new List('connection-lists',{
        valueNames : ['fullnames']
    });
    $('#press-enter').on('click',function () {
        if(document.getElementById('press-enter').checked){
            $('#send_to_topic_message').attr("onkeypress","javascript:messageUser(event);");
            $('#send_to_topic').addClass('hide');
        }else{
            $('#send_to_topic_message').removeAttr('onkeypress');
            $('#send_to_topic').removeClass('hide');
        }
    });
    //NEWLY ADDED CODES
    $('#signup-form-step1').ajaxForm({
        beforeSubmit : function () {
            $('#signup-error').html("");
            $('button.signup','#signup-form-step1')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Attempting Sign Up...');
        },
        success : function (data) {
            if(data.success == 'yes'){
                window.location.href='/home';
            }
			else{
				window.location.href='/home';
			}
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err ='';
            $.each($errors,function (index,error) {
                err += '<div>'+error+'</div>';
            })
            $('#signup-error').html('<div class="alert alert-danger">'+err+'</div>');
            $('button.signup','#signup-form-step1')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-user"></span> Sign Up');
				
        }
    });
    $('#signup-form-step2').ajaxForm({
        beforeSubmit : function () {
            $('#signup-error').html("");
            $('button.save','#signup-form-step2')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving Data...');
        },
        success : function (data) {
            if(data.success == 'yes'){
                window.location.href= data.redirect;
            }
        },
        error : function (data) {
            var $errors = $.parseJSON(data.responseText),err ='';
            $.each($errors,function (index,error) {
                err += '<div>'+error+'</div>';
            })
            $('#signup-error').html('<div class="alert alert-danger">'+err+'</div>');
            $('button.save','#signup-form-step2')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save & Continue');
        }
    });

    var Utils = {
        init : function () {
            this.uploads.openDiaglog();
            this.uploads.profilePhotoUpload();
            this.uploads.coverImageUpload();
        },
        uploads : {
            ajaxSpinner : '<span class="fa fa-spin fa-spinner"></span>',
            btnElem : $('.file-upload'),
            openDiaglog : function () {
                this.btnElem.on('click',function () {
                    var target  = $(this).data('target');
                    document.getElementById(target).click();
                    return false;
                });
            },
            profilePhotoUpload : function () {
                var elem = $('.profile-photo-upload');
                var targetImg = $(elem.data('target-img')),
                    targetForm = $(elem.data('target-form'));
                elem.on('change',function () {
                    Utils.uploads.btnElem.html("Please wait... "+Utils.uploads.ajaxSpinner);
                    Utils.uploads.btnElem.attr('disabled','disabled');
                    targetForm.ajaxSubmit({
                        success : function (data) {
                            if(data.success == 'yes'){
                                targetImg.attr('src','/users/'+data.img);
                                Utils.uploads.btnElem.html('<span class="glyphicon glyphicon-cloud"></span> Change Photo');
                                Utils.uploads.btnElem.removeAttr('disabled');
                            }
                        },
                        error : function (data) {
                            var errors = $.parseJSON(data.responseText),err = '';
                            $.each(errors,function (index,value) {
                                err += '<div>'+index+' : '+value+'</div>';
                            });
                            Utils.notify(err,"Fix the following errors");
                            Utils.uploads.btnElem.html('<span class="glyphicon glyphicon-cloud"></span> Change Photo');
                            Utils.uploads.btnElem.removeAttr('disabled');
                        }
                    });
                });
            }, coverImageUpload : function () {

                var elem = $('.cover-photo-upload');
                var targetDiv = $(elem.data('target-div')),
                    targetForm = $(elem.data('target-form'));
                elem.on('change',function () {
                    Utils.uploads.btnElem.html("Please wait... "+Utils.uploads.ajaxSpinner);
                    Utils.uploads.btnElem.attr('disabled','disabled');
                    targetForm.ajaxSubmit({
                        success : function (data) {
                            if(data.success == 'yes'){
                                $('#user-custom-bg').attr("src","/uploads/cover/"+data.img);
                                $('#user-custom-bg').removeClass('hide');
                                //targetDiv.css('background','url("/uploads/cover/'+data.img+'")');
                                Utils.uploads.btnElem.html('<span class="fa fa-camera"></span> Add Cover Photo ');
                                Utils.uploads.btnElem.removeAttr('disabled');
                            }
                        },
                        error : function (data) {
                            var errors = $.parseJSON(data.responseText),err = '';
                            $.each(errors,function (index,value) {
                                err += '<div>'+index+' : '+value+'</div>';
                            });
                            Utils.notify(err,"Fix the following errors");
                            Utils.uploads.btnElem.html('<span class="fa fa-camera"></span> Add Cover Photo ');
                            Utils.uploads.btnElem.removeAttr('disabled');
                        }
                    });
                });
            }
        },
        findPeople : function (elem) {
            $.get('/connection/find-plople',function(data){
                var $template = '';
                $.each(data,function(index,person){
                    var photo = '/users/'+person.photo;
                    $template += '<div class="user media " id="user-div-'+person.id+'">'
                        +'<div class="media-object pull-left">'
                        +'<a class="user-popover" href="">'
                        +'<img class="current-avatar" src="'+photo+'">'
                        +'</a>'
                        + '</div>'
                        +'<div class="media-body">'
                        +'<a href="">'
                        +'<h5 class="media-heading">'
                        + person.fullnames +'<span>'+'<span class="label label-success">'+person.account_type+'</span></span>'
                        +'</h5>'
                        +'</a>'
                        +'<div class="action-buttons">'
                        +'<button type="button" class="btn btn-primary button-sm follow" id="connect-btn-'+person.id+'" onclick="javascript:follow('+person.id+',this);"><span class="fa fa-user-plus"></span> Connect </button>'
                        +'</div>'
                        +'</div>'
                        + '</div>'
                    ;
                });
                elem.html($template);
            });
        }
        ,notify: function (msg,title) {
            var modal = $('#notify'),header = "";
            if(typeof title == "undefined"){
                header = "Notification"
            }else{
                header = title;
            }
            modal.modal('show');
            $('.modal-title',modal).html(header);
            $('.modal-body',modal).html(msg);
        }
    };

    Utils.init();
    $('#step-two-form').ajaxForm({
        beforeSubmit : function () {
            $('button.save','#step-two-form')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving....wait');
        },
        success : function (data) {
            if(data.success = 'yes'){
                var $container = $('#member-container');
                $('button.save','#step-two-form')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                $('.getstarted-left').css("opacity","0.4");
                $('#find-people').removeClass('shadow').addClass('box').css("opacity","1");
                $container.css("background","white none repeat scroll 0 0");
                Utils.findPeople($container);
                $('#continue-btn').removeAttr('disabled');
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err = '';
            $.each(errors,function (index,value) {
                err += '<div>'+value+'</div>';
            });
			 $('#signup-error').html('<div class="alert alert-danger">'+err+'</div>');
            Utils.notify(err,"Fix the following errors");
            $('button.save','#step-two-form')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
        }
    });
    function UploadEmployerLogo() {
        
    }
    //END NEWLY ADDED CODES
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //SEARCH
    $('#search-query').focus(function () {
        $('#search-result').fadeIn();
        console.log("information is power");
    }).blur(function () {
        $('#search-result').fadeOut();
    });

    if(window.location.pathname == '/home'){
        loadFeeds();
    }
    var scrollPage = 1;
    $(window).scroll(function () {
        if($(window).scrollTop() + $(window).height() == $(document).height()){
            scrollPage++;
            if(scrollPage > 1){
                loadFeeds(scrollPage);
            }
        }
    });



    /** New Codes **/

    $('#settings-data').ajaxForm({
        beforeSubmit : function () {
            $('button.save','#settings-data')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving ... Please wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                $('button.save','#settings-data')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify("Your profile Information was successfully updated","Successfully Updated")
            }else{
                $('button.save','#settings-data')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify(data.feedback,"Fix the following error");
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err ='';
            $.each(errors,function (index,error) {
                err += '<div>'+index+':'+error+'</div>';
            });
            $('button.save','#settings-data')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            Utils.notify(err,"Fix the following errors");
        }
    });
    $('#settings-change-password').ajaxForm({
        beforeSubmit : function () {
            $('button.save','#settings-change-password')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Changing Password ... Please wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                $('button.save','#settings-change-password')
                    .removeAttr('disabled')
                    .html('<span class="fa fa-key"></span> Change Password');
                Utils.notify("Your Password was changed successfully","Successfully Updated")
            }else{
                $('button.save','#settings-change-password')
                    .removeAttr('disabled')
                    .html('<span class="fa fa-key"></span> Change Password');
                Utils.notify("Your Password is incorrect","Fix the following error");
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err ='';
            $.each(errors,function (index,error) {
                err += '<div>'+index+':'+error+'</div>';
            });
            $('button.save','#settings-change-password')
                .removeAttr('disabled')
                .html('<span class="fa fa-key"></span> Change Password');
            Utils.notify(err,"Fix the following errors");
        },
        resetForm : true
    });
    $('#settings-privacy').ajaxForm({
        beforeSubmit : function () {
            $('button.save','#settings-privacy')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving ... Please wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                $('button.save','#settings-privacy')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify("Your privacy Settings was successfully updated","Successfully Updated")
            }else{
                $('button.save','#settings-privacy')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify(data.feedback,"Fix the following error");
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err ='';
            $.each(errors,function (index,error) {
                err += '<div>'+index+':'+error+'</div>';
            });
            $('button.save','#settings-privacy')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            Utils.notify(err,"Fix the following errors");
        }
    });
    $('#settings-notifications').ajaxForm({
        beforeSubmit : function () {
            $('button.save','#settings-notifications')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Saving ... Please wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                $('button.save','#settings-notifications')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify("Your Notification settings was successfully updated","Successfully Updated")
            }else{
                $('button.save','#settings-notifications')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-pencil"></span> Save');
                Utils.notify(data.feedback,"Fix the following error");
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err ='';
            $.each(errors,function (index,error) {
                err += '<div>'+index+':'+error+'</div>';
            });
            $('button.save','#settings-notifications')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Save');
            Utils.notify(err,"Fix the following errors");
        }
    });
    $('#settings-delete-account').ajaxForm({
        beforeSubmit : function () {
            $('button.delete','#settings-delete-account')
                .attr('disabled','disabled')
                .html('<span class="fa fa-spin fa-spinner"></span> Initializing Delete ... Please wait...');
        },
        success : function (data) {
            if(data.status == 'success'){
                $('button.delete','#settings-delete-account')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-trash"></span> Delete Account');
                Utils.notify("Your request to delete your account has been Initialize","Delete Initialized")
            }else{
                $('button.delete','#settings-delete-account')
                    .removeAttr('disabled')
                    .html('<span class="glyphicon glyphicon-trash"></span> Delete Account');
                Utils.notify("Your password is incorrect","Wrong Password");
            }
        },
        error : function (data) {
            var errors = $.parseJSON(data.responseText),err ='';
            $.each(errors,function (index,error) {
                err += '<div>'+index+':'+error+'</div>';
            });
            $('button.delete','#settings-delete-account')
                .removeAttr('disabled')
                .html('<span class="glyphicon glyphicon-pencil"></span> Delete Account');
            Utils.notify(err,"Fix the following errors");
        }
    });

});
function refreshFeeds(){
    var $template = '';
        $.get('/posts/newsfeed',function(data){
            var photo = $('.prof-photo').attr('src');
            $.each(data.posts,function(index,val){
                var userType = '';
                if(val.account_type == 'jobseeker'){
                    userType = '<span class="label label-info"><span class="glyphicon glyphicon-arrow-right"></span> '+val.account_type+'</span>';
                }else {
                    userType ='<span class="label label-primary"><span class="glyphicon glyphicon-arrow-right"></span> '+val.account_type+'</span>'
                }
                var comments = getComments(val.id,'comment-container-'+val.id);
                $template += '<div class="box panel panel-default noheading">'
                           +'<div class="panel-body">'
                           +'<div class="col-md-1">'
                           + '<img src="/users/'+val.photo+'" class="avatar-xs-circle change-avatar">'
                           + '</div>'
                           + '<div class="col-md-11">'
                           + '<p>'
                           + '<a href="">'
                           + '<span class="fullnames">' +val.fullnames +' </span>'
                           + '</a>'
                           + userType
                           + '</p>'
                           + val.content
                           + '<div class="comment-container">'
                            + '<div id="comment-container-'+val.id+'">'
                            //+ comments
                            + '</div>'
                           + '<div class="col-md-1">'
                           + '<img src="'+photo+'" class="avatar-xxs-circle change-avatar">'
                           + '</div>'
                           + '<div class="col-md-11">'
                           + '<textarea id="comment-box-'+val.id+'" class="form-control comment-box" placeholder="Add comment" maxlength="255" onkeypress="return addComment('+val.id+',event);"></textarea>'
                           + '</div>'
                           + '<div class="clearfix"></div>'

                           + '</div>'
                           + '</div>'
                           + '</div>'
                           + '</div>'
                           ;
            });
        $('#newsfeed').html($template);
        },'json');
}
function fileDialog(target)
{
    document.getElementById(target).click();
}
/*
function addComment(i,e)
{
    if(e.keyCode == 13)
    {
        var comment = $('#comment-box-'+i).val();
        $('#comment-box-'+i).val("");
        if(comment!='')
        {
            $.post('/posts/addcomment',{comment:comment,cid:i},function(){
                getComments(i,'comment-container-'+i);
            });
        }
    }
}
*/
function getComments(id,elem)
{
    var $template = '';
    $.get('/posts/comments/'+id,function(data){
        if(data.comments != ''){
            $.each(data.comments,function(index,val){
                $template += '<div class="col-md-1">'
                        + '<img src="/users/'+val.photo+'" class="avatar-xxs-circle change-avatar">'
                        + '</div>'
                        + '<div class="col-md-11">'
                        + '<div class="comment">'
                        + val.content
                        + '</div>'
                        + '</div>'
                        + '<div class="clearfix"></div>'
                        ;
            });
        }
        if(typeof elem != "undefined"){
            $('#'+elem).html($template)
        }else{
            return $template;
        }
     });
}
function follow(id,btn) {
    var $btn = $(btn);
    $btn.html('<span class="fa fa-spin fa-spinner"></span> Connecting...');
    $.post('/connection/follow',{id:id},function (data) {
        if(data.status == 'ok'){
            $btn.attr('onclick','javascript:unFollow('+id+',this);');
            $btn.addClass('unfollow');
            $btn.html('<span class="glyphicon glyphicon-transfer"></span> Disconnect');
        }
    });
}
function unFollow(id,btn) {
    var $btn = $(btn);
    $btn.html('<span class="fa fa-spin fa-spinner"></span> Processing...');
    $.post('/connection/unfollow',{id:id},function (data) {
        if(data.status == 'ok'){
            $btn.attr('onclick','javascript:follow('+id+',this);');
            $btn.removeClass('unfollow');
            $btn.html('<span class="glyphicon glyphicon-transfer"></span> Connect');
        }
    });
}


//INNOVATION
var loadFeeds = function (page) {
    var url = '/user/feed';
    if(typeof page !='undefined'){
        url = '/user/feed/'+page
    }
    $.getJSON(url,function (data) {
        var $posts = data.feed,$template = '';
        feedTemplate($posts);
    });
}
var loadProfilePosts = function () {
    var username = $('#profile-username').val();
    $.getJSON('/'+username+'/feed',function (data) {
        var $posts = data.feed,$template = '';
        feedTemplate($posts);
    });
}
var addComment = function (e,id) {
    if(typeof id == 'undefined'){
        return false;
    }
    if(e.keyCode == 13){
        var $comment = $('#comment-text-'+id),userId = parseInt($('#current_user').val());
        if($comment.val() !=''){
            if(userId == 0){
                return false;
            }
            $.post('/post/addcomment',{content:$comment.val(),post_id:id,user_id:userId},function (data) {
                $comment.val("");
                var comment = data.comment;
                $template = '<li>' +
                    '<div class="connection-item">'+
                    '<div class="pull-left" style="margin-right: 10px;">' +
                    '<a href="">' +
                    '<img class="avatar-xx circle-img" src="/users/'+comment.photo+'">'+
                    '</a>'+
                    '</div>'+
                    '<div class="pull-left comment-content">' +
                    '<a href="">' +
                    comment.fullnames  +
                    '</a> '+
                    comment.content +
                    '<br>' +
                    '<time>'+moment().fromNow()+'</time>' +
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>'+
                    '</li>'
                $('#post-comments-'+id+' ul').append($template);
            });
        }
    }
}
var loadComments = function (comments) {
    $template = '';
    $.each(comments,function (index,comment) {
        $template += '<li>' +
            '<div class="connection-item">'+
            '<div class="pull-left" style="margin-right: 10px;">' +
            '<a href="'+comment.username+'" style="float:left;">' +
            '<img class="avatar-xx circle-img" src="/users/'+comment.photo+'">'+
			'&nbsp;&nbsp;'+'<span>'+comment.fullnames+'</span>'+
            '</a>'+
            '</div>'+
            '<div class="comment-content" style="clear:left; margin-left:30px;">' +
            comment.content +
            '<br>' +
            '<time style="color:gray;">'+moment.utc(comment.created_at).fromNow()+'</time>' +
            '</div>'+
            '<div class="clearfix"></div>'+
            '</div>'+
            '</li>';
    });
    return $template;
}
var flash = function (msg) {
    var $flash = $('#flash-data'),$p = $('#flash-data p');
    $p.html(msg);
    $flash.fadeIn();
    setTimeout(function () {
        $flash.fadeOut();
    },4000);
};
var viewAllReactorsTemplate = function (postId) {
    var $template = '<li>' +
        '<a href="#" onclick="PostBox.viewReactors('+postId+'); return false;" id="view-all-reactors-'+postId+'" class="view-reactors" data-toggle="tooltip" data-placement="top" title="View All">' +
        '<span class="glyphicon glyphicon-option-horizontal"></span>' +
        '</a>'+
        '</li>';
    return $template;
};
var getReactions = function (reactions) {
    var $template = '',count = 0;
    var postId = 0;
    $.each(reactions,function (index,user) {
        $template += '<li>' +
            '<a href="'+user.username+'" data-toggle="tooltip" data-placement="top" title="'+user.fullnames+'">' +
            '<img class="avatar-xx img-circle" src="/users/'+user.photo+'">' +
            '<img class="type" src="/likes/'+user.type+'.png">' +
            '</a>'+
            '</li>';
        count++;
        postId = user.post_id;
    });
    if(count > 0){
        $template += viewAllReactorsTemplate(postId);
    }
    return $template;
}
var postImages = function (imageStr) {
    var images = '';
    if(imageStr != '' && typeof imageStr != 'object' && typeof imageStr !='undefined'){
        var arr = imageStr.split(',');
        var count = arr.length;
        if(count > 0){
            switch(count){
                case 1:
                    images = '<img class="images-1" src="/posts/images/'+imageStr+'">';
                    break;
                case 2:
                    images = '<div class="images-2 pull-left">' +
                        '<img src="/posts/images/'+arr[0]+'">' +
                        '</div>' +
                        '<div class="images-2 pull-left">' +
                        '<img src="/posts/images/'+arr[1]+'">' +
                        '</div>';
                    images +='<div class="clearfix"></div>';
                    break;
                case 3:
                    images = '<div class="pull-left three-one">' +
                        '<img class="images-2" src="/posts/images/'+arr[0]+'"></div>' +
                        '<div class="pull-left three-two">' +
                        '<img src="/posts/images/'+arr[1]+'">' +
                        '<img src="/posts/images/'+arr[2]+'">' +
                        '</div>';
                    images += '<div class="clearfix"></div>';
                    break;
                case 4:
                    images = '<div class="pull-left four-img">' +
                        '<img src="/posts/images/'+arr[0]+'">' +
                        '<img src="/posts/images/'+arr[1]+'">' +
                        '</div>' +
                        '<div class="pull-left four-img">' +
                        '<img src="/posts/images/'+arr[2]+'">' +
                        '<img src="/posts/images/'+arr[3]+'">' +
                        '</div>';
                    images += '<div class="clearfix"></div>';
                    break;
                case 5:
                    images = '<div class="pull-left five-img-two">' +
                        '<img src="/posts/images/'+arr[0]+'">' +
                        '<img src="/posts/images/'+arr[1]+'">' +
                        '</div>' +
                        '<div class="pull-left five-mg-three">' +
                        '<img src="/posts/images/'+arr[2]+'">' +
                        '<img src="/posts/images/'+arr[3]+'">' +
                        '<img src="/posts/images/'+arr[4]+'">' +
                        '</div>';
                    images += '<div class="clearfix"></div>';
                    break;
                default :
                    for(var i = 0; i < arr.length; i++){
                        images += '<div class="default-img pull-left">' +
                            '<img src="/posts/images/'+arr[i]+'">' +
                            '</div>';
                    }
                    images += '<div class="clearfix"></div>';
                    break;
            }
        }
    }
    return '<div class="post-images">'+images+'</div>';
}


var feedTemplate = function (posts) {
    var savedPosts = Options.savedPosts.split(',').map(Number);
    var userId = Options.userId;
    var currentUserPhoto = $('#current_user_photo').val();
    var UserId = Number($('#current_user').val());
    $.each(posts,function (index,post) {
        var postOptions = '';
        var reportOption = '<li class="divider"></li>'+
            '<li><a href="#" onclick="javascript:PostBox.report('+post.id+'); return false;"><span class="fa fa-flag"></span>&nbsp;&nbsp; Report</a></li>';
        var saveOption = '<li><a id="save-post-'+post.id+'"  href="#" onclick="javascript:PostBox.save('+post.id+'); return false;"><span class="glyphicon glyphicon-save"></span>&nbsp;&nbsp; Save</a></li>';
        if(UserId == post.user_id){
            postOptions = '<li><a href="#" onclick="javascript:PostBox.edit('+post.id+'); return false;"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp; Edit</a></li>'+
                '<li><a href="#" onclick="javascript:PostBox.delete('+post.id+'); return false;"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp; Delete</a></li>';
            reportOption = '';
        }
        if(savedPosts.indexOf(post.id) != -1){
            saveOption = '<li><a id="save-post-'+post.id+'" href="#" onclick="javascript:PostBox.unSave('+post.id+'); return false;"><span class="glyphicon glyphicon-remove-circle"></span>&nbsp;&nbsp; Unsave</a></li>';
        }
        var likeButton = '<a id="like-btn-'+post.id+'" href="#" onclick="PostBox.like('+post.id+',\'like\',this); return false;"> <i class="fa fa-thumbs-up"></i> <span>Like</span></a>';
        if(post.likes.length > 0){
            $.each(post.likes,function (index,like) {
                if(like.userId == UserId){
                    likeButton = '<a id="like-btn-'+post.id+'" href="#" class="color" onclick="PostBox.like('+post.id+',\'like\',this); return false;"> <i class="fa fa-thumbs-up"></i> <span>Like</span></a>';
                }
            });
        }
		
		//Substring for feeds.
	/*function getRndInteger(min, max){
	return Math.floor(Math.random()*(max-min+1))+min;
	}
	var content = post.content;
	function hideSomeText(text){
	if(text.length>200){
		
		var readMoreString = '...<a href="javascript:void(0);" onclick="fullText();">'
	+'<strong>'+'Read More'+'</strong></a>';	
	return text.substr(0, getRndInteger(200, 500))+readMoreString;
	}
	else{
		return text;
	}
	}
	
	
	var readMore = hideSomeText(clickableUrl(content));*/
        
		
		$template = '<div id="post-'+post.id+'" class="box-feed padded" style="padding-bottom: 0;">' +
            '<div class="post-header">' +
            '<div class="">' +
            '<div class="user-media-photo pull-left">' +
            '<img class="avatar-m circle-img" src="/users/'+post.photo+'">' +
            '</div>' +
            '<div class="pull-right dropdown" aria-expanded="false">' +
            '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-option-horizontal"></span> </a> ' +
            '<ul class="dropdown-menu">' +
            '' + postOptions +
           /*'<li><a href="#" onclick="javascript:PostBox.hide('+post.id+'); return false;"><span class="glyphicon glyphicon-eye-close"></span>&nbsp;&nbsp; Hide</a></li>'+*/
            saveOption +
            reportOption +
            '</ul>' +
            '</div>' +
            '<div class="user-media-info pull-left">' +
            '<h5 class="names">' +
            '<a href="/'+post.username+'">' +
            post.fullnames +
            '</a>' +
            '<br>' +
            '<span><strong>'+post.account_type +'</strong></span>'+
            '<br>' +
            '<time>'+moment.utc(post.created_at).fromNow()+'</time>' +
            '</h5>' +
            '</div>' +
            '<div class="clearfix"></div>' +
            '</div>' +
            '</div>'+
            '<div id="edit-post-'+post.id+'"></div>' +
            '<div class="post-content">' +
            '<div class="content-text" id="posting-'+post.id+'">'+clickableUrl(post.content)+'</div>' + postImages(post.image) +
            '</div>' +
            '<div class="reactions">' +
            '<ul id="post-reactors-'+post.id+'">' +
            getReactions(post.likes) +
            '</ul>' +
            '</div>' +
            '<div class="posts-comments">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<div class="post-icons">' +
            '<ul>' +
            '<li class="feed-react">' +
            '<div class="react-icons">' +
            '<ul>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'like\'); return false;" data-toggle="tooltip" data-placement="top" title="Like"><img src="/likes/like.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'love\'); return false;" data-toggle="tooltip" data-placement="top" title="Love"><img src="/likes/love.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'haha\'); return false;" data-toggle="tooltip" data-placement="top" title="Haha"><img src="/likes/haha.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'yay\'); return false;" data-toggle="tooltip" data-placement="top" title="Yay"><img src="/likes/yay.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'wow\'); return false;" data-toggle="tooltip" data-placement="top" title="Wow"><img src="/likes/wow.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'sad\'); return false;" data-toggle="tooltip" data-placement="top" title="Sad"><img src="/likes/sad.png"></a></li>' +
            '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'angry\'); return false;" data-toggle="tooltip" data-placement="top" title="Angry"><img src="/likes/angry.png"></a></li>' +
            '</ul>' +
            '</div>' + likeButton +
            '</li>' +
            //'<li><a href="#" onclick="javascript:void(0); return false;"> <i class="glyphicon glyphicon-comment"></i> <span>Comment</span></a></li>' +
            '<li><a href="#" onclick="javascript:PostBox.share('+post.id+'); return false;"> <i class="fa fa-retweet"></i> <span>Share</span></a></li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row comments">' +
            '<div class="col-md-12">' +
            '<div id="post-comments-'+post.id+'" class="list-comments">' +
            '<ul>' +
            loadComments(post.comments) +
            '</ul>' +
            '</div>' +
            '<div class="comment-box">' +
            '<div class="left pull-left">' +
            '<img src="/users/'+currentUserPhoto+'" class="avatar-xx circle-img">' +
            '</div>' +
            '<div class="right pull-left">' +
            '<textarea class="input" id="comment-text-'+post.id+'" placeholder="Write comment" onkeypress="addComment(event,'+post.id+');"></textarea>' +
            '</div>' +
            '<div class="clearfix"></div>'+
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $('#feeds-container').append($template);
    });
    $('[data-toggle="tooltip"]').tooltip();
	
};

var postTemplate = function (post) {
    var savedPosts = Options.savedPosts.split(',').map(Number);
    var userId = Options.userId;
    var currentUserPhoto = $('#current_user_photo').val();
    var UserId = Number($('#current_user').val());
    var postOptions = '';
    var reportOption = '<li class="divider"></li>'+
        '<li><a href="#" onclick="javascript:PostBox.report('+post.id+'); return false;"><span class="fa fa-flag"></span>&nbsp;&nbsp; Report</a></li>';
    var saveOption = '<li><a id="save-post-'+post.id+'"  href="#" onclick="javascript:PostBox.save('+post.id+'); return false;"><span class="glyphicon glyphicon-save"></span>&nbsp;&nbsp; Save</a></li>';
    if(UserId == post.user_id){
        postOptions = '<li><a href="#" onclick="javascript:PostBox.edit('+post.id+'); return false;"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp; Edit</a></li>'+
            '<li><a href="#" onclick="javascript:PostBox.delete('+post.id+'); return false;"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp; Delete</a></li>';
        reportOption = '';
    }
    if(savedPosts.indexOf(post.id) != -1){
        saveOption = '<li><a id="save-post-'+post.id+'" href="#" onclick="javascript:PostBox.unSave('+post.id+'); return false;"><span class="glyphicon glyphicon-remove-circle"></span>&nbsp;&nbsp; Unsave</a></li>';
    }
    var likeButton = '<a id="like-btn-'+post.id+'" href="#" onclick="PostBox.like('+post.id+',\'like\',this); return false;"> <i class="fa fa-thumbs-up"></i> <span>Like</span></a>';
    if(post.likes.length > 0){
        $.each(post.likes,function (index,like) {
            if(like.userId == UserId){
                likeButton = '<a id="like-btn-'+post.id+'" href="#" class="color" onclick="PostBox.like('+post.id+',\'like\',this); return false;"> <i class="fa fa-thumbs-up"></i> <span>Like</span></a>';
            }
        });
    }
	
	//Substring for posts.
	/*function getRndInteger(min, max){
	return Math.floor(Math.random()*(max-min+1))+min;
	}
	var content = post.content;
	function hideSomeText(text){
	if(text.length>200){
	return text.substr(0, getRndInteger(200, 500))+'...'+'<a href="javascript:void(0)" onclick="fullText('+content+');"><strong>Read More</strong></a>';
	}
	else{
		return text;
	}
	}
	var readMore = hideSomeText(clickableUrl(content));*/
        
		
    $template = '<div id="post-'+post.id+'" class="box-feed padded" style="padding-bottom: 0;">' +
        '<div class="post-header">' +
        '<div class="">' +
        '<div class="user-media-photo pull-left">' +
        '<img class="avatar-m circle-img" src="/users/'+post.photo+'">' +
        '</div>' +
        '<div class="pull-right dropdown" aria-expanded="false">' +
        '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-option-horizontal"></span> </a> ' +
        '<ul class="dropdown-menu">' +
        '' + postOptions +
        /*'<li><a href="#" onclick="javascript:PostBox.hide('+post.id+'); return false;"><span class="glyphicon glyphicon-eye-close"></span>&nbsp;&nbsp; Hide</a></li>'+*/
        saveOption +
        reportOption +
        '</ul>' +
        '</div>' +
        '<div class="user-media-info pull-left">' +
        '<h5 class="names">' +
        '<a href="/'+post.username+'">' +
        post.fullnames +
        '</a>' +
        '<br>' +
        '<span><strong>'+post.account_type +'</strong></span>'+
        '<br>' +
        '<time>'+moment.utc(post.created_at).fromNow()+'</time>' +
        '</h5>' +
        '</div>' +
        '<div class="clearfix"></div>' +
        '</div>' +
        '</div>'+
        '<div id="edit-post-'+post.id+'"></div>' +
        '<div class="post-content">' +
        '<div class="content-text">' +clickableUrl(post.content)+'</div>' + postImages(post.image) +
        '</div>' +
        '<div class="reactions">' +
        '<ul id="post-reactors-'+post.id+'">' +
        getReactions(post.likes) +
        '</ul>' +
        '</div>' +
        '<div class="posts-comments">' +
        '<div class="row">' +
        '<div class="col-md-12">' +
        '<div class="post-icons">' +
        '<ul>' +
        '<li class="feed-react">' +
        '<div class="react-icons">' +
        '<ul>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'like\'); return false;" data-toggle="tooltip" data-placement="top" title="Like"><img src="/likes/like.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'love\'); return false;" data-toggle="tooltip" data-placement="top" title="Love"><img src="/likes/love.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'haha\'); return false;" data-toggle="tooltip" data-placement="top" title="Haha"><img src="/likes/haha.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'yay\'); return false;" data-toggle="tooltip" data-placement="top" title="Yay"><img src="/likes/yay.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'wow\'); return false;" data-toggle="tooltip" data-placement="top" title="Wow"><img src="/likes/wow.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'sad\'); return false;" data-toggle="tooltip" data-placement="top" title="Sad"><img src="/likes/sad.png"></a></li>' +
        '<li><a href="javascript:void(0);" onclick="PostBox.like('+post.id+',\'angry\'); return false;" data-toggle="tooltip" data-placement="top" title="Angry"><img src="/likes/angry.png"></a></li>' +
        '</ul>' +
        '</div>' + likeButton +
        '</li>' +
       // '<li><a href="#" onclick="javascript:void(0); return false;"> <i class="glyphicon glyphicon-comment"></i> <span>Comment</span></a></li>' +
        '<li><a href="#" onclick="javascript:PostBox.share('+post.id+'); return false;"> <i class="fa fa-retweet"></i> <span>Share</span></a></li>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row comments">' +
        '<div class="col-md-12">' +
        '<div id="post-comments-'+post.id+'" class="list-comments">' +
        '<ul>' +
        loadComments(post.comments) +
        '</ul>' +
        '</div>' +
        '<div class="comment-box">' +
        '<div class="left pull-left">' +
        '<img src="/users/'+currentUserPhoto+'" class="avatar-xx circle-img">' +
        '</div>' +
        '<div class="right pull-left">' +
        '<textarea class="input" id="comment-text-'+post.id+'" placeholder="Write comment" onkeypress="addComment(event,'+post.id+');"></textarea>' +
        '</div>' +
        '<div class="clearfix"></div>'+
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return $template;
};
var search = function () {
    var $keyword = $('#search-query').val();
    if($keyword.trim() != ''){
        $.getJSON('/search',{keyword:$keyword},function (data) {
            var template = '';
            if(data.result == ''){
                $('#search-loader').html('<li><a href="#"> <span class="fa fa-spinner fa-spin"></span> Searching...</a> </li>');
            }else{
                $('#search-loader').html('<li class="first"> <a href="#" onclick="javascript:void(0)"><span class="fa fa-users"></span> Find People By name,city,state,country and more... </a> </li>');
            }
            $.each(data.result,function (index,user) {
                template += '<li>' +
                    '<div class="user media">' +
                    '<div class="media-object pull-left">' +
                    '<a href="/'+user.username+'">' +
                    '<img class="avatar-xx circle-img" src="/users/'+user.photo+'">' +
                    '</a> ' +
                    '</div>' +
                    '<div class="media-body">' +
                    '<a href="'+user.username+'">' +
                    '<h5 class="media-heading">' +
                    user.fullnames + ' '+ '<span>'+user.account_type+'</span>' +
                    '</h5>' +
                    '</a> ' +
                    '</div>' +
                    '</div>' +
                    '</li>';
            });
            $('#search-ul').html(template);
        });
    }
}
var messageUser = function (event) {
    if(event.keyCode == 13){
        postMessage();
    }
}
var postMessage =  function () {
    var msg = $('#send_to_topic_message').val();
    if (msg.trim().length === 0) {
        //Utils.notify('Enter a message');
        return;
    }
    $('#send_to_topic_message').val('');
    $('#loader_topic').show();
    var receiver = $('#send_to_topic').data('receiver');
    var userId = $('#current_user').val();
    if(parseInt(userId) == 0)
    {
        return false;
    }
    $.post("/messages/add", {msg: msg,receiver:receiver,sender:userId}, function (data) {
        if (data.success === 'ok') {
            var message = data.messages;
            $('#last-msg-'+receiver).html(message.content);
            var li = '<li class="self"><div class="chat-photo"><img src="/users/'+message.photo+'" class="avatar-xx circle-img"> </div><div class="chat-content"><label class="name"><a href="">' + message.fullnames + '</a></label><div class="message">' + message.content + '</div><div class="clear"></div></div><div class="clearfix"></div> </li>';
            $('ul#topic_messages').append(li);
            Messages.scrollToBottom('msg_container_topic');
        } else {
            Utils.notify('Sorry! Unable to send message','ooops');
        }
    }).done(function () {

    }).fail(function () {
        alert('Sorry! Unable to send message');
    }).always(function () {
        $('#loader_topic').hide();
    });
};
function fullText(){
		$.getJSON('/postdetails',function(data){
			var me = data.posts;
			
		var $display = me.content;
		var allText = $display.substr(0);
		var $displayAllText = '<div>'+allText+'</div>';
		$('#posting-'+me.id+'').html($displayAllText);
		});
	}

//Function to mark notifications as notified.

function notify(){
	$.post('/notify',function(data){
		if(data.status == 'ok'){
			return true;
		}	
	});
	}
	
	//function to convert urls to links.
	
	function clickableUrl(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}
