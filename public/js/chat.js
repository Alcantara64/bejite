/**
 * Created by mac on 6/1/16.
 */
var chat =
{

    windowFocus : true,
    userId : '',
    chatHeartbeatCount : 0,
    minChatHeartbeat : 1000,
    maxChatHeartbeat : 33000,
    chatHeartbeatTime : this.minChatHeartbeat,
    originalTitle : '',
    blinkOrder : 0,
    chatboxFocus : new Array(),
    newMessages : new Array(),
    newMessagesWin : new Array(),
    chatBoxes : new Array(),

    rearrangeChatBoxes : function()
    {
        var align = 0;
        for(x in this.chatBoxes)
        {
            var chatboxId = this.chatBoxes[x],
                chatbox = $('#chatbox-'+chatboxId);
            if(chatbox.css('display') != 'none')
            {
                if(align == 0)
                {
                    chatbox.css('right','220px');
                }
                else
                {
                    var width = (align)*(225+7)+220;
                    chatbox.css('right',width + 'px');
                }
                align++;
            }
        }
    },
    createChatBox : function (chatboxId,chatboxTitle,chatboxMinimized)
    {
        if($('#chatbox-'+chatboxId).length > 0)
        {
            if($('#chatbox-'+chatboxId).css('display') == 'none')
            {
                $('#chatbox-'+chatboxId).css('display','block');
                this.rearrangeChatBoxes();
            }
            $('#chatbox-'+chatboxId+' .chatboxtextarea').focus();
            return;
        }
        $("<div>")
            .attr('id','chatbox-'+chatboxId)
            .addClass("chatbox")
            .html('<div class="chatboxhead"><div class="chatboxtitle">'+chatboxTitle+'</div><div class="chatboxoptions"><a href="javascript:void(0)" onclick="javascript:chat.toggleChatbox(\''+chatboxId+'\')"><span class="fa fa-cog"></span> </a> &nbsp; <a href="javascript:void(0)" onclick="javascript:chat.closeChatbox(\''+chatboxId+'\')">X</a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><textarea class="chatboxtextarea" placeholder="Type a message" onkeydown="javascript:return chat.sendMessage(event,this,\''+chatboxId+'\');"></textarea></div>')
            .appendTo($('body'));
        var chatbox = $('#chatbox-'+chatboxId);
        chatbox.css('bottom','0px');
        var chatBoxesLenght = 0;
        for (x in this.chatBoxes)
        {
            if($('#chatbox-'+this.chatBoxes[x]).css('display') != 'none')
            {
                chatBoxesLenght++;
            }
        }
        if(chatBoxesLenght == 0)
        {
            chatbox.css('right','220px');
        }
        else
        {
            var width = (chatBoxesLenght)*(225+7)+220;
            chatbox.css('right',width + 'px');
        }
        this.chatBoxes.push(chatboxId);

        if(chatboxMinimized == 1)
        {
            var minimize = 0,
                minimizedChatBoxes = Array();

            if($.cookie('chatbox_minimized'))
            {
                minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
            }
            for (j=0;j<minimizedChatBoxes.length;j++)
            {
                if (minimizedChatBoxes[j] == chatboxId)
                {
                    minimize = 1;
                }
            }
            if(minimize == 1)
            {
                $('#chatbox-'+chatboxId+' .chatboxcontent').css('display','none');
                $('#chatbox-'+chatboxId+' .chatboxinput').css('display','none');
            }
        }
        chat.chatboxFocus[chatboxId] = false;
        $('#chatbox-'+chatboxId+' .chatboxtextarea').on('blur',function () {
            chat.chatboxFocus[chatboxId] = false;
            $(this).removeClass('chatboxtextareaselected');
        }).on('focus',function () {
            chat.chatboxFocus[chatboxId] = true;
            $(this).addClass('chatboxtextareaselected');
        });

        chatbox.on('click',function () {
            if($('#chatbox-'+chatboxId+' .chatboxcontent').css('display') != 'none')
            {
                $('#chatbox-'+chatboxId+' .chatboxtextarea').focus();
            }
        });
        chatbox.show();
        $.post('/chat/create',{chatbox:chatboxId,title:chatboxTitle},function (data) {
            if(data.success == 'yes')
            {

            }
        })
    },
    closeChatbox : function (chatboxId) {
        $('#chatbox-'+chatboxId).css('display','none');
        this.rearrangeChatBoxes();
        $.post('/chat/close',{chatbox:chatboxId},function (data) {

        })
    },
    toggleChatbox :  function (chatboxId) {
        var chatboxContent = $('#chatbox-'+chatboxId+' .chatboxcontent');
        if(chatboxContent.css('display') == 'none')
        {
            var minimizedChatBoxes = new Array();

            if ($.cookie('chatbox_minimized')) {
                minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
            }

            var newCookie = '';

            for (i=0;i<minimizedChatBoxes.length;i++) {
                if (minimizedChatBoxes[i] != chatboxId) {
                    newCookie += chatboxId+'|';
                }
            }

            newCookie = newCookie.slice(0, -1)
            $.cookie('chatbox_minimized', newCookie);
            chatboxContent.css('display','block');
            $('#chatbox-'+chatboxId+' .chatboxinput').css('display','block');
            chatboxContent.scrollTop(chatboxContent[0].scrollHeight);
        }
        else
        {
            var newCookie = chatboxId;

            if ($.cookie('chatbox_minimized')) {
                newCookie += '|'+$.cookie('chatbox_minimized');
            }
            $.cookie('chatbox_minimized',newCookie);

            chatboxContent.css('display','none');
            $('#chatbox-'+chatboxId+' .chatboxinput').css('display','none');
        }
    },
    sendMessage : function (event,textarea,chatboxId) {
        if(event.keyCode == 13 && event.shiftKey == 0)
        {
            message = $(textarea).val();
            message = message.replace(/^\s+|\s+$/g,"");
            var userId = $('#current_user').val();

            $(textarea).val('');
            $(textarea).focus();
            $(textarea).css('height','44px');
            if(message!= '')
            {
                //TODO send message and append it to the current chat
                if(parseInt(userId) == 0)
                {
                    return false;
                }
                $.post('/messages/add',{sender:userId,receiver:chatboxId,msg:message},function (data) {

                });
            }
            this.chatHeartbeatTime = this.minChatHeartbeat;
            this.chatHeartbeatCount = 1;
            return false;
        }

        var adjustedHeight = textarea.clientHeight;
        var maxHeight = 94;
        if (maxHeight > adjustedHeight)
        {
            adjustedHeight = Math.max(textarea.scrollHeight, adjustedHeight);
            if (maxHeight)
            {
                adjustedHeight = Math.min(maxHeight, adjustedHeight);
            }
            if (adjustedHeight > textarea.clientHeight)
            {
                $(textarea).css('height',adjustedHeight+8 +'px');
            }
        }
        else
        {
            $(textarea).css('overflow','auto');
        }

        //TODO open a chat window to the receiver
        //of  the chat message if window is minimized or closed
    },
    heartBeat : function () {
        setTimeout(function () {
            var userId = $('#current_user').val();
            if(parseInt(userId) != 0)
            {
                for( i = 0; i < chat.chatBoxes.length; i++)
                {
                    chat.loadChat(chat.chatBoxes[i],userId);
                }
            }
            chat.heartBeat();
        },5000);
    },
    initHeatBeat : function () {

    },
    loadChat : function (x,y) {
        //Todo load the chatbox
        //to the message receiver if it is not already loaded
        var $conversation = '';
        $.getJSON('/messages/user/' + x +'/'+y,function (data) {
            $.each(data.messages,function (index,chat) {
                if(chat) // fix ie bug
                {
                    if(y != chat.id)
                    {
                        if($('#chatbox-'+y).css('display')!= 'block')
                        {
                        }
                    }
                    var style = '';
                    if(y == chat.id)
                    {
                        style = 'self'
                    }
                    $conversation += '<div class="'+style+' chat-object">' +
                        '<div class="chat-user-photo" >' +
                        '<a href="/'+chat.username+'">' +
                        '<img class="" src="/users/'+chat.photo+'"> '+
                        '</a> '+
                        '</div>' +
                        '<div class="">' +
                        '<div class="chat-info">' +
                        '<div class="chat-info-inner">' +
                        '<div class="message">' +
                        '<div class="msg">' +
                        '<div class="text">' +
                        '<span>'+chat.content+'</span>' +
                        '</div> ' +
                        '</div>' +
                        '</div>'+
                        '</div> ' +
                        '<div class="clearfix"></div> ' +
                        '</div> ' +
                        '</div>' +
                        '</div>';
                }
            });
            $('#chatbox-'+x+' .chatboxcontent').html($conversation);
            chat.scrollToBottom(x);
        });
    },
    initChat : function () {
        $.getJSON('/chat/init',function (data) {
            chat.userId = data.userId;

            $.each(data.chatboxes,function (index,chatbox) {

                if(chatbox) //fixing strange ie bug
                {
                    if($('#chatbox-'+chatbox.id).length <= 0)
                    {
                        chat.createChatBox(chatbox.id,chatbox.title);
                    }
                }
            });
            chat.heartBeat();
        });
    },
    scrollToBottom : function (x) {
        $('#chatbox-'+x+' .chatboxcontent').animate({ scrollTop : $('#chatbox-'+x+' .chatboxcontent').prop('scrollHeight') },1000);
    },
    chatRequests : function () {
        setTimeout(function () {
            $.getJSON('/chat/requests',function (data) {
                var i = 0;
                $.each(data.chat_requests,function (index,chatbox) {
                    chat.createChatBox(chatbox.id,chatbox.title);
                    i++;
                });
                if(i > 0)
                {
                    chat.removeChatRequests();
                }
                chat.chatRequests();
            });
        },5000);
    },
    removeChatRequests : function () {
        $.get('chat/request/remove',function (data) {
        });
    }
}


$(document).ready(function () {
    var $userId = $('#current_user').val();
    if(parseInt($userId) != 0)
    {
        chat.originalTitle = window.title;
        chat.initChat();
        chat.chatRequests();
        //setInterval(chat.heartBeat(),chat.chatHeartbeatTime);
        $([window,document]).on('blur',function () {
            chat.windowFocus = false;
        }).on('focus',function () {
            chat.windowFocus = true;
            chat.originalTitle = window.title;
        });
    }
});

function chatWith(userId,title) {
    chat.createChatBox(userId,title);
    $('#chatbox-'+userId+ ' .chatboxtextarea').focus();
    return false;
}

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
