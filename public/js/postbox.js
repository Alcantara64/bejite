/**
 * Created by mac on 7/5/16.
 */

var PostBox = {
    form : $('#post-box'),
    removePhotos : function () {
        $('#media-photo-selected').addClass('hide');
        var textData = $('#post').val(), post = $('#post');
        this.form.resetForm();
        $('.img-count').html('');
        $('#post').attr('placeholder','What\'s on your mind?');
        post.val('');
        post.val(textData);
    },
    addPhotos : function (input) {
        var filesCount = $(input).get(0).files.length;
        $('.img-count').html(filesCount);
        $('#media-photo-selected').removeClass('hide');
        $('#post').attr('placeholder','Write something about your image(s)');
    },
    like : function (postId,type) {
        if(typeof postId != 'undefined' && typeof type !='undefined'){
            $.post('/post/like',{post_id:postId,type:type},function (data) {
                if(data.status == 'ok'){
                    var user = data.user;
                    var addMoreDiv = document.getElementById('view-all-reactors-'+postId);
                    var $template = '<li>' +
                        '<a href="'+user.username+'" data-toggle="tooltip" data-placement="top" title="'+user.fullnames+'">' +
                        '<img class="avatar-xx img-circle" src="/users/'+user.photo+'">' +
                        '<img class="type" src="/likes/'+user.type+'.png">' +
                        '</a>'+
                        '</li>';
                    $('#post-reactors-'+postId).append($template);
                    $('#like-btn-'+postId).addClass('color');
                }
            });
        }
    },
    share : function (postId) {
        if(typeof postId != 'undefined'){
            $.post('/post/share',{id:postId},function (data) {
                if(data.status == 'ok'){
                    var currentUserPhoto = $('#current_user_photo').val();
                    var post = data.post,
                        $template = postTemplate(post);
                    $('#feeds-container').prepend($template);
                    flash("Post was successfully shared on your timeline");
                }
            });
        }
    },
    hide : function (postId) {
        if(typeof postId != 'undefined') {
            swal({
                title : 'Are you sure?',
                text : 'Hidden posts will not be visible on your feed',
                showCancelButton : true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Hide it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },function () {
                $.post('/post/hide',{id:postId},function (data) {
                    if(data.status == 'ok'){
                        $('#post-'+postId).fadeOut();
                        swal('Post hidden','Post was successfully hidden');
                    }
                });
            });
        }
    },
    edit : function (postId) {
        if(typeof postId != 'undefined') {
            $.post('/post/edit',{id:postId},function (data) {
                var post = data.post;
                if(data.status == 'ok'){
                    console.log(post.content);
                    var template = '<div class="edit-post-wrapper">' +
                        '<textarea style="min-height: 150px;" class="input" placeholder="Write something..." id="edit-post-txtarea-'+postId+'">'+post.content+'</textarea>' +
                        '</div>' +
                        '<div class="edit-post-actions">' +
                        '<button type="button" class="btn btn-default btn-xs" onclick="PostBox.cancelEditPost('+postId+'); return false;"><span class="glyphicon glyphicon-remove"></span> Cancel</button> ' +
                        '<button type="button" class="edit-p-ajax btn btn btn-primary btn-xs" onclick="PostBox.editPost('+postId+'); return false;"><span class="glyphicon glyphicon-pencil"></span> Save</button> ' +
                        '</div> ';
                    $('#edit-post-'+postId).html(template).addClass('edit-post-content');
                }
            });
        }
    },
    cancelEditPost : function (postId) {
        $('#edit-post-'+postId).html("").removeClass('edit-post-content');
    },
    editPost : function (postId) {
        if(typeof postId != 'undefined'){
            var $post = $('#post-'+postId);
            var $saveBtn = $post.find('button.edit-p-ajax');
            var $saveBtnText = $saveBtn.html();
            $saveBtn.html('<span class="fa fa-spin fa-spinner"></span> Saving...');
            var content = $('#edit-post-'+postId).find('textarea').val();
            if(content != '' || content != null){
                $.post('/post/edit/feed',{id:postId,content:content},function (data) {
                    var post = data.post;
                    if(data.status == 'ok'){
                        $post.find('.post-content').html('<div class="content-text">'+post.content+'</div>' + postImages(post.image));
                        $saveBtn.html($saveBtnText);
                        PostBox.cancelEditPost(postId);
                    }
                });
            }
        }
    }, 
    delete : function (postId) {
        if(typeof postId != 'undefined') {
            swal({
                title : 'Are you sure?',
                text : 'Deleted posts can not be recovered',
                showCancelButton : true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },function () {
                $.post('/post/delete',{id:postId},function (data) {
                    if(data.status == 'ok'){
                        $('#post-'+postId).fadeOut();
                        swal('Post Deleted','Post was successfully deleted');
                    }
                });

            });
        }
    },
    report : function (postId) {
        if(typeof postId != 'undefined') {
            swal({
                title: "Report Post",
                text: "Why are you reporting this post?",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                inputPlaceholder: "Give a reason for your report..."
            },function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write something!");
                    return false
                }
                $.post('/post/report',{id:postId,reason:inputValue},function (data) {
                    if(data.status == 'ok'){
                        swal("Reported", "Thanks for reporting, we will look into it.", "success");
                    }else{
                        swal("Already Reported", data.feedback, "info");
                    }
                });

            });
        }
    },
    save : function (postId) {
        if(typeof postId != 'undefined') {
            $.post('/post/save',{id:postId},function (data) {
                if(data.status == 'ok'){
                    var btn = $('#save-post-'+postId);
                    btn.attr('onclick','PostBox.unSave('+postId+'); return false;');
                    btn.html('<span class="glyphicon glyphicon-remove-circle"></span>&nbsp;&nbsp; Unsave');
                    flash("Post saved successfully");
                    var savePostsCount = Number($('#user-saved-posts').text());
                    $('#user-saved-posts').text(savePostsCount + 1);
                }
            });
        }
    },
    unSave : function (postId) {
        if(typeof postId != 'undefined') {
            $.post('/post/unsave',{id:postId},function (data) {
                if(data.status == 'ok'){
                    var btn = $('#save-post-'+postId);
                    btn.attr('onclick','PostBox.save('+postId+'); return false;');
                    btn.html('<span class="glyphicon glyphicon-save"></span>&nbsp;&nbsp; Save');
                    flash("Post successfully unsaved");
                    var savePostsCount = Number($('#user-saved-posts').text());
                    $('#user-saved-posts').text(savePostsCount - 1);
                }
            });
        }
    },
    viewReactors : function (pid) {
        var $modal = $('#post-reactions');
        $.post('/post/get-likes',{id:pid},function (data) {
            $modal.modal('show');
            var likes = '';
            $.each(data.likes,function (index,like) {
                likes += '<li>' +
                    '<div class="user media">' +
                    '<div class="media-object pull-left">' +
                    '<a href="/'+like.username+'">' +
                    '<img class="avatar-xx circle-img" src="/users/'+like.photo+'">' +
                    '</a> ' +
                    '</div>' +
                    '<div class="media-body"> ' +
                    '<a href="/'+like.username+'">' +
                    '<h5 class="media-heading">' +
                    like.fullnames + ' <span> @'+like.username+' </span>' +
                    '</h5>' +
                    '</a>' +
                    '</div>' +
                    '</div> ' +
                    '</li>';
            });
            likes = '<ul id="reactions">'+likes+'</ul>';
            $modal.find('.modal-body').html(likes);
        });
    }
};

$(function () {
    $('#post-box-form').ajaxForm({
        buttonText : $('.post-btn','#post-box-form').html(),
        beforeSubmit : function () {
            if($('#post').val() == '' && $('#feed-photo').val() == ''){
                return false;
            }
            $('.post-btn','#post-box-form').attr('disabled','disabled');
            $('.post-btn','#post-box-form').html('<span class="fa fa-spin fa-spinner"></span> Posting...');
        },
        success : function (data) {
            $('.post-box-container').fadeOut();
            $('#post').val('');
            $('.post-btn','#post-box-form').removeAttr('disabled');
            $('.post-btn','#post-box-form').html(this.buttonText);
            PostBox.removePhotos();
            $('#post').css('height','50');
            if(data.status == 'ok'){
                var post = data.post;
                var $template = postTemplate(post);
                $('#feeds-container').prepend($template);
                flash("Post successfully published on your timeline");
            }
        },
        error : function (data) {
            var errors = '';
            var errorMessages = $.parseJSON(data.responseText);
            $.each(errorMessages, function (index,val) {
                errors += '<div>'+index+' : '+val+' </div>';
            });
            Utils.notify(errors,"Ooops some errors occurred");
            $('.post-btn','#post-box-form').html(buttonText);
        }
    });
});