<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListofJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('list_of_jobs')){
      Schema::create('list_of_jobs', function(Blueprint $table)
  {
    $table->increments('id');
    $table->string('job_name');
    $table->timestamps();
  });
}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('list_of_jobs');
    }
}
