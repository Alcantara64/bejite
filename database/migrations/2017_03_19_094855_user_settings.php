<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  if(!Schema::hasTable('user_settings')){
        Schema::create('user_settings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('follower');
			$table->string('profile_view');
			$table->string('profile_birthday');
			$table->string('profile_msg');
			$table->string('profile_notification');
			$table->string('profile_connect_with_me');
			$table->string('profile_post_comment');
			$table->string('profile_like');
			$table->string('profile_post_share');
			$table->timestamps();
		});
	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('user_settings');
    }
}
