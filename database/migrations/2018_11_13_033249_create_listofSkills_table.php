<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListofSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('listofskills')){
      Schema::create('listofskills', function(Blueprint $table)
  {
    $table->increments('id');
    $table->string('skill_name');
    $table->timestamps();
  });
}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('listofskills');
    }
}
