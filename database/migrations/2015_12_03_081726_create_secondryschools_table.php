<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondryschoolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secondryschools', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('secondary_school');
			$table->string('secondary_school_qualification');
			$table->string('secondary_school_year');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secondryschools');
	}

}
