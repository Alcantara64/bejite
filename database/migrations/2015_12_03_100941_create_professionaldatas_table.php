<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionaldatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('professionaldatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('profession_name');
			$table->string('profession_year_obtained');
			$table->string('profession_specialization');
			$table->string('profession_experience');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('professionaldatas');
	}

}
