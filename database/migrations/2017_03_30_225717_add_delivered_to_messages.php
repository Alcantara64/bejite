<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveredToMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasColumn('messages', 'delivered')){
       Schema::table('messages', function(Blueprint $table){
		  $table->string('delivered')->after('receiver_id'); 
	   });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('messages', function(Blueprint $table){
		   $table->dropColumn('delivered');
	   });
    }
}
