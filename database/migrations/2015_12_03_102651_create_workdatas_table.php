<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkdatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workdatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('work_place');
			$table->string('work_position');
			$table->string('work_sector');
			$table->string('work_experience');
			$table->string('work_reasons');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workdatas');
	}

}
