<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationaldatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('educationaldatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('university_attended');
			$table->string('degree_obtained');
			$table->string('course');
			$table->string('year_graduated');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('educationaldatas');
	}

}
