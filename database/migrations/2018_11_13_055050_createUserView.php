<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("
        CREATE  VIEW `user_job_vw` AS select `a`.`country_of_origin` AS `country_of_origin`,`j`.`user_id` AS `user_id`,`a`.`state_of_origin` AS `state_of_origin`,`a`.`lga_of_origin` AS `lga_of_origin`,`a`.`marital_status` AS `marital_status`,`a`.`dob` AS `dob`,`a`.`language_spoken` AS `language_spoken`,`a`.`complexion` AS `complexion`,`a`.`religion` AS `religion`,`b`.`university_attended` AS `university_attended`,`b`.`degree_obtained` AS `degree_obtained`,`b`.`course` AS `course`,`b`.`year_graduated` AS `year_graduated`,`c`.`name` AS `hobby`,`d`.`secondary_school` AS `secondary_school`,`d`.`secondary_school_qualification` AS `secondary_school_qualification`,`d`.`secondary_school_year` AS `secondary_school_year`,`e`.`skill_name` AS `skill_name`,`e`.`skill_year` AS `skill_year`,`f`.`work_place` AS `work_place`,`f`.`work_position` AS `work_position`,`f`.`work_sector` AS `work_sector`,`f`.`work_experience` AS `work_experience`,`j`.`job_city` AS `job_city`,`j`.`job_name` AS `job_name`,`j`.`job_sector` AS `job_sector`,`j`.`job_salary` AS `job_salary`,`j`.`job_state` AS `job_state`,`j`.`job_town` AS `job_town`,`j`.`job_type` AS `job_type` from ((((((`biodatas` `a` join `educationaldatas` `b`) join `hobbies` `c`) join `secondryschools` `d`) join `skills` `e`) join `workdatas` `f`) join `jobs` `j`) where ((`a`.`user_id` = `b`.`user_id`) and (`c`.`user_id` = `d`.`user_id`) and `j`.`user_id`);

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("
drop view if exists user_vw;
      ");
    }
}
