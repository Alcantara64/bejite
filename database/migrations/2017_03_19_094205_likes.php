<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Likes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('likes')){
       Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('post_id');
            $table->string('type');
            $table->timestamps();
        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('likes');
    }
}
