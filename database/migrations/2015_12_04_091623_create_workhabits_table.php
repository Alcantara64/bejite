<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkhabitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workhabits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('night_reading');
			$table->string('night_driving');
			$table->string('have_business');
			$table->string('drivers_license');
			$table->string('typing_speed');
			$table->string('coding_speed');
			$table->string('armed_force');
			$table->string('convicted');
			$table->string('availability');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workhabits');
	}

}
