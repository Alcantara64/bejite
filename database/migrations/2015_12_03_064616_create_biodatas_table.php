<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiodatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('biodatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('country_of_origin');
			$table->string('state_of_origin');
			$table->string('lga_of_origin');
			$table->string('marital_status');
			$table->string('dob');
			$table->string('language_spoken');
			$table->string('language_proficiency');
			$table->string('complexion');
			$table->string('height');
			$table->string('physical_features');
			$table->string('tribe');
			$table->string('religion');
			$table->string('race');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('biodatas');
	}

}
