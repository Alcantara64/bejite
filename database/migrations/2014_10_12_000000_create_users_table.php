<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fullnames');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('username',100);
			$table->string('photo');
			$table->string('account_type',50);
			$table->string('phone',50);
			$table->string('account_status',50);
			$table->string('reg_status',50);
			$table->string('address',50);
			$table->string('country',50);
			$table->string('bday',50);
			$table->string('bmonth',50);
			$table->string('byear',50);
			$table->string('state',50);
			$table->string('city',50);
			$table->string('time',255);
			$table->text('chats');
			$table->string('hear_about_us');
			$table->string('gender',50);
			$table->string('status',10);
			$table->string('saved_posts');
			$table->string('hidden_posts');
			$table->string('cover_photo');
			$table->text('summary');
			$table->string('is_online');
			$table->string('newsletter',50);
			$table->string('connect_notify',50);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
