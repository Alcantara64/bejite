<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationaldatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vacationaldatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('school');
			$table->string('qualification');
			$table->string('year');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vacationaldatas');
	}

}
