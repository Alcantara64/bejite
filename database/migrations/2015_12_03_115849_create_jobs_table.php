<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('job_country');
			$table->string('job_state');
			$table->string('job_name');
			$table->string('job_type');
			$table->string('job_sector');
			$table->string('job_lga');
			$table->string('job_salary');
			$table->string('job_city');
			$table->string('job_town');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
