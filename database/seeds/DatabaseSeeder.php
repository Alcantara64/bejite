<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'firstname' => 'John',
            'lastname' => 'Doe',
            'email' => 'admin@bejite.com',
            'password' => bcrypt('secret'),
            'country' => 'Nigeria',
            'state' => 'Rivers',
            'city' => 'Port Harcourt',
            'photo' => 'avatar.jpg',
            'permissions' => '',
            'gender' => 'Male',
        ]);
    }
}
