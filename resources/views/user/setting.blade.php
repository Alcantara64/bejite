@extends('layouts.app')

@section('content')
@if(Auth::user()->account_type == 'jobseeker')

@endif
@if (count($errors) > 0)
                        <div class="alert alert-danger red-line">
                            <strong>Whoops!</strong> Fix the following errors and continue.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
@endif
<div class="box panel panel-default">
            <div class="panel-heading">
              My Settings
            </div>
            <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/settings') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Profile Photo</label>
                            <div class="col-md-5">
                                <input type="file" name="photo">
                            </div>
                            <div class="col-md-4" style="text-align: left;">
                                <img src="{{ asset('/users/'.Auth::user()->photo) }}" class="small-round-avatar">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="text" class="input form-control validate[required]" name="firstname" value="{{ $names[0] }}" placeholder="Firstname">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="input form-control validate[required]" value="{{ $names[1] }}" placeholder="Lastname" name="lastname">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="email" class="input form-control validate[required]" name="email" value="{{ Auth::user()->email }}" placeholder="E-Mail Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="address" class="input form-control validate[required]" name="address" value="{{ Auth::user()->address }}" placeholder="Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="text" class="input form-control validate[required]" name="city" value="{{ Auth::user()->city }}" placeholder="City">
                            </div>
                            <div class="col-md-6">
                                <input type="number" class="input form-control validate[required]" name="phone" value="{{ Auth::user()->phone }}" placeholder="Phone Number">
                            </div>
                        </div>

                        @if(Auth::user()->connect_notify == 'yes')
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="newsletter" value="yes" checked="">
                            Email me Bijte weekly Digest and Newsletters
                          </label>
                        </div>
                        @else
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="newsletter" value="yes">
                            Email me Bijte weekly Digest and Newsletters
                          </label>
                        </div>
                        @endif
                        @if(Auth::user()->newsletter == 'yes')
                        <div class="checkbox" style="margin-bottom: 20px;">
                          <label>
                            <input type="checkbox" name="connect_notify" value="yes" checked="">
                            Email me when a user add me to their connections
                          </label>
                        </div>
                        @else
                         <div class="checkbox" style="margin-bottom: 20px;">
                          <label>
                            <input type="checkbox" name="connect_notify" value="yes">
                            Email me when a user add me to their connections
                          </label>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="ibutton button2">
                                    <span class="glyphicon glyphicon-pencil"></span> Save
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
    </div>
</div>
@endsection
