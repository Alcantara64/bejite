@extends('layouts.app')

@section('content')

            @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Fix the following errors and continue.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            <div class="box panel panel-default">
                <div class="panel-heading">Change Password</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/change-password') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-2 control-label">Current password</label>
                            <div class="col-md-9">
                                <input type="password" class="input form-control validate[required]" name="current_password" placeholder="Current Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="input form-control validate[required]" name="password" placeholder="New Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="input form-control validate[required]" name="password_confirmation" placeholder="Re-type New Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="ibutton button2 btn-block">
                                    <span class="glyphicon glyphicon-pencil"></span> Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
@endsection
