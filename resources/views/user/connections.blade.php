@extends('layouts.app')

@section('content')
	<div class="col-md-12 connections-panel">
		<div class="col-md-6 col-md-offset-3 connections-elements">
				<div class="connections-box-title">
						<h2>Connections</h2>
				</div>
				<div class=" clearfix connections-box my-connections">
					<div class=" box-content">
						@foreach(\App\User::getConnections() as $connection)
							<div class="connection-item" id="user-div-{{ $connection->id }}">
								<div class="pull-left" style="margin-right: 10px;">
									<a href="/{{ $connection->username }}">
										<img class="avatar-xx circle-img" src="{{ asset('/users/'.$connection->photo) }}">
									</a>
								</div>
								<div class="pull-left fullnames">
									<a href="/{{ $connection->username }}">
										{{ ucfirst(explode(" ", $connection->fullnames)[0]) }}
									</a>
									<p><a href="/{{ $connection->username }}">
										{{  $connection->account_type }}
									</a></p>

								</div>
								 <div class="pull-right" style="padding-top: 5px;padding-bottom: 5px;;">
									<button type="button" class="btn btn-primary btn-xs" id="connect-btn-{{ $connection->id }}" onclick="unFollow({{ $connection->id }},this);"><span class="glyphicon glyphicon-transfer"></span><strong> Disconnect </strong></button>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
			</div>
		</div>
	</div>


@endsection
