@extends('layouts.app')

@section('content')
    <!--<div class="profile-bg">
        <div class="profile-bg-img">
            @if($profile->cover_photo != '')
                <img id="user-custom-bg" src="/uploads/cover/{{ $profile->cover_photo }}" alt="Custom background image" style="top: 0px; transform: translateY(2.5px);height: 100%;">
                @else
                <img id="user-custom-bg" alt="Custom background image" class="hide" style="top: 0px; transform: translateY(2.5px);height: 100%;">
            @endif
        </div>
    </div>-->
    <div class="container">
        <div class="row" style="margin-top:20px;">
            <div class="span left-column pull-left">
                @include('partials.profile')
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-info box">
                                <div class="background-label">
                                    Background
                                </div>
                                <div class="p-content">
                                    @if(!empty($profile->summary))
                                        <div class="summary">
                                            <header>
                                                <span class="fa fa-info-circle fa-2x" style="margin-right: 40px;"></span>
                                                <span class="title">Summary</span>
                                            </header>
                                            {{ $profile->summary }}
                                        </div>
                                    @endif

                                    @if($profile->account_type == 'jobseeker' && Auth::user()->account_type == 'jobseeker')
                                            @if($hasSkills > 0)
                                                <div class="summary">
                                                    <header>
                                                        <span class="fa fa-briefcase fa-2x" style="margin-right: 40px;"></span>
                                                        <span class="title">Skills</span>
                                                    </header>
                                                    <div class="profile-skills">
                                                        <?php $i = 0; ?>
                                                        @foreach($skills as $skill)
                                                            <?php $i++; ?>
                                                            <div class="skill">
                                                                <span class="label label-custom">{{ $i }}</span>
                                                                <span class="label label-primary shift-left-5">{{ $skill->skill_type }}</span>
                                                                <span class="skill-title shift-left-5">{{ $skill->skill_name }}</span>
                                                                <span class="label label-success shift-left-5">{{ $skill->skill_year }}</span>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif

                                            @if($hasWork > 0)
                                                <div class="summary">
                                                    <header>
                                                        <span class="fa fa-archive fa-2x" style="margin-right: 40px;"></span>
                                                        <span class="title">Work Experience</span>
                                                    </header>
                                                    <div class="profile-skills">
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Place of work</th>
                                                                <th>Position</th>
                                                                <th>Sector</th>
                                                                <th>Experience</th>
                                                                <th>Reasons</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i = 0; ?>
                                                            @foreach($work as $wk)
                                                                <?php $i++; ?>
                                                                <tr>
                                                                    <td><span class="label label-custom">{{ $i++ }}</span></td>
                                                                    <td>{{ $wk->work_place }}</td>
                                                                    <td>{{ $wk->work_position }}</td>
                                                                    <td>{{ $wk->work_sector }}</td>
                                                                    <td>{{ $wk->work_experience }} yrs</td>
                                                                    <td>{{ $wk->work_reasons }}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="summary">
                                                <header>
                                                    <span class="fa fa-user-secret fa-2x" style="margin-right: 40px;"></span>
                                                    <span class="title">Work Habbits</span>
                                                </header>
                                                <div class="profile-skills">

                                                    <div class="skill">
                                                        <span class="label label-primary">1</span>
                                                        <span class="skill-title shift-left-5">Can read at night? </span>
                                                        @if($habbit->night_reading =='yes')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Yes</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> No</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">2</span>
                                                        <span class="skill-title shift-left-5">Can drive at night? </span>
                                                        @if($habbit->night_driving =='yes')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Yes</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> No</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">3</span>
                                                        <span class="skill-title shift-left-5">Has an established business? </span>
                                                        @if($habbit->have_business =='yes')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Yes</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> No</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">4</span>
                                                        <span class="skill-title shift-left-5">Have drivers license? </span>
                                                        @if($habbit->drivers_license =='yes')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Yes</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> No</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">5</span>
                                                        <span class="skill-title shift-left-5">How fast I can type? </span>
                                                        @if($habbit->typing_speed =='very fast')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Very Fast</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> Not Very Fast</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">6</span>
                                                        <span class="skill-title shift-left-5">Ever been in the arm forces? </span>
                                                        @if($habbit->armed_force =='yes')
                                                            <span class="label label-custom shift-left-5"><span class="fa fa-thumbs-up"></span> Yes</span>
                                                        @else
                                                            <span class="label label-warning shift-left-5"><span class="fa fa-thumbs-down"></span> No</span>
                                                        @endif
                                                    </div>
                                                    <div class="skill">
                                                        <span class="label label-primary">7</span>
                                                        <span class="skill-title shift-left-5">I am available to start work </span>
                                                        <span class="label label-success shift-left-5"><span class="fa fa-thumbs-up"></span> {{ ucwords($habbit->availability) }}</span>

                                                    </div>
                                                </div>
                                            </div>

                                            @if($hasJob > 0)
                                                <div class="summary">
                                                    <header>
                                                        <span class="fa fa-chain-broken fa-2x" style="margin-right: 40px;"></span>
                                                        <span class="title">Job Opportunity {{ $profile->fullnames }} is looking for:</span>
                                                    </header>
                                                    <div class="profile-skills">
                                                        <div class="skill">
                                                            <span class="skill-title"> Title </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_name) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> Type </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_type) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> Sector </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_sector) }} </span>
                                                        </div>
                                                       
                                                        <div class="skill">
                                                            <span class="skill-title"> Country </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_country) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> State </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_state) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> L.G.A </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_lga) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> City </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_city) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> Town </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_town) }} </span>
                                                        </div>
                                                        <div class="skill">
                                                            <span class="skill-title"> Expected Salary </span>
                                                            <span class="label label-custom shift-left-5"> &nbsp;&nbsp;&nbsp;{{ ucwords($job->job_salary) }} </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-column pull-left">
                @include('partials.connections')
                @include('partials.suggested-connections')
				@include('partials.chat')
            </div>
        </div>
    </div>
    
    <input type="hidden" value="{{ $profile->username }}" id="profile-username">
@endsection