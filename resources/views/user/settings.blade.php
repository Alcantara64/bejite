@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="span left-column pull-left">
                <div class="box profile-settings" style="min-height: 100px;;">
                    <div class="settings-nav pull-left padded">
                        <ul>
                            @if($flag == '')
                                <li class="active"><a href="{{ action('UserController@profileSettings') }}">Profile Information</a> </li>
                                <li class="divider"></li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings') }}">Profile Information</a> </li>
                                <li class="divider"></li>
                            @endif
                            @if($flag == 'change-password')
                                <li class="active"><a href="{{ action('UserController@profileSettings','change-password') }}">Change Password</a> </li>
                                <li class="divider"></li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings','change-password') }}">Change Password</a> </li>
                                <li class="divider"></li>
                            @endif
                            @if($flag == 'privacy')
                                <li class="active"><a href="{{ action('UserController@profileSettings','privacy') }}">Privacy</a> </li>
                                <li class="divider"></li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings','privacy') }}">Privacy</a> </li>
                                <li class="divider"></li>
                            @endif
                            @if($flag == 'notifications')
                                <li class="active"><a href="{{ action('UserController@profileSettings','notifications') }}">Notifications</a> </li>
                                <li class="divider"></li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings','notifications') }}">Notifications</a> </li>
                                <li class="divider"></li>
                            @endif
                            @if($flag == 'invite-friends')
                                <li class="active"><a href="{{ action('UserController@profileSettings','invite-friends') }}">Invite Friends</a> </li>
                                <li class="divider"></li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings','invite-friends') }}">Invite Friends</a> </li>
                                <li class="divider"></li>
                            @endif
                            @if($flag == 'delete-account')
                                <li class="active"><a href="{{ action('UserController@profileSettings','delete-account') }}">Delete My Account</a> </li>
                            @else
                                <li><a href="{{ action('UserController@profileSettings','delete-account') }}">Delete My Account</a> </li>
                            @endif
                        </ul>
                    </div>
                    <div class="settings-content pull-left">
                        <div class="padded">

                            @if($flag == '')
                                <div class="settings-title">
                                    <h3>Profile Information</h3>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-data" role="form" method="POST" action="{{ action('UserController@updateUserData') }}">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <button data-target="photo-upload" class="file-upload btn btn-primary" type="button"><span class="glyphicon glyphicon-cloud-upload"></span> Change Photo</button>
                                            </div>
                                            <div class="col-md-6">
                                                <img class="current-avatar avatar-m circle-img pull-right" src="{{ asset('users/'.Auth::user()->photo) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Profile Summary</label>
                                                <textarea name="summary" class="input auto-grow" placeholder="Write something interesting about yourself" maxlength="1500">{{ Auth::user()->summary }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Fullnames</label>
                                                <input type="text" class="input" name="fullnames" placeholder="Fullnames" value="{{ Auth::user()->fullnames }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Email Address</label>
                                                <input type="text" class="input" name="email" placeholder="Email Address" value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Username</label>
                                                <input type="text" class="input" name="username" placeholder="Username" value="{{ Auth::user()->username }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Country</label>
                                                <select name="country" id="country" class="input form-control  validate[required]" onchange="print_state('state', this.selectedIndex);">
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">State</label>
                                                <select name="state" class="input form-control  validate[required]" id="state">
                                                    <option value="">Choose State</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">City</label>
                                                <input type="text" class="input" name="city" placeholder="City" value="{{ Auth::user()->city }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Mobile Number</label>
                                                <input type="text" class="input" name="phone" placeholder="Mobile Number" value="{{ Auth::user()->phone }}" maxlength="15">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Birthday</label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">
                                                <select class="input validate[required]" name="bday">
                                                    @for($i = 1; $i < 31; $i++)
                                                        @if(Auth::user()->bday == $i)
                                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="input validate[required]" name="bmonth">
                                                    @foreach(App\Common::getMonths() as $key => $val)
                                                        @if(Auth::user()->bmonth == $key)
                                                            <option value="{{ $key }}" selected>{{ $val }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $val }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="input validate[required]" name="byear">
                                                    @for($i = 1940; $i < date('Y'); $i++)
                                                        @if(Auth::user()->byear == $i)
                                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    <form class="form-validate" id="change-photo-form" role="form" method="POST" action="{{ action('UploadController@profilePhoto') }}" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="file" name="photo" data-target-form="#change-photo-form" data-target-img=".current-avatar" class="profile-photo-upload" id="photo-upload" style="display: none">
                                    </form>
                                </div>
                            @elseif($flag == 'change-password')
                                <div class="settings-title">
                                    <h3>Change Password</h3>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-change-password" role="form" method="POST" action="{{ action('UserController@changePassword') }}">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Current Password</label>
                                                <input type="password" class="input validate[required]" name="pass" placeholder="Current Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">New Password</label>
                                                <input type="password" class="input validate[required]" name="password" placeholder="New Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Confirm Password</label>
                                                <input type="password" class="input validate[required]" name="password_confirmation" placeholder="Confirm Password" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"><span class="fa fa-key"></span> Change Password</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @elseif($flag == 'privacy')
                                <div class="settings-title">
                                    <h3>Privacy Settings</h3>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-privacy" role="form" method="POST" action="{{ action('UserController@updatePrivacy') }}">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Who can view my profile</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-view">
                                                    @if(Auth::user()->settings()->profile_view == 'everyone')
                                                        <option value="everyone">Everyone</option>
                                                        <option value="connections">Connections Only</option>
                                                    @else
                                                        <option value="connections">Connections Only</option>
                                                        <option value="everyone">Everyone</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Who can see your birth date</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-birthday">
                                                    @if(Auth::user()->settings()->profile_birthday =='everyone')
                                                        <option value="everyone">Everyone</option>
                                                        <option value="connections">Connections Only</option>
                                                    @else
                                                        <option value="connections">Connections Only</option>
                                                        <option value="everyone">Everyone</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Who can send me message</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-msg">
                                                    @if(Auth::user()->settings()->profile_msg =='connections')
                                                        <option value="connections">Connections Only</option>
                                                        <option value="nobody">Nobody</option>
                                                    @else
                                                        <option value="nobody">Nobody</option>
                                                        <option value="connections">Connections Only</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Email Notifications</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-notification">
                                                    @if(Auth::user()->settings()->profile_notification =='yes')
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No</option>
                                                    @else
                                                        <option value="no">No</option>
                                                        <option value="yes">Yes</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"><span class="fa fa-pencil"></span> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            @elseif($flag == 'notifications')
                                <div class="settings-title">
                                    <h3>Notifications Settings</h3>
                                    <p>Send me notifications when:</p>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-notifications" role="form" method="POST" action="{{ action('UserController@updateNotifications') }}">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Someone connect with me:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-connect-with-me">
                                                    @if(Auth::user()->settings()->profile_connect_with_me =='yes')
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No</option>
                                                    @else
                                                        <option value="no">No</option>
                                                        <option value="yes">Yes</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Comment on your post</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-post-comment">
                                                    @if(Auth::user()->settings()->profile_post_comment =='yes')
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No</option>
                                                    @else
                                                        <option value="no">No</option>
                                                        <option value="yes">Yes</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Like your posts,photos,etc.</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-like">
                                                    @if(Auth::user()->settings()->profile_like =='yes')
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No</option>
                                                    @else
                                                        <option value="no">No</option>
                                                        <option value="yes">Yes</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Someone share your posts</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="input" name="profile-post-share">
                                                    @if(Auth::user()->settings()->profile_post_share =='yes')
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No</option>
                                                    @else
                                                        <option value="no">No</option>
                                                        <option value="yes">Yes</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"><span class="fa fa-pencil"></span> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            @elseif($flag == 'invite-friends')
                                <div class="settings-title">
                                    <h3>Search your address book for friends</h3>
                                    <p>Choosing a service will open a window for you to log in securely and import your contacts </p>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-privacy" role="form" method="POST" action="">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <img src="{{ asset('img/facebook.png') }}"> <span>Facebook</span>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-primary btn-outline pull-right">Find Friends</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <img src="{{ asset('img/gmail.png') }}"> <span>Gmail</span>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-primary btn-outline pull-right">Find Friends</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label" style="padding-bottom: 10px;">Invite by email address</label>
                                                <textarea class="input" style="min-height:150px;" name="emails" placeholder="Enter your friends emails seperated with (,)"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"> Send Invitations </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @elseif($flag == 'delete-account')
                                <div class="settings-title">
                                    <h3>Delete my account</h3>
                                    <p>To delete your account provide your current password and note that you cannot undo this action </p>
                                </div>
                                <div class="setting-content">
                                    <form class="form-validate form-horizontal" id="settings-delete-account" role="form" method="POST" action="{{ action('UserController@deleteAccount') }}">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="password" name="password" class="input" placeholder="Enter your Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary save" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete My Account </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            @endif

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="right-column pull-left">
                @include('partials.connections')
                @include('partials.suggested-connections')
				@include('partials.chat')
            </div>
        </div>
    </div>
    
@endsection