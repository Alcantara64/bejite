@extends('layouts.app')
@section('content')
    <div class="container" style="margin-top: 10px;">
        <div class="row">
            <div class="span left-column pull-left">
                <div class="box messages-wrapper">
                    <div class="usr_container">
                        <div class="heading">
                            Conversations
                            <button type="button" class="btn btn-primary btn-xs pull-right">
                                <span class="fa fa-pencil-square-o"></span>
                            </button>
                        </div>
                        <div id="connection-lists">
                            <div class="user-search">
                                <input type="text" class="input search" name="user-search" placeholder="Search" style="width:80%;">
                                <button class="btn btn-xs btn-primary" class="sort" data-sort="fullnames">
                                    <span class="fa fa-search"></span>
                                </button>
                            </div>
                            <ul class="sort-list list" id="topics">
                                @if(count(Auth::user()->getConnections()) > 0)
                                    @foreach(Auth::user()->getConnections() as $key => $user)
                                        <li id="{{ $user->id }}" class="{{ App\Http\Controllers\MessageController::setCssClass($key) }}">
                                            <div class="message-item">
                                                <div class="photo pull-left">
                                                    <img class="avatar-m circle-img" src="{{ asset('/users/'.$user->photo) }}">
                                                </div>
                                                <div class="content pull-left">
                                                    <label class="fullnames">{{ ucwords($user->fullnames) }}</label>
                                        <span id="last-msg-{{ $user->id }}">
                                            @if(\App\Messages::hasLastMessage($user->id))
                                                {{ App\Messages::getLastMessage($user->id) }}
                                            @endif
                                        </span>
                                                    @if(\App\Messages::hasLastMessage($user->id))
                                                        <div class="timestamp">
                                                            <time class="timeago" datetime="{{ App\Messages::getLastMessagetime($user->id) }}"></time>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li>
                                        <div class="no-connection">
                                            There is no one here at the momemt,
                                            try making some connections and come back here
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="msg_container msg_container_topic div">
                        <div class="heading">
                            <div id="msg-username" class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default">New</button>
                                    <button type="button" class="btn btn-default"><span class="fa fa-trash"></span> Delete Conversation</button>
                                </div>
                            </div>
                        </div>
                        <ul id="topic_messages" style="margin-top:40px;"></ul>
                    </div>
                    <div class="send_container">
                        <div class="wrap">
                            <textarea placeholder="Write a reply" id="send_to_topic_message"></textarea>
                            <div class="toolbar">
                                <a href="">
                                    <span class="fa fa-camera"></span> Add Photos
                                </a>
                                <a href="#" onclick="javascript:void(0);">
                                    <span class="fa fa-smile-o"></span> Smileys
                                </a>
                                <span style="margin-left: 40px;color: #aaaaaa;">Press Enter to send</span>
                                <input type="checkbox" id="press-enter">
                                @if(count(Auth::user()->getConnections()) > 0)
                                    <button type="button" id="send_to_topic" class="btn btn-primary btn-xs pull-right">Reply </button>
                                @else
                                    <button type="button" id="send_to_topic" class="btn btn-primary btn-xs pull-right" disabled>Reply </button>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="right-column pull-left">
                @include('partials.recent-connections')
                @include('partials.suggested-connections')
            </div>
        </div>
    </div>

@endsection