@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:20px;">
    <div class="row">



                        <div class="col-md-offset-1 col-md-10">
                        <ul id="progressbar">
                        <li class="active">Personal Details</li>
                        <li class="active">Upload Photo</li>
                        <li class="active">Discover</li>
                        </ul>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Discover people, follow at least two people to continue.
                            </div>
                            <div class="panel-body">
                            <form class="form-horizontal form-validate" role="form" method="POST" action="{{ url('/discover/finish') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @foreach($users as $user)
                                <div class="media">
                                  <div class="media-left">
                                    <a href="#">
                                        <img src="{{ asset('users/'.$user->photo) }}" class="avatar-md media-object">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">
                                        {{ ucwords($user->fullnames) }}
                                    </h4>
                                  </div>

                                  <div class="pull-right media-right">
                                  @if(!$user->isFollowing($user->id))
                                      <button data-user-id="{{ $user->id }}" id="follow-btn-{{ $user->id }}" type="button" class="btn btn-primary btn-sm follow"><span class="glyphicon glyphicon-user"></span> Follow</button>
                                      <button data-user-id="{{ $user->id }}" id="unfollow-btn-{{ $user->id }}" type="button" class="btn btn-primary btn-sm unfollow hide"><span class="glyphicon glyphicon-user"></span> Following</button>
                                   @else
                                    <button data-user-id="{{ $user->id }}" id="unfollow-btn-{{ $user->id }}" type="button" class="btn btn-primary btn-sm unfollow"><span class="glyphicon glyphicon-user"></span> Following</button>
                                    <button data-user-id="{{ $user->id }}" id="follow-btn-{{ $user->id }}" type="button" class="btn btn-primary btn-sm follow hide"><span class="glyphicon glyphicon-user"></span> Follow</button>
                                   @endif
                                  </div>

                                </div>
                            @endforeach
                            <button style="margin-top:50px;" type="submit" class="btn btn-primary btn-block" id="finish-btn" disabled>Finish</button>
                            </form>
                        </div>
                    </div>
                        </div>


    </div>
    </div>

@endsection
