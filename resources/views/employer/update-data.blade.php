@extends('layouts.app')

@section('content')
    <div class="row">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('EmployerController@saveData') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-offset-2 col-md-10">
                            <div class="form-group">
                            <label class="col-md-3 control-label">Company Logo</label>
                            <div class="col-md-4">
                                <input type="file" name="logo">
                            </div>
                            <div class="col-md-5" style="text-align: left;">
                                <img class="employer-logo-big" src="{{ $logo }}" alt="Company logo">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                @if($employer->type == 'company')
                                    <label class="radio-inline">
                                      <input type="radio" name="account_type" value="company" checked="checked"> We are a company
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="account_type" value="individual"> I am an Individual
                                    </label>
                                @else
                                     <label class="radio-inline">
                                      <input type="radio" name="account_type" value="company"> We are a company
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="account_type" value="individual" checked> I am an Individual
                                    </label>
                                @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <input type="text" class="input form-control validate[required]" name="company" value="{{ $employer->company }}" placeholder="Company Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <input type="text" class="input form-control validate[required]" name="address1" value="{{ $employer->address1 }}" placeholder="Company Address Line 1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <input type="text" class="input form-control validate[required]" name="address2" value="{{ $employer->address2 }}" placeholder="Company Address Line 2">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="text" class="input form-control" name="website" value="{{ $employer->website }}" placeholder="Website">
                            </div>
                            <div class="col-md-5">
                                <input type="text" min-value="1" class="input form-control validate[required]" name="support_email" value="{{ $employer->support_email }}" placeholder="Company Support email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <input type="text" class="input form-control validate[required]" name="position" value="{{ $employer->position }}" placeholder="what is your position in the company?">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="ibutton button2">
                                    <span class="glyphicon glyphicon-pencil"></span> Save
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
    </div>
@endsection
