@extends('layouts.app-admin')

@section('content')

    <div class="admin-content-container" style="border:none;">
        <div class="row">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#list" aria-controls="list" role="tab" data-toggle="tab">Sliders</a></li>
                <li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">Add New</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="list">

                    <div class="table-responsive">
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Heading</th>
                                <th>Description</th>
                                <th>Order</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="sliders-container">
                            <?php $i = 0; ?>
                            @foreach ($carousels as $carousel)
                            <?php $i++; ?>
                            <tr id="row-{{ $carousel->id }}">
                                <td>{{ $i }}</td>
                                <td><img class="carousel-preview" src="/uploads/sliders/{{ $carousel->img }}"></td>
                                <td>{{ $carousel->heading }}</td>
                                <td>{{ $carousel->text }}</td>
                                <td><span class="label label-primary">{{ $carousel->display_order }}</span></td>
                                <td>
                                    @if($carousel->status == 'published')
                                        <span class="label label-success">{{ $carousel->status }}</span>
                                    @else
                                        <span class="label label-danger">{{ $carousel->status }}</span>
                                    @endif
                                </td>

                                <td>
                                    <button type="button" class="btn btn-primary btn-xs" onclick="window.location='/admin/sliders/{{ $carousel->id }}/edit'"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                                    <button type="button" class="btn btn-danger btn-xs" onclick="javascript:deleteSlider({{ $carousel->id }});"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="create">

                        <form id="add-slider" enctype="multipart/form-data" class="form-validate form-horizontal" role="form" method="POST" action="{{ action('AdminController@saveSlider') }}">
                            {!! csrf_field() !!}

                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Image:</label>
                        <div class="col-md-8">
                            <input type="file" class="validate[required]" name="img">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Heading:</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('heading') }}" class="input validate[required]" name="heading" placeholder="Caption Heading" maxlength="255">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Sub heading:</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('text') }}" class="input validate[required]" name="text" placeholder="Caption Description" maxlength="50">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Description:</label>
                        <div class="col-md-8">
                            <textarea class="input" name="content" placeholder="Slider Description (Optional) 200 characters maximum" maxlength="200" style="min-height: 50px;">{{ old('content') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Order:</label>
                        <div class="col-md-8">
                            <input type="number" min="1" value="{{ old('order') }}" class="input validate[required]" name="order" placeholder="Display Order" maxlength="3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Status:</label>
                        <div class="col-md-8">
                            <select name="status" class="input validate[required]">
                                <option value="">Choose Status</option>
                                <option value="published">Publish</option>
                                <option value="draft">Draft</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="action" class="control-label col-sm-1"></label>
                        <div class="col-md-10">
                            <button class="custom-button" type="submit"><span class="glyphicon glyphicon-plus"></span> Create </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection