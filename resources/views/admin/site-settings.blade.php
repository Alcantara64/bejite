@extends('layouts.app-admin')

@section('content')

<div class="admin-content-container">
    <h1 class="page-header">Site Settings</h1>

    <div class="row">

        <form id="site-settings" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@saveSiteSettings') }}">
            {!! csrf_field() !!}
        <div class="form-group">
            <label for="name" class="control-label col-sm-1">Logo:</label>
            <div class="col-md-5">
                <input type="file" name="logo">
                <input type="hidden" name="test" value="hello">
            </div>
            <div class="col-md-6">
                <?php if(!empty(App\Option::get('logo'))){ ?>
                <img class="logo-preview site-logo" src="{{ asset('/logo/'.App\Option::get('logo')) }}" alt="Site logo">
                <?php } ?>
            </div>
        </div>
        <div class="form-group">
            <label for="keywords" class="control-label col-sm-1">Keywords:</label>
            <div class="col-md-8">
                <textarea name="option[keywords]" class="input validate[required]" placeholder="Site Meta Keywords">{{ \App\Option::get('keywords') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="control-label col-sm-1">Descriptions:</label>
            <div class="col-md-8">
                <textarea name="option[description]" class="input validate[required]" placeholder="Site Meta Descriptions" style="min-height: 150px;">{{ \App\Option::get('description') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="keywords" class="control-label col-sm-1">Address:</label>
            <div class="col-md-8">
                <textarea name="option[address]" class="input validate[required]" placeholder="Contact address">{{ \App\Option::get('address') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="telephone" class="control-label col-sm-1">Telephone:</label>
            <div class="col-md-8">
                <input type="text" name="option[telephone]" value="{{ \App\Option::get('telephone') }}" class="input validate[required]" placeholder="Contact Telephone number">
            </div>
        </div>
        <div class="form-group">
            <label for="telephone" class="control-label col-sm-1">Support Email:</label>
            <div class="col-md-8">
                <input type="text" name="option[support_email]" value="{{ \App\Option::get('support_email') }}" class="input validate[required]" placeholder="Support email">
            </div>
        </div>
        <div class="form-group">
            <label for="fax" class="control-label col-sm-1">Fax:</label>
            <div class="col-md-8">
                <input type="text" name="option[fax]" value="{{ \App\Option::get('fax') }}" class="input validate[required]" placeholder="Fax Number">
            </div>
        </div>
        <div class="form-group">
            <label for="fax" class="control-label col-sm-1">Slogan:</label>
            <div class="col-md-8">
                <input type="text" name="option[slogan]" value="{{ \App\Option::get('slogan') }}" class="input validate[required]" placeholder="Sites Slogan">
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="control-label col-sm-1">Descriptions:</label>
            <div class="col-md-8">
                <textarea name="option[disclaimer]" class="input validate[required]" placeholder="Site Disclaimer" style="min-height: 150px;">{{ \App\Option::get('disclaimer') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="action" class="control-label col-sm-1"></label>
            <div class="col-md-8">
                <button class="custom-button" type="submit"><span class="glyphicon glyphicon-pencil"></span> Update</button>
            </div>
        </div>

        </form>

    </div>
</div>
@endsection