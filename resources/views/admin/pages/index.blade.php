@extends('layouts.app-admin')

@section('content')

    <div class="admin-content-container" style="border:none;">
        <div class="row">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#list" aria-controls="list" role="tab" data-toggle="tab">Pages</a></li>
                <li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">Create Page</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="list">

                    <div class="table-responsive">
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>Menu Order</th>
                                <th>Menu name</th>
                                <th>Position</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="pages-container">
                            <?php $i = 0;  ?>
                             @foreach($pages as $page)
                            <?php $i++; ?>
                            <tr id="row-{{ $page->id }}">
                                <td><?= $i; ?></td>
                                <td>{{ $page->title }}</td>
                                <td><span class="label label-primary">{{ $page->page_order }}</span></td>
                                <td>{{ $page->name }}</td>
                                <td><span class="label label-primary">  {{ ucfirst($page->position) }}</span></td>
                                <td>
                                    @if($page->status == 'published')
                                    <span class="label label-success">{{ $page->status }}</span>
                                    @else
                                    <span class="label label-danger">{{ $page->status }}</span>
                                    @endif
                                </td>

                                <td>
                                    <button type="button" class="btn btn-primary btn-xs" onclick="window.location='/admin/pages/{{ $page->id }}/edit'"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                                    <button type="button" class="btn btn-danger btn-xs" onclick="javascript:deletePage({{ $page->id }});"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="create">

                    <form id="add-page" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@savePage') }}">
                        {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">title:</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('title') }}" class="input validate[required]" name="title" placeholder="Page title" maxlength="255">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Name:</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('name') }}" class="input validate[required]" name="name" placeholder="Menu name" maxlength="255">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Order:</label>
                        <div class="col-md-8">
                            <input type="number" min="1" value="{{ old('page_order') }}" class="input validate[required]" name="page_order" placeholder="Menu Order" maxlength="3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Status:</label>
                        <div class="col-md-8">
                            <select name="status" class="input validate[required]">
                                <option value="">Choose Status</option>
                                <option value="published">Publish</option>
                                <option value="draft">Draft</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Parent Menu:</label>
                        <div class="col-md-8">
                            <select name="parent" class="input">
                                <option value="0">Choose Menu</option>
                                @foreach ($menus as $menu)
                                <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Page Position:</label>
                        <div class="col-md-8">
                            <select name="position" class="input">
                                <option value="">Choose Position</option>
                                <option value="top">Top</option>
                                <option value="bottom">Bottom</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-1">Content:</label>
                        <div class="col-md-8">
                            <textarea name="content" class="editor input validate[required]" placeholder="Page Body" style="min-height: 300px;">{{ old('content') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="action" class="control-label col-sm-1"></label>
                        <div class="col-md-10">
                            <button class="custom-button" type="submit"><span class="glyphicon glyphicon-plus"></span> Create</button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection