@extends('layouts.app-admin')

@section('content')
    <div class="admin-content-container" style="border:none;">
        <h1 class="page-header">Profile Settings</h1>
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Change Password</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form id="admin-change-password" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@changePassword') }}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label for="name" class="control-label col-sm-2">Current Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="input validate[required]" name="pass" placeholder="Current Password" maxlength="15">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label col-sm-2">New Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="input validate[required]" name="password" placeholder="New Password" maxlength="15">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label col-sm-2">Confirm Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="input validate[required]" name="password_confirmation" placeholder="Confirm Password" maxlength="15">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="action" class="control-label col-sm-2"></label>
                                    <div class="col-md-10">
                                        <button class="custom-button" type="submit"><span class="glyphicon glyphicon-pencil"></span> Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection