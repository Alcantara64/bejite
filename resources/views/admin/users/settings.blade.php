@extends('layouts.app-admin')

@section('content')
    <div class="admin-content-container" style="border:none;">
    <h1 class="page-header">Profile Settings</h1>
    <div class="row">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-heading">Change Photo</div>

                <div class="panel-body">
                    <div class="col-md-6">
                        <form id="change-photo" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@changeProfilePhoto',$user->id) }}">
                        {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="custom-button" id="change-photo-btn" type="button"> <span class="glyphicon glyphicon-cloud-upload"></span> Upload Photo</button>
                                    <input type="file" class="hide" name="photo" id="photo-holder">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <img class="profile-photo avatar-m img-circle" src="{{ asset('users/'.Auth::user()->photo) }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Information</div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <form id="profile-settings" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@saveProfileSettings',$user->id) }}">
                        {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name" class="control-label col-sm-1">Firstname:</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ $user->fullnames }}" class="input validate[required]" name="fullnames" placeholder="First name" maxlength="120">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label col-sm-1">Email:</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ $user->email }}" class="input validate[required]" name="email" placeholder="Email Address" maxlength="120">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country" class="control-label col-sm-1">Country:</label>
                                <div class="col-md-8">
                                    <select name="country" id="country" class="input form-control  validate[required]" onchange="print_state('states', this.selectedIndex);">
                                        <option value="{{ $user->country }}">{{ $user->country }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="stateoforigin" class="control-label col-sm-1">State:</label>
                                <div class="col-md-8">
                                    <select name="state" class="input form-control  validate[required]" id="states">
                                        <option value="{{ $user->state }}">{{ $user->state }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label col-sm-1">City:</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ $user->city }}" class="input validate[required]" name="city" placeholder="City" maxlength="255">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label col-sm-1">Mobile Number:</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ $user->phone }}" class="input validate[required]" name="phone" placeholder="Mobile Number" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label col-sm-1">Address:</label>
                                <div class="col-md-8">
                                    <input type="text" value="{{ $user->address }}" class="input validate[required]" name="address" placeholder="Address" maxlength="255">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="action" class="control-label col-sm-1"></label>
                                <div class="col-md-10">
                                    <button class="custom-button" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    </div>
@endsection