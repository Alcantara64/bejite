@extends('layouts.app-admin')

@section('content')

    <h1 class="page-header">Manage Users</h1>

    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped data-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Photo</th>
                    <th>Fullnames</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                 @foreach ($users as $user)
                <?php $i++; ?>
                    <tr id="row-{{ $user->id }}">
                        <td>{{ $i }}</td>
                        <td><img class="avatar-xs img-circle" src="/users/{{ $user->photo }}"></td>
                        <td>{{ $user->fullnames }}</td>
                        <td>{{ $user->email }}</td>
                        <td><span class="label label-info"><?= ucfirst($user->account_type); ?></span></td>
                        <td>
                            @if($user->status == 'active')
                            <span class="label label-success">{{ ucfirst($user->status) }}</span>
                            @else
                            <span class="label label-danger">{{ ucfirst($user->status) }}</span>
                            @endif
                        </td>
                        <td>
                            @if($user->account_type!='admin')
                            <button type="button" class="btn btn-danger btn-xs" onclick="javascript:deleteUser({{ $user->id }});"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection