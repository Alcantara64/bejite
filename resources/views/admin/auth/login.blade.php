<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta content="Paradise Ekpereta" name="author">
    <title> My App - Sign In</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/validationEngine.jquery.css') }}" rel="stylesheet">
    <link href="{{ asset('css/validationEngine.jquery.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/signin.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Fonts -->
    <!--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">

    <form id="admin-login" class="form-validate form-horizontal" role="form" method="POST" action="{{ action('AdminAuthController@postLogin') }}">
        {{ csrf_field() }}
    <div class="space"></div>
    <div id="login-error"></div>
    <h2 class="form-signin-heading sr-only">Admin area</h2>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" value="{{ old('email') }}" class="input form-control  validate[required]" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" class="input form-control validate[required]" placeholder="Password">
    <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
    </form>

</div> <!-- /container -->
<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.validationEngine.js') }}"></script>
<script src="{{ asset('js/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('js/admin.js') }}" type="text/javascript"></script>
</body>
</html>