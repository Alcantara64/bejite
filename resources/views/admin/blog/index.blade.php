@extends('layouts.app-admin')

@section('content')

    <div class="admin-content-container" style="border:none;">
        <div class="row">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#list" aria-controls="list" role="tab" data-toggle="tab">Posts</a></li>
                <li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">New Post</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="list">

                    <div class="table-responsive">
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Thumb</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="posts-container">
                            <?php $i = 0;  ?>
                            @foreach($posts as $post)
                                <?php $i++; ?>
                                <tr id="row-{{ $post->id }}">
                                    <td><?= $i; ?></td>
                                    <td><img class="carousel-preview" src="{{ asset('posts/'.$post->thumb) }}"></td>
                                    <td>{{ $post->title }}</td>
                                    <td>
                                        @if($post->status == 'published')
                                            <span class="label label-success">{{ $post->status }}</span>
                                        @else
                                            <span class="label label-danger">{{ $post->status }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $post->created_at->toDateTimeString() }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-xs" onclick="window.location='/admin/blog/posts/{{ $post->id }}/edit'"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                                        <button type="button" class="btn btn-danger btn-xs" onclick="javascript:deletePost({{ $post->id }});"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="create">

                    <form id="add-post" enctype="multipart/form-data" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('BlogController@addNew') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Thumbnail:</label>
                            <div class="col-md-8">
                                <input type="file" class="validate[required]" name="img">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Title:</label>
                            <div class="col-md-8">
                                <input type="text" value="{{ old('title') }}" class="input validate[required]" name="title" placeholder="Post title" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Status:</label>
                            <div class="col-md-8">
                                <select name="status" class="input validate[required]">
                                    <option value="">Choose Status</option>
                                    <option value="published">Publish</option>
                                    <option value="draft">Draft</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Content:</label>
                            <div class="col-md-8">
                                <textarea name="content" class="editor input validate[required]" placeholder="Post Body" style="min-height: 300px;">{{ old('content') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="action" class="control-label col-sm-1"></label>
                            <div class="col-md-10">
                                <button class="custom-button" type="submit"><span class="glyphicon glyphicon-plus"></span> Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection