@extends('layouts.app-admin')

@section('content')

    <div class="admin-content-container" style="border:none;">
        <div class="row">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#list" aria-controls="list" role="tab" data-toggle="tab">Posts</a></li>
                <li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">New Post</a></li>
                <li role="presentation" class="active"><a href="#edit" aria-controls="edit" role="tab" data-toggle="tab">Edit Post</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane" id="list">

                    <div class="table-responsive">
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Thumb</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="posts-container">
                            <?php $i = 0;  ?>
                            @foreach($posts as $post)
                                <?php $i++; ?>
                                <tr id="row-{{ $post->id }}">
                                    <td><?= $i; ?></td>
                                    <td><img class="carousel-preview" src="/posts/{{ $post->thumb }}"></td>
                                    <td>{{ $post->title }}</td>
                                    <td>
                                        @if($post->status == 'published')
                                            <span class="label label-success">{{ $post->status }}</span>
                                        @else
                                            <span class="label label-danger">{{ $post->status }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $post->created_at->toDateTimeString() }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-xs post" onclick="window.location='/admin/blog/{{ $post->id }}/edit'"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                                        <button type="button" class="btn btn-danger btn-xs post" onclick="javascript:deletePost({{ $post->id }});"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="create">

                    <form id="add-post" enctype="multipart/form-data" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('BlogController@addNew') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Thumbnail:</label>
                            <div class="col-md-8">
                                <input type="file" class="validate[required]" name="img">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Title:</label>
                            <div class="col-md-8">
                                <input type="text" value="{{ old('title') }}" class="input validate[required]" name="title" placeholder="Post title" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Status:</label>
                            <div class="col-md-8">
                                <select name="status" class="input validate[required]">
                                    <option value="">Choose Status</option>
                                    <option value="published">Publish</option>
                                    <option value="draft">Draft</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Content:</label>
                            <div class="col-md-8">
                                <textarea name="content" class="editor input validate[required]" placeholder="Post Body" style="min-height: 300px;">{{ old('content') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="action" class="control-label col-sm-1"></label>
                            <div class="col-md-10">
                                <button class="post custom-button" type="submit"><span class="glyphicon glyphicon-plus"></span> Create</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div role="tabpanel" class="tab-pane active" id="edit">

                    <form id="edit-post" enctype="multipart/form-data" class="form-horizontal form-validate" role="form" method="POST" action="{{ action('BlogController@updatePost',$p->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Thumbnail:</label>
                            <div class="col-md-5">
                                <input type="file" class="" name="img">
                            </div>
                            <div class="col-md-6">
                                <img class="img-circle" style="width: 50px;height: 50px;" src="{{ asset('uploads/posts/'.$p->thumb) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Title:</label>
                            <div class="col-md-8">
                                <input type="text" value="{{ $p->title }}" class="input validate[required]" name="title" placeholder="Post title" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Status:</label>
                            <div class="col-md-8">
                                <select name="status" class="input validate[required]">
                                    @if($p->status == 'published')
                                        <option value="published">Publish</option>
                                        <option value="draft">Draft</option>
                                        @else
                                        <option value="draft">Draft</option>
                                        <option value="published">Publish</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-1">Content:</label>
                            <div class="col-md-8">
                                <textarea name="content" class="editor input validate[required]" placeholder="Post Body" style="min-height: 300px;">{{ $p->content }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="action" class="control-label col-sm-1"></label>
                            <div class="col-md-10">
                                <button class="custom-button" type="submit"><span class="glyphicon glyphicon-plus"></span> Update </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection