@extends('layouts.app')

@section('content')
    <div class="homepage-hero">
        <div class="container">
            <div class="jumbotron">
                <div >
                    <div class="col-md-12" >
                        <div >
                                <h2 class="hero-title">
                                    <strong>Social</strong> Job Hunt
                                </h2>
                               <p><h3 style="color: #ffffff;"> Join the world's smartest job network. Find jobs, recruit job seekers, find business mentors and partners.</h3> </p>
                            </div>
                        <div class="col-md-6" >
                            <div >

							<div class="signup-hero">
           <!-- <div class="modal-content splash-panel">-->
                <!--<div class="modal-body header-less-modal">-->
                   <!--<div class="col-md-6 col-sm-12">
                        <h4 class="modal-title" id="myModalLabel">Sign in to with..</h4>
                        <a class="btn btn-block btn-social btn-facebook btn-lg">
                            <span class="fa fa-facebook"></span> Sign in with Facebook
                        </a>
                        <a class="btn btn-block btn-social btn-google btn-lg">
                            <span class="fa fa-google-plus"></span> Sign in with Google+
                        </a>
                        <a class="btn btn-block btn-social btn-linkedin btn-lg">
                            <span class="fa fa-linkedin"></span> Sign in with Linkedin
                        </a>
                    </div>-->
                    <!--<div class="col-md-5  col-sm-5 login-column" >-->
                        <h3 class="modal-title" id="myModalLabel">Log in with email</h3>
                        <form id="signin-form" class="form-validate" role="form" method="POST" action="{{ url('/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div id="signin-error"></div>
                            <div class="form-group">
                                <input type="email" class="input form-control" name="email" value="{{ old('email') }}" placeholder="Email Address">
                            </div>

                            <div class="form-group">
                                <input type="password" class="input form-control" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <button type="submit" class="signin ibutton button1"><span class="fa fa-key"></span> Login</button>

                                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
					</div>
					</div>

                                <div class="col-md-6">
									<div class="signup-hero">
                                    <div class="signup-title">
                                        First time on Bejite?
                                    </div>
                                    <!--<a class="btn btn-block btn-social btn-facebook btn-lg" style="margin-bottom:10px;">
                                        <span class="fa fa-facebook"></span> Sign up
                                    </a>-->
                                    <form id="signup-form-step1" class="form-validate" role="form" method="POST" action="{{ action('UserAuthController@join') }}">
                                        <div id="signup-error"></div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="form-group">
                                            <input type="text" class="input form-control validate[required]" name="fullnames" value="{{ old('fullnames') }}" placeholder="Full Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="input form-control  validate[required]" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="input form-control  validate[required]" name="password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="input form-control  validate[required]" name="password_confirmation" placeholder="Confirm Password">
                                        </div>



                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="Male" checked> Male
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="Female"> Female
                                            </label>
                                        </div>
										<div class="form-group">
                                            <input type="text" class="hidden-input" name="usermail">
                                        </div>
                                        <div class="form-group">
                                            <div class="">
                                                <button type="submit" class="signup ibutton button2 btn-block"><span class="glyphicon glyphicon-user"></span> Sign Up</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="lgrey sign-form__agreement" style="color:#000000;">By continuing, you're confirming that you've read and agreed to our <a target="_blank" class="lgrey link" href="terms-and-conditions" style="color:#f10084;">Terms and Conditions</a>, <a target="_blank" class="lgrey link" href="privacy-policy" style="color:#f10084;">Privacy Policy</a> and <a target="_blank" class="lgrey link" href="cookie-policy" style="color:#f10084;">Cookie Policy</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                    <!--
            <div class="col-md-4">
                <img class="animated bounceInDown" src="{{ asset('img/professional2.png') }}" alt="Professional">
            </div>
            -->
                <footer class="page-footer font-small white pt-4">
        <div class="row" id="">
        @if(Auth::check())
         <!--   <div class="col-sm-4">
                <ul class="links pull-left">
                    <li><a href="/">Home</a></li> |
                    <li><a href="privacy-policy" target="_blank">Privacy Policy</a></li>
                    <li><a href="about" target="_blank">About Bejite</a></li>
                </ul>
                <ul class="links pull-left">
                    <li><a href="terms-and-conditions" target="_blank">Terms and Conditions</a></li>
                </ul><br>
                <ul class="links pull-left">
                    <li><a href="security-advice" target="_blank">Security Advice</a></li>
                </ul><br>
                <ul class="links pull-left">
                    <li><a href="how-to-use-bejite" target="_blank">How to use Bejite</a></li>
                </ul><br>
                <ul class="links pull-left">
                    <li><a href="cookie-policy" target="_blank">Cookie Policy</a></li>
                </ul><br>
                <ul class="links pull-left">
                    <li>All rights reserved 2017</li><br>
                    <li>&copy; bejite.com by <a href="http://solprogroup.com" target="_blank">SolPro</a></li>
                </ul>
            </div>-->
        @else
        <div class="container-fluid text-center text-md-left">
            <div class="row">
            <div class="col-sm-3">
                <ul class="front-links pull-left list-unstyled">
                    <li><a class="white-color"href="/">Home</a></li>
                    <li><a class="white-color"href="privacy-policy" target="_blank">Privacy Policy</a></li>
                    <li><a class="white-color"href="about" target="_blank">About Bejite</a></li>

                </ul>
                </div>
            <div class="col-sm-4">

                <ul class="link-group pull-left list-unstyled" style="display:inline;color:white">

                    <li ><a class="white-color"style="display:inline;color:white" href="how-to-use-bejite" target="_blank">How to use Bejite</a></li>
                    <li><a href="cookie-policy"class="white-color" target="_blank">Cookie Policy</a></li>
                </ul>
                </div>
                <div class="col-sm-4">

                <ul class="front-links pull-left list-unstyled">
                    <li><a href="how-to-use-bejite"class="white-color" target="_blank">How to use Bejite</a></li>
                    <li><a href="security-advice"class="white-color" target="_blank">Security Advice</a></li>

                </ul>
                </div>
            </div>
            </div>
        @endif

        </div>
        <div class="row">
        <div class="footer-copyright text-center py-3 white-color">All rights reserved 2017 &copy; bejite.com by <a href="http://solprogroup.com" target="_blank">SolPro</a></div>
        </div>
    </footer>
                </div>

            </div>
           </div>
    </div>

@endsection
