@extends('layouts.app')

@section('content')

        <div class="box panel panel-default">
            <div class="panel-heading">
                <h4>Site Settings</h4>
            </div>
            <div class="panel-body">
           <form class="form-horizontal form-validate" role="form" method="POST" action="{{ url('admin/save-site-settings') }}" enctype="multipart/form-data">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="col-md-12">
                   <div class="form-group">
                        <label for="fullnames" class="control-label col-sm-2">Logo:</label>
                        <div class="col-md-4">
                            <input type="file" name="logo">
                        </div>
                        <div class="col-md-4">
                          <img class="logo-img" src="{{ asset('logo/'.$g_option->logo) }}" alt="Bejite Logo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Site Description:</label>
                        <div class="col-md-10">
                            <textarea name="descriptions" class="input validate[required] form-control" placeholder="Site Description">{{ $option->descriptions }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Site Keywords:</label>
                        <div class="col-md-10">
                            <textarea name="keywords" class="input validate[required] form-control" placeholder="Site Keywords">{{ $option->keywords }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="action" class="control-label col-sm-2"></label>
                        <div class="col-md-10">
                            <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                        </div>
                    </div>

               </div>
           </form>
           </div>
        </div>

@endsection