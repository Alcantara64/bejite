@extends('layouts.app')

@section('content')

   <div class="box panel panel-default">
     <div class="panel-heading"><h4> Manage Employers</h4></div>
     <div class="panel-body">
        <table class="table table-striped">
          <thead>
              <tr>
                  <th>#</th>
                  <th>title</th>
                  <th>name</th>
                  <th>order</th>
                  <th>slug</th>
                  <th>location</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              @foreach ($pages as $page)
                <tr>
                <td style="display: none;">{{ $i++ }}</td>
                  <td>{{ $i }}</td>
                  <td>{{ $page->title }}</td>
                  <td>{{ $page->name }}</td>
                  <td>
                      <span class="label label-primary">{{ ucfirst($page->page_order) }}</span>
                  </td>
                  <td>{{ $page->slug }}</td>
                  <td> {{ $page->location }}</td>
                  <td>
                        <a class="ibutton button2 button-sm btn-xs" href="{{ action('AdminController@getEditPage', $page->id) }}"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                        <a class="ibutton btn-red button-sm btn-xs" href="{{ action('AdminController@getDeletePage', $page->id) }}"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
     </div>
   </div>
@endsection