@extends('layouts.app')

@section('content')

        <div class="panel panel-red">
            <div class="panel-heading">
                <h4><span class="glyphicon glyphicon-plus"></span> Create Page</h4>
            </div>
            <div class="panel-body">
           <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('AdminController@postUpdatePage',$page->id) }}" enctype="multipart/form-data">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="col-md-12">
                   <div class="form-group">
                        <label for="fullnames" class="control-label col-sm-2">Title:</label>
                        <div class="col-md-10">
                            <input type="text" class="input form-control validate[required]" name="title" value="{{ $page->title }}" placeholder="Page title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fullnames" class="control-label col-sm-2">Menu name:</label>
                        <div class="col-md-10">
                            <input type="text" class="input form-control validate[required]" name="name" value="{{ $page->name }}" placeholder="Enter Menu name eg. About Us">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fullnames" class="control-label col-sm-2">Options:</label>
                        <div class="col-md-5">
                            <input type="text" class="input form-control validate[required]" name="order" value="{{ $page->page_order }}" placeholder="Order">
                        </div>
                        <div class="col-md-5">
                            <select name="location" class="input form-control validate[required]">
                                @if($page->location == 'bottom')
                                  <option value="bottom">Bottom</option>
                                  <option value="top">Top</option>
                                @else
                                  <option value="top">Top</option>
                                  <option value="bottom">Bottom</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Content:</label>
                        <div class="col-md-10">
                            <textarea name="content" class="editor input validate[required] form-control" placeholder="Page Content" style="min-height: 250px;">{{ $page->content }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="action" class="control-label col-sm-2"></label>
                        <div class="col-md-10">
                            <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                        </div>
                    </div>

               </div>
           </form>
           </div>
        </div>

@endsection