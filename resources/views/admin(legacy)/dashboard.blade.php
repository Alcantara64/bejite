@extends('layouts.app')
@section('content')
<div class="panel panel-red">
    <div class="panel-heading">
      <h4>Users Statistics</h4>
    </div>
    <div class="panel-body">
        <div class="col-md-3">
            <div class="stat-box stat-green">
               <div class="item">
               <div style="margin-top: -20px;margin-bottom: 10px;">Today</div>
                 <div>2345</div>
               </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-box stat-red">
               <div class="item">
               <div style="margin-top: -20px;margin-bottom: 10px;">This week</div>
                 <div>2345</div>
               </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-box stat-blue">
               <div class="item">
               <div style="margin-top: -20px;margin-bottom: 10px;">This month</div>
                 <div>{{ $d }}</div>
               </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-box stat-red">
               <div class="item">
               <div style="margin-top: -20px;margin-bottom: 10px;">All time</div>
                 <div>{{ $g_allUsersCount }}</div>
               </div>
            </div>
        </div>
    </div>
</div>
@endsection