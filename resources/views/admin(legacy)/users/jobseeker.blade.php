@extends('layouts.app')

@section('content')

   <div class="box panel panel-default">
     <div class="panel-heading">Manage Jobsekkers</div>
     <div class="panel-body">
        <table class="table table-striped">
          <tbody>
              @foreach ($jobseekers as $user)
                <tr>
                <td style="display: none;">{{ $i++ }}</td>
                  <td>{{ $i }}</td>
                  <td>{!! Auth::user()->getUserPhoto($user->id,'avatar-xs-circle') !!}</td>
                  <td>
                      @if($user->account_status == 'active')
                        <span class="label label-info">{{ ucfirst($user->account_status) }}</span>
                      @else
                        <span class="label label-danger">{{ ucfirst($user->account_status) }}</span>
                      @endif
                  </td>
                  <td>{{ $user->fullnames }}</td>
                  <td>{{ $user->phone }}</td>
                  <td>{{ $user->email }}</td>
                  <td><span class="label label-info">{{ ucfirst($user->account_type) }}</span></td>
                  <td>
                  <div class="dropdown">
                      <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-toggle="tooltip" data-placement="bottom" title="More actions"><span class="glyphicon glyphicon-option-vertical font-btn"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                          @if($user->account_status == 'active')
                            <a href="{{ action('AdminController@getDeactivateUser', $user->id) }}"><span class="glyphicon glyphicon-user"></span> Deactivate</a>
                          @else
                            <a href="{{ action('AdminController@getActivateUser', $user->id) }}"><span class="glyphicon glyphicon-user"></span> Activate</a>
                          @endif
                        </li>
                        <li><a href="" id="delete-user"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
     </div>
   </div>

@endsection