@extends('layouts.app')

@section('content')

   <div class="box panel panel-default">
     <div class="panel-heading">Manage Employers</div>
     <div class="panel-body">
        <table class="table table-striped">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Logo</th>
                  <th>Employer</th>
                  <th>Status</th>
                  <th>Email</th>
                  <th>Website</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              @foreach ($employers as $user)
                <tr>
                <td style="display: none;">{{ $i++ }}</td>
                  <td>{{ $i }}</td>
                  <td><img class="logo-sm" src="/images/logo-image/{{ $user->logo }}" alt="{{ $user->company }} Logo"></td>
                  <td>{{ $user->company }}</td>
                  <td>
                      @if($user->account_status == 'active')
                        <span class="label label-primary">{{ ucfirst($user->account_status) }}</span>
                      @else
                        <span class="label label-danger">{{ ucfirst($user->account_status) }}</span>
                      @endif
                  </td>
                  <td>{{ $user->support_email }}</td>
                  <td><a href="{{ $user->website }}"> {{ $user->website }} </a></td>
                  <td>
                      @if($user->account_status == 'active')
                        <a class="btn btn-warning btn-xs" href="{{ action('AdminController@getDeactivateUser', $user->user_id) }}"><span class="glyphicon glyphicon-user"></span> Deactivate</a>
                      @else
                        <a class="btn btn-success btn-xs" href="{{ action('AdminController@getActivateUser', $user->user_id) }}"><span class="glyphicon glyphicon-user"></span> Activate</a>
                      @endif
                      <button type="button" id="delete-user" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
     </div>
   </div>
@endsection