@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<div class=" other-pages-content">
			<h3><strong>HOW TO USE BEJITE</strong></h3><hr>
			<div class="how-to-use-content">
				<p>
				<strong><a href="/" target="_blank">www.bejite.com</a> (for Employers and jobseekers)</strong>
				</p>
				<p><strong>Let's explain <a href="/" target="_blank">www.bejite.com</a>:</strong></p>
				<p>
					Bejite is a social job hunt site that connects jobseekers and employers. On bejite.com, users also
					find mentors, business partners, etc. The platform is designed to fight/eliminate unemployment 
					world-over with a smart recruitment search engine that allows employers to search out their desired
					candidate(s) in less than 1min. The search engine makes your job as a recruiter very easy and 
					straightforward. For instance, if you are looking for a female of 20yrs of age; who studied Marketing,
					from the university of Nigeria, graduated in 2014; who wants to work in the ICT industry; she wants 
					20k salary monthly; lives in Lagos; she must be from Enugu state; fair in complexion; speaks Yoruba; 
					has MS excel knowledge, etc.; simply enter all these tiny details in the search engine and click on 
					search, all the candidates that filled their CVs with the details will appear. You can send them 
					invites to do a physical interview or interview them on the platform immediately using the real-time
					chat (just the way you chat on Facebook). Either way, you have saved yourself time and money in
					downloading tonnes of CVs through your email and cracking your brain to select the people for 
					interviews from your piles of Cvs. Bejite takes care of that part. This means that there will be no 
					need posting job ads as this attracts both the qualified and the unqualified people to you (God help
					you if the unqualified are far more than the qualified). In fact, jobseekers on 
					<a href="/" target="_blank">www.bejite.com</a> stand 
					70-90% chances of securing their desired jobs faster than others not on bejite.com.
				</p>
				<p>
					www.bejite.com is also a social networking platform. We often hear that today's graduates are not 
					employable. <a href="/" target="_blank">www.bejite.com</a> asks why and what can be done? Therefore, 
					<a href="/" target="_blank">www.bejite.com</a> provides that 
					mentoring platform that connects jobseekers and employers together. Employers now have the chance 
					to directly tell jobseekers what exactly they require in a prospective candidate. This will 
					challenge the decaying roots of our educational system in Nigeria and elsewhere around the globe.
					With <a href="/" target="_blank">www.bejite.com</a>, we will have a better federal character and
					reduced needless job racketeering 
					in the civil service. Here, with a big screen connected to a system, everyone can see the 
					recruitment process. It is that simple. No more hide and employ game. No more backyard employments.
					Again, Police, Immigration and NAF and other Nigerian agencies will benefit immensely from the 
					platform. Immigration will never result to peoples deaths if the interviews are conducted state by
					state, local govt by local govt. <a href="/" target="_blank">www.bejite.com</a> makes that possible, faster, and easier. They will
					also save funds spent on those long and annoying recruitment processes of theirs.<br>
					Like all other social networking platforms, <a href="/" target="_blank">www.bejite.com</a>
					allows you to post pictures, comments, 
					shares memories, etc. So, you can build your brand through your connections and expand your reach.
					It is that simple.<br>
					The site is in its testing (beta) stage that will last for 6months (March - August, 2017). 
					It is best used on a computer. It is free to sign up and free to use!
					Don't forget to sign up on your laptop. Share with friends.

				</p>
			</div>
		</div>
	</div>
</div>

@endsection