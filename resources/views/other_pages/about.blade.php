@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<div class=" other-pages-content">
			<h3><strong>ABOUT BEJITE</strong></h3><hr>
			<div class="about-bejite-contentm">
				<h3>Bejite.com</h3>
				<p><h3>Your next hire is here.</h3></p>
				<p>
					Bejite is an authority in job sourcing and helps millions of job seekers and employers find the 
					right fit every day. At Bejite, you have the rare opportunity of chatting with a 
					prospect/jobseeker/employer and getting hired immediately. The opportunity Bejite gives jobseekers
					is beyond their wildest dreams as they connect to their favourite employers and other jobseekers 
					to learn, share and network easily. Start hiring now on the world's best job site!
				</p>
				<p><strong>Why Post a Job?</strong></p>
				<p>
					Yes. With bejite.com, you do not have any need to post a job. That will be a waste of your 
					precious time. We aim at filling those vacancies with your first few clicks instead of waiting 
					for 3 months to fill an important vacancy.
				</p>
				<p><strong>The right fit for your jobs</strong></p>
				<p>
					Millions of people visit bejite.com every month, giving you access to the best talents in every field.
				</p>
				<p><strong>On desktop</strong></p>
				<p>Over 80% of job searches are desktop. Our mobile app will be announced in due course.</p>
				<p><strong>More quality hires</strong></p>
				<p>
					Bejite is the best job site external source of hire and provides twice more hires than any 
					other job site existing on the internet. In fact, bejite is the fastest means of bridging the 
					gap between an employer and an employee in any field, anywhere.
				</p>
				<p><h3>How bejite.com helps you hire</h3></p>
				<p><strong>Reach the most job seekers today</strong></p>
				<p>
					Create and log in into your account as an employer. Visit the advanced search page and fill in 
					the required fields and qualifications you want your new hire to have. Click search and all the
					people who met the criteria will appear. Conduct your interview online through our dedicated 
					chat system or simply call them and conduct the physical interview.
				</p>
				<p>Most of our users prefer the online chat as the first step of interview. It is easy and FREE!</p>
				<p>
					Create an account and enter your job description. Review applications, manage candidates 
					and schedule interviews from your bejite account. It is that simple!
				</p>
				<p><strong>Reach the most Employers today</strong></p>
				<p>
					Create and log in into your account as a jobseeker and follow as many employers as you wish. 
					This will give you the double privileges of having a personal relationship with the employer 
					as well as be in the know of the latest developments in the sector you have chosen to work in.
					Ensure that you fill-in all the fields in your profile as this will expose you more to employers 
					who may desire your services. You can also connect to other jobseekers to share information and 
					desires.
				</p>
				<p>
					Create an account and enter your job description. Review applications, manage offers and 
					interviews from your bejite account.
				</p>
				<p><strong>Elevate your Employer and Jobseeker brand</strong></p>
				<p>
					With more than 10 million employer/jobseeker reviews, company/jobseeker pages give people 
					insights into potential employers and jobseekers and help you create a memorable candidate 
					experience. Build your talent brand for free with your Bejite Personal/Company Page.
				</p>
				<p><h3>Get started with Bejite.com</h3></p>
				<p>Socialize. Hire. Share in 2 minutes!</p>
			</div>
		</div>
	</div>
</div>

@endsection