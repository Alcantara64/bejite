@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<div class=" other-pages-content">
			<h3><strong>BEJITE PRIVACY POLICY</strong></h3><hr>
			<div>
				<ol>
					<li><strong>INRODUCTION</strong>
						<div>
							SolPro Innovative Technologies Limited ('SolPro') is responsible for the maintenance 
							and management of www.bejite.com which help employers/employees find suitable employees
							and willing employers from every part of the world.
						</div>
						<div class="privacy-list-items">
							<ol>
								<li><strong>Who should read this privacy policy?</strong>
									<p>
										You may read our privacy policy as it may be relevant to you, especially 
										if you are user of the website services as:
									</p>
									<ul>
										<li>an employer;</li>
										<li>a worker;</li>
										<li>a job seeker;</li>
										<li>an employment service provider;</li>
										<li>a participant in a program or service delivered by us;</li>
										<li>a contractor, consultant, or supplier of goods or services to us;</li>
										<li>an applicant for a grant;</li>
										<li>a policy stakeholder who works with us;</li>
										<li>a person whose information may be given to us by a third party, 
											including other Employment Agencies
										</li>
										<li>an academic or researcher;</li>
										<li>an entrant in a competition conducted by us;</li>
										<li>a person seeking employment with us and through us;</li>
										<li>a current or past employee of the SolPro;</li>
										<li>any other individual whose personal information we may collect or hold.</li>
									</ul>
								</li>
								<li><strong>Purpose of this privacy policy</strong>
									<p>The purpose of this privacy policy is to:</p>
									<ul>
										<li>describe the types of personal information that we collect, hold, use 
											and disclose;
										</li>
										<li>outline our personal information handling systems and practices;</li>
										<li>enhance the transparency of our management of personal information;</li>
										<li>explain our authority to collect your personal information, why it 
											may be held by us, how it is used and how it is protected;
										</li>
										<li>notify whether we are likely to disclose personal information to other 
											recipients and, if so, to whom;
										</li>
										<li>provide information on how you can access your personal information,
											correct it if necessary and complain if you believe it has been wrongly 
											collected or inappropriately handled.
										</li>	
									</ul>
									<p>This privacy policy has been developed to as much areas as legally required.</p>
								</li>
								<li><strong>Privacy Act 1988</strong>
									<p>Term Definition:</p>
									<p> 'Personal information' means information (or an opinion), whether true or 
										not, in any form that can identify a living person.
									</p>
								</li>
								<li><strong>Information covered under this privacy policy</strong>
									<p>
										This privacy policy has been developed in accordance with the applicable 
										laws of the federal republic of Nigeria and embodies our commitment to 
										protecting the privacy of personal information.
									</p>
									<p>
										It covers how we collect and handle personal information, including 
										sensitive information.
									</p>
									<p>
										'Sensitive information' means personal information about you that is of a 
										sensitive nature, including information about health, genetics, biometrics 
										or disability; racial or ethnic origin; religious, political or 
										philosophical beliefs; professional association or trade union memberships,
										sexuality; or criminal record. Special requirements apply to the collection
										and handling of sensitive information.
									</p>
									<p>
										This privacy policy is not intended to cover our handling of 
										commercially sensitive information or other information that is not 
										personal information as defined in the Privacy Act.
									</p>
								</li>
							</ol>
						</div>
					</li>
					<li><strong>OUR PERSONAL INFORMATION HANDLING PRACTICES</strong>
						<div>
							<ol>
								<li><strong>Collection of personal information</strong>
									<p>
										Personal information may be collected directly by us, or by people or 
										organisations acting on our behalf (e.g. contracted service providers). 
										It may be collected directly from you, or on your behalf from a 
										representative you have authorised.
									</p>
									<p>
										We may also obtain personal information collected by other Employment 
										agencies, state or territory governments, other third parties, or from
										publicly available sources. This will only occur where you consent, where 
										it is unreasonable or impractical to collect the information only from you 
										or where we are required or authorised to do so by law.
									</p>
									<p>
										We are also authorised to collect personal information (which may also be 
										defined as 'protected information' where relevant) under a range of 
										applicable laws. 
									</p>
									<p>
										Under the Nigeria's Privacy Policy Act, we will only collect information for 
										a lawful purpose that is reasonably necessary for, or directly related to, 
										one or more of our functions and activities under the portfolio legislation
										listed above, or where otherwise required or authorised by law.
									</p>
									<p>
										When we collect personal information, we are required under the NPPA to 
										notify you of a number of matters. These include the purposes for which we 
										collect the information, whether the collection is required or authorised 
										by law, and any person or body to whom we usually disclose the information,
										including if those persons or bodies are located overseas. We usually 
										provide this notification by including privacy notices on our paper based 
										forms and online portals.
									</p>
								</li>
								<li><strong>Types of personal information collected by us</strong>
									<p>We collect and hold a broad range of personal information in records relating to:</p>
									<ul>
										<li>employment and personnel matters for our staff and contractors 
											(including security assessments);
										</li>
										<li>the performance of our legislative and administrative functions;</li>
										<li>individuals participating in our funded programs and initiatives;</li>
										<li>correspondence from members of the public to us.</li>
										<li>complaints (including privacy complaints) made and feedback provided to 
											us; and
										</li>
										<li>the provision of legal advice by internal and external lawyers.</li>
									</ul>
									<p>This personal information may include but is not limited to:</p>
									<ul>
										<li>your name, address and contact details (e.g. phone, email and fax);</li>
										<li>photographs, video recordings and audio recordings of you;</li>
										<li>
											information about your personal circumstances (e.g. marital status, 
											age, gender, occupation, accommodation and relevant information about 
											your partner or children);
										</li>
										<li>
											information about your financial affairs (e.g. payment details, 
											bank account details and information about business and financial 
											interests);
										</li>
										<li>
											information about your identity (e.g. date of birth, country of 
											birth, passport details, visa details, drivers licence);
										</li>
										<li>information about your employment (e.g. work history, referee comments,
											remuneration);
										</li>
										<li>
											information about your background (e.g. educational qualifications, the 
											languages you speak and your English proficiency);
										</li>
										<li>
											government identifiers (e.g. National Identification Number, Tax 
											Identification Number, International Passport Number, etc);
										</li>
										<li>information about assistance provided to you under our funding 
											arrangements; 
										</li>
									</ul>
								</li>
								<li><strong>Collection of sensitive information</strong>
									<p>
										In carrying out our functions and activities we may collect personal 
										information that is sensitive information (see section 1.4). The NPPA 
										impose additional obligations on us when collecting, using or disclosing 
										sensitive information. We may only collect sensitive information from you:
									</p>
									<ul>
										<li>where you provide your consent; or</li>
										<li>where required or authorised by law; or</li>
										<li>where a permitted general situation exists such as to prevent a serious 
											threat to safety .
										</li>
									</ul>
									<p>
										We also collect sensitive information where authorised to do so for the 
										purposes of human resource management, fraud investigations, taking 
										appropriate action against suspected unlawful activity or serious 
										misconduct, and responding to inquiries by courts, tribunals and other 
										external review bodies.
									</p>
								</li>
								<li><strong>Collecting personal information from children and young people</strong>
									<p>
										In carrying out our functions and activities we may collect personal 
										information about children and young people, either directly from them, 
										through their parents or guardians, or from their education providers.
										Where children and young people are over the age of 16, our general policy 
										is to collect information directly from them as they are likely to have the
										capacity to understand any privacy notices provided to them and to give 
										informed consent to collection. For children under the age of 16, or where 
										capacity to provide consent is at issue, our policy is that a parent or
										guardian will be notified and their consent sought.
									</p>
								</li>
								<li><strong>Collection of unsolicited information</strong>
									<p>
										Sometimes personal information is not sought by us but is delivered or sent
										to us by either the individual or a third party without prior request.
									</p>
									<p>
										Where unsolicited information is received by us, we will, within a
										reasonable period, determine whether that information is directly related 
										to one or more of our functions or activities. If this cannot be determined,
										we will, as soon as practicable, destroy or de-identify the information. If 
										this can be determined we will notify you of the purpose of collection and 
										our intended uses and disclosures according to the requirements of the APPs,
										unless it is impracticable or unreasonable for us to do so.
									</p>
								</li>
								<li><strong>How we collect personal information</strong>
									<p>
										We primarily use forms, online portals and other electronic or paper 
										correspondence to collect your personal information. By signing paper 
										documents or agreeing to the terms and conditions and disclaimers for 
										electronic documents you are consenting to the collection of any personal 
										information you provide to us
									</p>
									<p>
										We may also collect your personal information if you:
									</p>
									<ul>
										<li>communicate with us by telephone, mail, email, fax or SMS;</li>
										<li>attend a face to face meeting or event conducted by us or our contractors;</li>
										<li>use our websites;</li>
										<li>interact with us on our social media platforms.</li>
									</ul>
									<p>For further information on what information we collect online see section 
										2.12 of this privacy policy.
									</p>
									<p>
										As noted at section 2.1, in certain circumstances we may collect and 
										receive personal information about you from third parties including other 
										Employment Agencies and state and territory governments.
									</p>
								</li>
								<li><strong>Remaining anonymous or using a pseudonym</strong>
									<p>
										We understand that anonymity is an important element of privacy and you may 
										wish to remain anonymous, or use a pseudonym when interacting with us.
									</p>
									<p>
										In some cases you will be able to advise us that you wish to remain 
										anonymous or use a pseudonym during your contact with us. However, there 
										will be occasions where it will not be practicable for you to remain 
										anonymous or use a pseudonym and we will notify you accordingly at the time 
										of collection. For example, it may be impracticable to investigate and 
										resolve your particular complaint about how your case was handled or how
										the department behaved unless you provide your name or similar information.
									</p>
								</li>
								<li><strong>Information collected by our contractors</strong>
									<p>
										Under the Privacy Act, we are required to take contractual measures to 
										ensure that contracted service providers (including subcontractors) comply 
										with the same privacy requirements applicable to us.
									</p>
								</li>
								<li><strong>Storage and data security</strong>
									<ol>
										<li><strong>Storage</strong>
											<p>
												We hold personal information in a range of paper-based and 
												electronic records, including cloud computing.
											</p>
											<p>
												Storage of personal information (and the disposal of information 
												when no longer required) is managed in accordance with the Nigerian
												Government's records management regime. This ensures your personal 
												information is held securely.
											</p>
										</li>
										<li><strong>Data security</strong>
											<p>
												We take all reasonable steps to protect the personal information 
												held in our possession against loss, unauthorised access, use, 
												modification, disclosure or misuse.
											</p>
											<p>
												Access to your personal information held by us is restricted to 
												authorised persons who are SolPro's employees or contractors, on a 
												need to know basis.
											</p>
											<p>
												Electronic and paper records containing personal information are 
												protected in accordance with Nigeria's security policies and 
												applicable laws. 
											</p>
											<p>
												We conduct regular audits to ensure we adhere to these policies.
											</p>
										</li>
									</ol>
								</li>
								<li><strong>Data quality</strong>
									<p>
										We take all reasonable steps to ensure that the personal information we 
										collect is accurate, up-to-date, complete, relevant and not misleading.
									</p>
									<p>
										These steps include responding to requests to correct personal information 
										when it is reasonable and appropriate to do so. For further information on 
										correcting personal information see section 3 of this privacy policy.
									</p>
									<p>
										Audits and quality inspections are also conducted from time to time to 
										ensure the accuracy and integrity of information, and any systemic data 
										quality issues are identified and resolved promptly.
									</p>
								</li>
								<li><strong>Purposes for which information is collected, held, used and disclosed</strong>
									<p>
										We collect personal information for a variety of different purposes 
										relating to our functions and activities including:
									</p>
									<ul>
										<li>performing our employment and personnel functions in relation to our 
											staff and contractors;
										</li>
										<li>performing our legislative and administrative functions;</li>
										<li>policy development, research and evaluation;</li>
										<li>complaints handling;</li>
										<li>program management;</li>
										<li>contract management; and</li>
										<li>management of correspondence with the public.</li>
									</ul>
									<p>
										We use and disclose personal information for the primary purpose for which 
										it is collected. You will be given information about the primary purpose of
										collection at the time the information is collected.
									</p>
									<p>
										We will only use your personal information for secondary purposes where 
										we are able to do so in accordance with the Privacy Act. This may include 
										where you have consented to this secondary purpose, or where the secondary
										purpose is related (or if sensitive information, directly related) to the 
										primary purpose and you would reasonably expect us to use or disclose the 
										information for the secondary purpose, where it is required or authorised 
										by law or where a permitted general situation exists such as to prevent a 
										serious threat to safety.
									</p>
									<p>
										Likely secondary purposes for which we many use or disclose your personal 
										information include but are not limited to: quality assurance, auditing, 
										reporting, research, evaluation and analysis, and promotional purposes.
									</p>
								</li>
								<li><strong>Our website</strong>
									<ol>
										<li><strong>Log information (browsing)</strong>
											<p>
												When you use our online services, our servers automatically record 
												information that your browser sends whenever you visit a website.
												These server logs may include information such as your server 
												address, your top level domain name (for example, .com, .gov, .au, 
												.uk, etc.), the date and time of visit to the site, the pages 
												accessed and documents viewed, the previous sites visited, the
												browser type, the browser language, and one or more cookies that
												may uniquely identify your browser.
											</p>
											<p>
												No attempt is made to identify you through your browsing other 
												than in exceptional circumstances, such as an investigation into
												the improper use of the website.
											</p>
										</li>
										<li><strong>Cookies</strong>
											<p>
												A cookie is a small file that uniquely identifies your browser. 
												It contains information that your web browser sends back to our 
												website server whenever you visit it again.
											</p>
											<p>
												We use cookies to 'remember' your browser between page visits. In 
												this situation, the cookie identifies your browser, not you 
												personally. No personal information is stored within our cookies.
											</p>
										</li>
										<li><strong>Google Analytics</strong>
											<p>
												We use Google Analytics to collect information about visitors to 
												our website. Google Analytics uses cookies and JavaScript code to 
												help analyse how users use the site. It anonymously tracks how 
												visitors interact with this website, including how they have 
												accessed the site (for example from a search engine, a link, an 
												advertisement) and what they did on the site. The information 
												generated by the cookie about your use of the website (including
												your IP address) will be transmitted to and stored by Google on 
												servers in the United States. Google will use this information for 
												the purposes of compiling reports on website activity and providing 
												other services relating to website activity and internet usage. You
												may refuse the use of cookies by selecting the appropriate settings
												on your browser.
											</p>
										</li>
										<li><strong>Links to External Websites</strong>
											<p>
												Our website includes links to other websites. We are not responsible 
												for the content and privacy practices of other websites. We 
												recommend that you examine each website's privacy policy separately.
											</p>
										</li>
										<li><strong>Electronic communication</strong>
											<p>
												There are inherent risks associated with the transmission of 
												information over the internet, including via email. You should be 
												aware of this when sending personal information to us via email or 
												via our website or social media platforms. If this is of concern to
												you then you may use other methods of communicating with us, such 
												as post, fax or telephone (although these also have risks associated 
												with them).
											</p>
											<p>
												We only record your email address when you send a message to us or 
												subscribe to one of our mailing lists. Any personal information, 
												including email addresses, will only be used or disclosed for the
												purpose for which it was provided.
											</p>
										</li>
									</ol>
								</li>
								<li><strong>Disclosure of personal information to 3rd parties</strong>
									<p>
										We will, on occasion, disclose personal information to 3rd parties
										recipients. The situations in which we may disclose personal information 
										3rd parties include:
									</p>
									<ul>
										<li>
											the publication on the internet of material which may contain personal
											information, such as SolPro reports and other documents; photographs, 
											video recordings and audio recordings; and posts and comments on our 
											social media platforms;
										</li>
										<li>
											the provision of personal information to overseas researchers or 
											consultants (where consent has been given for this or we are otherwise 
											legally able to provide this information);
										</li>
										<li>
											the provision of personal information to recipients using a web-based
											email account where data is stored on an overseas server; and
										</li>
										<li>
											the provision of personal information to foreign governments and law
											enforcement agencies (in limited circumstances and where authorised by 
											law).
										</li>
									</ul>
									<p>
										We will never sell or use your personal information in any unlawful way.
									</p>
									<p>
										We will not disclose your personal information to an overseas recipient
										unless at least one of the following applies:
									</p>
									<ul>
										<li>the recipient is subject to a law or binding scheme substantially 
											similar to the Nigerian Privacy Principles, including mechanisms for 
											enforcement;
										</li>
										<li>
											you consent to the disclosure after being expressly informed that we 
											will not be taking reasonable steps to ensure that the overseas
											recipient does not breach the Nigerian Privacy Principles;
										</li>
										<li>disclosure is required or authorised by law;</li>
										<li>
											disclosure is reasonably necessary for an enforcement related activity 
											conducted by, or on behalf of, an enforcement body and the recipient 
											performs similar functions.
										</li>
									</ul>
									<p>
										It is not practicable to list every country to which we may provide personal
										information as this will vary depending on the circumstances.
									</p>
								</li>
								<li><strong> Accidental or unauthorised disclosure of personal information</strong>
									<p>
										We will take seriously and deal promptly with any accidental or unauthorised 
										disclosure of personal information. We follow the OAIC's 
										<a href="http://www.oaic.gov.au/privacy/privacy-resources/privacy-guides/
										data-breach-notification-a-guide-to-handling-personal-information-security-
										breaches" target="_blank">Data breach notification � A guide to handling 
										personal information security breaches</a> when handling accidental or 
										unauthorised disclosures of personal 
										information
									</p>
									<p>
										Legislative or administrative sanctions may apply to unauthorised 
										disclosures of personal information.
									</p>
								</li>
							</ol>
						</div>
					</li>
					<li><strong>ACCESSING AND CORRECTING YOUR PERSONAL INFORMATION</strong>
						<div>
							<ol>
								<li><strong> How to seek access to and correction of personal information</strong>
									<p>
										You have a right under the Privacy Act to access personal information we
										hold about you. You also have a right under the Privacy Act to request 
										corrections of any personal information that we hold about you if you think 
										the information is inaccurate, out-of-date, incomplete, irrelevant or 
										misleading.
									</p>
									<p>
										To access or seek correction of personal information we hold about you, 
										please use your login details or contact us using the contact details set
										out at section 5.1 of this privacy policy.
									</p>
								</li>
								<li><strong>Our access and correction process</strong>
									<p>
										If you request access to or correction of your personal information, 
										we will respond to you within 30 calendar days.
									</p>
									<p>
										While the Privacy Act requires that we give you access to your personal 
										information upon request or an opportunity to request the correction of 
										your personal information, it does set out circumstances in which we may
										refuse to give you access or decline to correct your personal information.
									</p>
									<p>
										If we refuse to give you access or make corrections to your personal 
										information, we will provide you with a written notice which, among other 
										things, gives our reasons for refusing your request.
									</p>
								</li>
								<li><strong>If you are unsatisfied with our response</strong>
									<p>
										If you are unsatisfied with our response, you may make a complaint, 
										either directly to us (see section 5.1 below), or you may wish to contact 
										<a href="#">support@bejite.com</a> 
									</p>
								</li>
							</ol>
						</div>
					</li>
					<li><strong>COMPLAINTS</strong>
						<div>
							<ol>
								<li><strong>How to make a complaint</strong>
									<p>
										If you think we may have breached your privacy you may contact us to make a
										complaint using the contact details set out at section 5.1 of this privacy 
										policy. In order to ensure that we fully understand the nature of your
										complaint and the outcome you are seeking, we prefer that you make your 
										complaint in writing.
									</p>
									<p>
										Please be aware that it may be difficult to properly investigate or respond 
										to your complaint if you provide insufficient detail. You may submit an 
										anonymous complaint, however if you do it may not be possible for us to
										provide a response to you.
									</p>
								</li>
								<li><strong>Our complaint handling process</strong>
									<p>
										We are committed to quick and fair resolution of complaints and will ensure 
										your complaint is taken seriously and investigated appropriately. Please be 
										assured that you will not be victimised or suffer negative treatment if you 
										make a complaint.
									</p>
								</li>
								<li><strong>If you are unsatisfied with our response</strong>
									<p>
										If you are not satisfied with the way we have handled your complaint in the
										first instance, you may contact the Office of the Consumer Protection
										Council or any other authorized body to refer your complaint for further 
										investigation. Please note that the Council may not investigate if you have
										not first brought your complaint to our attention.
									</p>
								</li>
							</ol>
						</div>
					</li>
					<li><strong>CONTACT US</strong>
						<div>
							<ol>
								<li><strong>General enquiries, complaints, requests for access or correction</strong>
									<p>If you wish to:</p>
									<ul>
										<li>query how your personal information is collected, held, used or 
											disclosed by us;
										</li>
										<li>ask us questions about this privacy policy;</li>
										<li>
											request access to or seek correction of your personal
											information; or
										</li>
										<li>make a privacy complaint;</li>
									</ul>
									<p>
										please contact us:
									</p>
									<p>
										By post:	22 Okomoko Street, D/Line<br>
										Port Harcourt, Rivers State<br>
										Nigeria
									</p>
									<p>By email: complaint@bejite.com </p>
									<p>By telephone: +2348068735953</p>
								</li>
							</ol>
						</div>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>

@endsection