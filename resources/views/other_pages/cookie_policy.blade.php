@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<div class=" other-pages-content">
			<h3><strong>BEJITE COOKIE POLICY</strong></h3><hr>
			<div class="about-bejite-content">
				<p>
					Our Privacy Policy explains how we collect and use information from and about you when you use 
					Bejite and other SolPro. services. We have provided this cookie policy to tell you more about why 
					we use cookies and other similar identifying technologies, the types of cookies and similar 
					technologies we use, and your choices in relation to these technologies.
				</p>
				<p><h3>What Cookie Means</h3></p>
				<p>
					A cookie is a small piece of data that is sent to your browser or device by websites, mobile 
					apps, and advertisements that you access or use. This data is stored on your browser or device
					and helps websites and mobile apps remember things about you. For example, cookies may help us 
					remember certain preferences you have selected, such as your language preference or employers
					you have visited their pages.
				</p>
				<p><h3>Our Use of Cookies</h3></p>
				<p>
					Like most online services, SolPro. uses cookies for a number of reasons, like protecting your 
					Bejite data and account, helping us see which features are most popular, counting visitors to a 
					page, improving our users' experience, keeping our services secure, and just generally providing 
					you with a better, more intuitive, and satisfying experience. The cookies we use generally fall 
					into one of the following categories.
				</p>
				<p>
					<table>
						<tr>
							<th>Category of Cookies</th>
							<th>Why we use these cookies</th>
						</tr>
						<tr>
							<td>Preferences</td>
							<td>We use these cookies to remember your settings and preferences.<br>
								For example, we may use these cookies to remember your employers preferences, 
								jobs favourites, etc
							</td>
						</tr>
						<tr>
							<td>Security</td>
							<td>
								We use these cookies to help identify and prevent security risks.<br>
								For example, we may use these cookies to store your web-session information to 
								prevent others from changing your password without your username and password.
							</td>
						</tr>
						<tr>
							<td>Performance</td>
							<td>
								We use these cookies to collect information about how you interact with our services
								and to help us improve them.<br>
								For example, we may use these cookies to determine if you have interactedwith a certain page. 
							</td>
						</tr>
						<tr>
							<td>Analytics</td>
							<td>
								We use these cookies to help us improve our services.<br>
								For example, we can use these cookies to learn more about which features are the 
								most popular with our users and which ones might need some tweaks.
							</td>
						</tr>
						<tr>
							<td>Advertising</td>
							<td>
								We use these cookies to deliver advertisements, to make them more relevant and
								meaningful to consumers, and to track the efficiency of our advertising campaigns,
								both on our services and on other websites or mobile apps.
							</td>
						</tr>
					</table>
				</p>
				<p><h3>Pixels, Local Storage, and Other Similar Technologies</h3></p>
				<p>
					We may also use other similar technologies on our services, such as pixel tags and local 
					storage. We use these technologies to do things like help us see what features are most popular,
					create a more personalized experience, and deliver relevant ads. Pixel tags (also called clear 
					GIFs, web beacons, or pixels) are small blocks of code installed in or on a webpage, mobile app,
					or advertisement. These tags can retrieve certain information about your browser and device 
					such as: operating system, browser type, device type and version, referring website, website 
					visited, IP address, and other similar information. Local storage is an industry-standard 
					technology that allows a website or mobile app to store and retrieve data on an individual's 
					computer, mobile phone, or other device.
				</p>
				<p><h3>Your Choices</h3></p>
				<p>
					Your browser probably lets you choose whether to accept browser cookies. And you may even be 
					able to choose or limit the use of cookies and other similar technologies in web apps and 
					mobile apps. But these technologies are an important part of how our services work, so removing 
					or rejecting cookies, or limiting the use of other similar technologies could affect the 
					availability and functionality of our services.
				</p>
				<p><strong>Web browser opt-out</strong></p>
				<p>
					Most web browsers are set to accept cookies by default. If you don't want to allow cookies you 
					may have some options. Your browser may provide you with a set of tools to manage cookies. 
					You can usually set your browser to refuse some cookies or all cookies. For example, some 
					browsers give you the option to allow first-party cookies but block third-party cookies. 
					So what is the difference between first-party and third-party cookies?
				</p>
				<p>
					<ul>
						<li>
							A "first-party" cookie is served by the page or domain that you are visiting. So for
							example, when you visit Bejite.com and we serve a cookie for purposes of remembering 
							your settings that is a first-party cookie.
						</li>
						<li>
							A "third-party" cookie is served by a company that doesn't operate the page or domain 
							you are visiting. So for example, when you visit Bejite.com and Google serves a cookie
							on Bejite.com for Bejite's analytics that is a third-party cookie.
						</li>
					</ul>
				</p>
				<p>
					You may also be able to remove cookies from your browser. Your ability to manage cookies 
					through a mobile browser, however, may be limited. For more information about how to manage 
					your cookie settings, please follow the instructions provided by your browser which are usually 
					located within the "Help," "Tools," or "Edit" settings.
				</p>
				<p><strong>Mobile device opt-outs</strong></p>
				<p>
					While we are still developing our mobile version of Bejite, it is important to note that it 
					will automatically adopt this policy when it is launched.
				</p>
				<p>
					Your mobile operating system may let you opt-out from having your information collected or used
					for interest-based advertising on mobile devices. You should refer to the instructions provided 
					by your mobile device's manufacturer; this information is typically available under the 
					"settings" function of your mobile device.
				</p>
				<p>
					And of course, if your mobile device offers an uninstall process, you can always stop us from 
					collecting information through the app by uninstalling the Bejite app when we finally launch 
					such.
				</p>
				<p>Thank you.</p>
			</div>
		</div>
	</div>
</div>

@endsection