@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-offset-1 col-md-10 col-md-offset-1">
		<div class=" other-pages-content">
			<h3><strong>BEJITE SECURITY ADVICE</strong></h3><hr>
				<div>
					<p>
						Bejite.com is a platform that brings together Jobseekers and Employers. We are neither a 
					recruitment firm nor do we act as labor consultants to, or employment partners of, any employer. 
					The portal is used by numerous corporate, placement companies, and recruitment firms. The usage 
					of our site is bound by a set of <a href="terms-and-conditions" target="_blank">terms and 
					conditions</a> that the employers must agree and adhere to.
					One of the primary objectives of these Terms and Conditions is to discourage and prevent misuse 
					and fraud. 
					</p>
					<p>
						Regrettably, sometimes, false job interviews are sent out, and non-existing job offers are 
						sent via messages or chats to illegally collect personal information and/or money from 
						unsuspecting job seekers. To tackle such cases, we need your help to remain vigilant every 
						day - when we receive complaints/reports from jobseekers about fraudulent or suspicious 
						emails or any employer requesting for money in any guise, we promptly notify the concerned 
						employer and, if necessary, block them from using our services.
					</p>
					<p>
						<strong>We encourage you to be wary of employers that</strong>
					</p>
					<p>
						<ul>
							<li>
								ask your personal, non-work related information such as Credit Card numbers or 
								Bank information over phone or email
							</li>
							<li>do not provide valid contact information</li>
							<li>
								ask for monetary transactions, money-transfers, or payment for any employment/recruitment related purpose 
							</li>
							<li>promise emigration and ask for money to process visa, etc</li> 
						</ul>
					</p>
					<p>
						Before you respond to such messages, <strong>we suggest you to do a discreet enquiry and be sure to
						verify the legitimacy of the employer</strong> with whom you are interacting. If possible, go to 
						their physical address with a companion. Don't just rely on the information provided online 
						including the ones on www.bejite.com . Please note that Bejite.com does not approve of, or 
						represent any employer or recruiter sending such fraudulent communication, which in fact 
						are a violation of the Terms and Conditions. We value the trust you place on Bejite.com
						and are committed to making your job search a safe and fraud-free experience on our site.<br>
						If you have received (or receive) any such suspicious email communication from a possible 
						Bejite client, do report it to us by using the "report" button. 
					</p>
					<p>
						<strong>Educate Yourself Against Fraud/Scams</strong>
					</p>
					<p>
						We encourage you to read the following and make yourself aware of the warning signs of 
						the most common kinds of Internet and Email frauds. 
					</p>
					<p>
						There are two types of email scams - 'phishing' and 'spoofing'. In both the cases, the 
						'from address' is forged to make it appear as if it came from a source that it actually did
						not come from. 
					</p>
					<p>
						<strong>What is Phishing?</strong>
					</p>
					<p>
						Phishing is an attempt by fraudsters to 'fish' for your personal details. A phishing 
						attempt is usually in the form of an e-mail, which encourages you to click a link that 
						takes you to a fraudulent log-on page designed to capture your account/password/personal
						details. These emails can also be used to lure the recipient into downloading harmful 
						software. Please note that Bejite.com will never ask you to download software in order to
						access your account. 
					</p>
					<p>
						More information about Phishing scams and how you can protect yourself against them is 
						available here <a href="www.antiphishing.org/resources/overview/" target="_blank">
						www.antiphishing.org/resources/overview/</a>
					</p>
					<p>
						<strong>What is Spoofing? </strong>
					</p>
					<p>
						Spoof emails usually include a fraudulent offer of employment and/or an invitation to 
						perform a monetary transaction. Such email scams are, unfortunately, common across the 
						world and could target anyone - including unsuspecting jobseekers who have registered 
						with Bejite.com. The sender's address is often disguised and/or the sender may not have
						provided the entire contact information, such as, the correct physical address, phone 
						numbers and email ID. 
					</p>
					<p>
						The precautionary measures jobseekers could take to protect themselves against suspected
						spoof emails have been mentioned above. 
					</p>
					<p>
						<strong>How can you check if an employer or a recruiter is a fraud or genuine?</strong>
					</p>
					<p>
						Below are a few things we can suggest you could try (you are encouraged to exhaust all 
						possible ways, not just be limited to the following) to ascertain the legitimacy of 
						the employers who contact you: 
					</p>
					<div>
						<ol>
							<li>Have you heard the name of the company before?</li>
							<li>Does the company or individual recruiter have verifiable/traceable physical address
							(it does not matter the state or country)? 
							</li>
							<li>
								If the recruiter is an individual and wants you to visit his residential premises
								(in terms of domestic jobs), ensure you go with companions if you are sceptical of 
								the recruiter.
							</li>
							<li>
								Does the domain name in their email or on Bejite.com address match the name of the 
								company? For example if you get a job offer from XYZ Petroleum Company, and their 
								email ID is xyzpetroleum@gmail.com or xyzpetroleum@yahoo.com then it's a fake 
								company. Real companies use their own domain name on their email, and do not use
								free email providers. 
							</li>
							<li>
								Search on Yahoo, Google or MSN to see if the company has a genuine website. 
								Also check if other people have reported that the employer has been involved in 
								job scams previously. 
							</li>
							<li>
								Note the country that company claims to be working in, and offering you a job in.
								Most fake companies offer scam jobs in Africa, Middle East, Asia, South America, 
								and to a much smaller extent, US or Western Europe. 
							</li>
							<li>
								Find out if you need to pay the company for anything. If they ask you to pay - at 
								first or even after some time - for visa, work permit or for travel, chances are 
								it's a fake company. 
							</li>
						</ol>
					</div>
					<p>
						Remember, If you have received (or receive) any such suspicious email communication from a
						possible Bejite client, do not hesitate to report it to us by using the report button. You 
						can also block the employer from contacting you in the future by using the "Block" button. 
					</p>
				</div>
			</div>
		</div>
	</div>

@endsection