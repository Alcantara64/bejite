<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body id="">
<div class="container-fluid" >
@include('partials.navbar')
@yield('content')
@if(Auth::check() && Auth::user()->reg_status == 'completed')


@endif

    <div id="search-result" style="display: none;">
        <div class="inner">
            <ul id="search-loader">
                <li class="first">
                    <a href="#" onclick="javascript:void(0)"><span class="fa fa-users"></span> Find People By name,city,state,country and more... </a>
                </li>
                <li class="first">
                    <a href="#" onclick="javascript:void(0)"><span class="fa fa-home"></span> Find People By Places</a>
                </li>
            </ul>
            <ul id="search-ul">
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Scripts -->
    </div>
    @if(Auth::check())
        <input type="hidden" id="current_user" value="{{ Auth::user()->id }}"/>
        <input type="hidden" id="current_user_photo" value="{{ Auth::user()->photo }}"/>
    @else
        <input type="hidden" id="current_user" value="0"/>
        <input type="hidden" id="current_user_photo" value="avatar.jpg"/>
    @endif
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.validationEngine-en.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.validationEngine.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/lists.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/countries.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.form.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/autogrow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/chat.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/search.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/postbox.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/advsearch.js') }}" type="text/javascript"></script>
    <!--<script type="text/javascript" src="http://sandbox.app:8000/php/app.php?widget-init.js"></script>-->
    <script language="javascript">print_country("country");</script>
    <script language="javascript">print_country("countries");</script>
    <script language="javascript">print_country("countries2");</script>
    <script type="text/javascript">

    $('document').ready(function(){
        $('#university').keyup(function(){
var query =$(this).val();
var _token =$('input[name="_token"]').val();
if(query !== ""){
  $.ajax({
    url:'{{route("university.fetch")}}',
    method:"POST",
    data:{query:query},
    success:function(data){
      if(data){
      $('#university_list').fadeIn();
      $('#university_list').html(data);
    }else{
       $('#university').val($(this).text());
    }
    }
  });
}
        });
        $(document).on('click','li.uni',function(){
         $('#university').val($(this).text());
         $('#university_list').fadeOut();
        });

//skills starts here
        $('#skill').keyup(function(){
   var query =$(this).val();
   var _token =$('input[name="_token"]').val();
   if(query !== ""){
   $.ajax({
    url:'{{route("skill.fetch")}}',
    method:"POST",
    data:{query:query},
    success:function(data){
      if(data){
      $('#skill_list').fadeIn();
      $('#skill_list').html(data);
    }else{
       $('#skill').val($(this).text());
    }
    }
   });
   }
        });

        $(document).on('click','li.skill',function(){
         $('#skill').val($(this).text());
         $('#skill_list').fadeOut();
        });
      });
    </script>
</body>
</html>
