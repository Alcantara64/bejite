<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token"content="{{csrf_token()}}">
    <title>Laravel</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/validationEngine.jquery.css') }}" rel="stylesheet">
    <link href="{{ asset('css/utils.css') }}" rel="stylesheet">
    <link href="{{ asset('editor/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body id="app-layout">
<div class="admin-page-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">Admin panel</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="ajax-loader"><span class="fa fa-spin fa-spinner fa-2x"></span> <span class="ajax-status"> Please wait...</span></a></li>
                </ul>
                @if(Auth::check())
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('/users/'.Auth::user()->photo) }}" class="avatar-xs img-circle"> &nbsp; <span class="fullnames">{{ ucwords(Auth::user()->fullnames) }}</span>  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ action('AdminController@showProfileSettings') }}">My Settings</a></li>
                                <li><a href="{{ action('AdminController@showChangePassword') }}">Change Password</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ action('AdminController@logout') }}">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </nav>

    <div class="container-fluid admin-content">
        <div class="row">

            <div class="col-sm-2 col-md-1 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="{{ action('AdminController@dashboard') }}"> Dashboard </a></li>
                    <li><a href="/admin/users">Users</a></li>
                    <!--<li><a href="{{ action('AdminController@showSliders') }}">Sliders</a></li>-->
                    <li><a href="{{ action('AdminController@showPages') }}">Pages</a></li>
                    <li><a href="{{ action('AdminController@showSiteSettings') }}"> Settings</a></li>
                    <li><a href="{{ action('BlogController@showAddNew') }}"> Blog</a></li>
                </ul>
                <div class="triangle"></div>
            </div>
            <div class="col-sm-10 col-sm-offset-2 col-md-11 col-md-offset-1 main">
                <div class="row">

                    @yield('content')

                            <!--
        Notification Modal
        <!-- Modal -->
                    <div class="modal fade" id="notify" tabindex="-1" role="dialog" aria-labelledby="notifyLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="notifyLabel">Notification</h4>
                                </div>
                                <div class="modal-body">
                                    <span class="fa fa-spin fa-spinner"></span> Loading content, please wait...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.form.js') }}"></script>
<script src="{{ asset('js/jquery.validationEngine.js') }}"></script>
<script src="{{ asset('js/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('js/countries.js') }}"></script>
<script src="{{ asset('js/utils.js') }}"></script>
<script src="{{ asset('editor/trumbowyg.min.js') }}"></script>
<script src="{{ asset('editor/plugins/upload/trumbowyg.upload.js') }}"></script>
<script src="{{ asset('editor/plugins/base64/trumbowyg.base64.js') }}"></script>
<script src="{{ asset('editor/plugins/noembed/trumbowyg.noembed.js') }}"></script>
<script language="javascript">print_country("country");</script>
<script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
