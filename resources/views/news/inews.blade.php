@foreach($posts as $post)
        <!DOCTYPE html>
        <html>
        <head>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/bootstrap-social.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
        </head>
        <body style="margin:0;padding:0;">

        <div class="box panel panel-default noheading">
          <div class="panel-body">
            <div class="row">

                    <div class="col-md-1">
                        {!! Auth::user()->getUserPhoto($post->user_id,'avatar-xs-circle change-avatar') !!}
                    </div>
                    <div class="col-md-11">
                        {!! $post->content !!}
                        <div class="comment-container">
                            <div class="col-md-1">
                                {!! Auth::user()->getUserPhoto(Auth::user()->id,'avatar-xxs-circle change-avatar') !!}
                            </div>
                            <div class="col-md-10">
                                <textarea id="comment-box-{{ $post->id }}" class="form-control comment-box" placeholder="Add comment"  onkeypress="return addComment({{ $post->id }},event);" maxlength="255"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            @foreach($comments->getComments($post->id) as $com)
                            <div class="col-md-1">
                                {!! Auth::user()->getUserPhoto($com->user_id,'avatar-xxs-circle change-avatar') !!}
                            </div>
                            <div class="col-md-10">
                                <div class="comment"> {{ $com->content }} </div>
                            </div>
                            <div class="clearfix"></div>
                            @endforeach
                        </div>
                    </div>

            </div>
            </div>
        </div>
        <script src="{{ asset('js/newsfeed.js') }}" type="text/javascript"></script>
        </body>
        </html>
    @endforeach