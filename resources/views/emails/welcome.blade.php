@extends('emails.email')

@section('content')
    <div class="header">
        <div class="user-photo pull-left">
            <a href="{{ url('/') }}">
                <img class="site-logo" src="{{ asset('logo/bejite_logo_red.png') }}" alt="Bejite - One stop Jobsite">
            </a>
        </div>
        <div class="heading pull-left">
            <h3>Welcome to Bejite</h3>
        </div>
        <div class="clearfix"></div>
        <div class="mail-content">
            Hi! {{ explode(' ',$user->fullnames)[0] }},
            Welcome to bejite.com. We are glad to have you on board.<br>
			To have the best experience on Bejite:
			<p><strong>As a jobseeker:</strong></p>
				-  Connect to more employers and jobseekers to grow your network<br>
				-  Update you CV <a href="www.bejite.com/jobseeker/update-data" target="_blank">here</a> with all
				the relevant details relating to your field to enable employers search you out easily<br>
				-  Be active on Bejite with attractive posts and comments.
			<p><strong>As an employer/Recruiter:</strong></p>
				-  Connect to more employers(or Jobseekers) to grow your network<br>
				-  Update your profile summary <a href="www.bejite.com/profile/settings" target="_blank">here</a>
					so that peolple will know what you or your company does<br>
				-  Be active on Bejite. Respond to jobseekers' questions and enquiries, make attractive posts, comments and 
					responses. This will grow your network and market reach for your company's product/services.
			
        </div>
    </div>
@endsection