<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<style>
    .email-container {
        max-width:600px;
        min-height:100px;
        margin : 0 auto;
        background-color: #ffffff;
        border:1px solid #e7e7e7;
        border-radius: 10px;
    }
    .avatar-m {
        width:35px;
        height:35px;
    }
    .site-logo {
        height: 35px;
    }
    .header {
        padding:15px;
        margin-bottom:10px;
        color: #ffffff;
    }
</style>

<div class="email-container">
    @yield('content')
</div>