@extends('layouts.app')

@section('content')
    <div class="container white">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                <div class="col-md-3">
                    <img class="employer-logo-big" src="/images/logo-image/{{ $employer->logo }}">
                </div>
                <div class="col-md-7">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-3">
                                <col class="col-xs-7">
                            </colgroup>
                            <thead>
                            <tr>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <code> Employer </code>
                                </th>
                                <td> {{ $employer->company }}</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Name of Rep. </code>
                                </th>
                                <td> {{ ucfirst(Auth::user()->fullnames) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Position </code>
                                </th>
                                <td> {{ $employer->position }}</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Type </code>
                                </th>
                                @if($employer->type == 'company')
                                    <td> {{ $employer->type }}</td>
                                @else
                                    <td> Sole Proprietor</td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Support Email </code>
                                </th>
                                <td> {{ $employer->support_email }}</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Website </code>
                                </th>
                                <td> {{ $employer->website }}</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <code> Address </code>
                                </th>
                                <td> {{ $employer->address1 }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" class="ibutton button2" onclick="window.location='/employer/update-data'"> <span class="glyphicon glyphicon-pencil"></span> Edit Profile </button>
                </div>
            </div>
        </div>
    </div>
@endsection
