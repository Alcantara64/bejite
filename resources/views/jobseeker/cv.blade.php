@extends('layouts.app')

@section('content')

		@include('layouts.top')
    <h4 class="text-center">Cv</h4>
    <hr/>
    <div class="container">
      <div class="panel-group" id="accordion">
       <div class="panel panel-default">
         <div class="panel-heading">
          <a data-toggle="collapse" class="" data-parent="#accordion" href="#collapse1">
<br>
						<h3 class="panel-title text-center" style="height:40px;color:white">

             Biodata
           </h3></a>
         </div>
         <div id="collapse1" class="panel-collapse collapse in">
           <div class="panel-body">
<form class="" action="" method="post">
	<div class="row">

	<div class="col-sm-2">


      <div class="form-group">
   <select  style="height:50px" class="form-control">
   <option></option>
@foreach(universities as university)
echo '<option>'.university[].'</option>';
@endforeach
   </select>
  </div>
</div>
<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
</div>
<div class="row">

<div class="col-sm-2">


		<div class="form-group">
	<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


	<div class="form-group">
<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


	<div class="form-group">
<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


	<div class="form-group">
<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


	<div class="form-group">
<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>
<div class="col-sm-2">


	<div class="form-group">
<input type="text" style="height:50px" placeholder="title" class="form-control"  name="" value="">
</div>
</div>

</div>
<div class="row">
<div class="col-sm-12">

	<button type="button" class="btn btn-primary" name="button">Add</button>
	<button type="button" class="btn btn-primary" name="button">Submit</button>
</div>

</div>

	</div>
</form>
           </div>
         </div>
       </div>
       <div class="panel panel-default">
         <div class="panel-heading">
					 <a data-toggle="collapse" data-parent="#accordion"  href="#collapse2">
						 <br>
           <h4 class="panel-title text-center"  style="height:40px;color:white">
             Educational Qualification
           </h4></a>
         </div>
         <div id="collapse2" class="panel-collapse collapse">
           <div class="panel-body">
<form class="" action="#" method="post">
	<div class="row">

	<div class="col-sm-3">


			<div class="form-group">
		<select class="form-control" style="height:50px" name="">
		<option value="">Tertiary Institution</option>
		</select>
	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<select class="form-control" style="height:50px" name="">
			<option value="">Degree obtained</option>
			</select>
	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<select class="form-control" style="height:50px" name="">
			<option value="">Course/Program</option>
			<option value="">Computer Science </option>
			<option value="">Economics</option>

		</select>
	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<select class="form-control" style="height:50px" name="">
			<option value="">year of Graduation</option>
			<option value="">2017</option>
			<option value="">2016</option>
			<option value="">2015</option>
			<option value="">2014</option>

		</select>
	</div>
	</div>
</div>

	<div class="row">
<div class="col-sm-12">

		<button type="button" class="btn btn-primary" name="button">Add</button>
		<button type="button" class="btn btn-primary" name="button">Submit</button>
	</div>

	</div>
				 </form>
         </div>
       </div>

     </div>
</div>
@endsection
