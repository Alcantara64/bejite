@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px;">

            <div class="span left-column pull-left">
                <!-- IMPROVE PROFILE -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Fix the following errors and continue.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-group" id="accordion">
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    Bio Data
                                </a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@saveBioData') }}" id="save-bio-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="country" class="control-label col-sm-2">Country of origin:</label>
                                            <div class="col-md-4">
                                                <select name="country" id="country" class="input form-control  validate[required]" onchange="print_state('states', this.selectedIndex);">

                                                </select>
                                            </div>
                                            <label for="stateoforigin" class="control-label col-sm-2">State of origin:</label>
                                            <div class="col-md-4">
                                                <select name="state" class="input form-control  validate[required]" id="states">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="LGA" class="control-label col-sm-2">LGA of Origin:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control"  name="lga_of_origin" value="{{ $biodata->lga }}" placeholder="Local Government of Origin" value="{{ $biodata->lga }}">
                                            </div>
                                            <label for="stateoforigin" class="control-label col-sm-2">Marital Status:</label>
                                            <div class="col-md-4">
                                                <select name="marital_status" class="input form-control  validate[required]">
                                                    @if($biodata->marital_status == 'Single')
                                                        <option value="Single"> Single </option>
                                                        <option value="Married"> Married </option>
                                                    @elseif($biodata->marital_status == 'Married')
                                                        <option value="Single"> Single </option>
                                                        <option value="Married"> Married </option>
                                                    @else
                                                        <option value=""> Choose </option>
                                                        <option value="Single"> Single </option>
                                                        <option value="Married"> Married </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="LGA" class="control-label col-sm-2">Date of Birth:</label>
                                            <div class="col-md-4">
                                                <input type="date" class="input validate[required] form-control" name="date_of_birth" value="{{ $biodata->dob }}" placeholder="MM/DD/YYYY">
                                            </div>
                                            <label for="languages_spoken" class="control-label col-sm-2">Languages spoken:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="language_spoken" value="{{ $biodata->language_spoken }}">
                                               <option value="{{ $biodata->language_spoken }}">{{ $biodata->language_spoken }}</option>
                                               <option value="english">English</option>
                                               <option value="spanish">Spanish</option>
                                               <option value="french">French</option>
                                               <option value="portugise">portugise</option>
                                              </select>
                                                <!--<input type="text" class="input validate[required] form-control" name="language_spoken" value="{{ $biodata->language_spoken }}" placeholder="List languages eg. English,Yoruba">-->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="LGA" class="control-label col-sm-2">Level of proficiency:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="language_proficiency" value="{{ $biodata->language_proficiency }}">
                                               <option value="{{ $biodata->language_spoken }}">{{ $biodata->language_proficiency }}</option>
                                               <option value="advance">Advance</option>
                                               <option value="intermediate">Intermediate</option>
                                               <option value="beginner">Beginner</option>
                                              </select>
                                            <//input type="text" class="input validate[required] form-control" name="language_proficiency" value="{{ $biodata->language_proficiency }}" placeholder="Level of language proficiency">
                                            </div>
                                            <label for="stateoforigin" class="control-label col-sm-2">Complexion:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="complexion" value="{{ $biodata->complexion }}" placeholder="Skin Complexion">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="height" class="control-label col-sm-2">Height:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="height" value="{{ $biodata->height }}" placeholder="Height eg. 5.7 ft">
                                            </div>
                                            <label for="physicalfeatures" class="control-label col-sm-2">Physical features:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="physical_features" value="{{ $biodata->physical_features }}" placeholder="Physical Features">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">Religion:</label>
                                            <div class="col-md-4">
                                                 <select class="validate[required] form-control" name="religion">
                                                   <option  value="{{ $biodata->religion }}">Choose</option>
                                                   <option  value="christianity">Christian</option>
                                                   <option  value="islam">Islam</option>
                                                   <option  value="hindu">Hindu</option>
                                                 </select>
                                              <!--  <input type="text" class="input validate[required] form-control" name="religion" value="{{ $biodata->religion }}" placeholder="Religion">-->
                                            </div>
                                            <label for="tribe" class="control-label col-sm-2">Tribe:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="tribe" value="{{ $biodata->tribe }}" placeholder="Tribe">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="race" class="control-label col-sm-2">Race:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="race" value="{{ $biodata->race }}" placeholder="Race">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="race" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    Education Qualifications
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addEductionalData') }}" id="save-education-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">University Attended:</label>
                                            <div class="col-md-4">

                                            <input type="text" id="university" class="input validate[required] form-control" name="university_attended" value="" placeholder="University Attended">
                                            <div class="" id="university_list"> </div>
                                            </div>

                                            <label for="tribe" class="control-label col-sm-2">Degree Obtained:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="degree_obtained">
                                                <option value="">Choose option</option>
                                                <option value="ssce/waec">SSCE/WAEC</option>
                                                <option value="ond">OND</option>
                                                <option value="hnd">HND</option>
                                                <option value="bsc">BSC</option>
                                                <option value="msc">MSC</option>
                                                <option value="phd">PHD</option>
                                              </select>
                                              <!--  <input type="text" class="input validate[required] form-control" name="degree_obtained" value="" placeholder="Degree Obtained">-->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">Course:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="course" value="" placeholder="Course studied">
                                            </div>
                                            <label for="tribe" class="control-label col-sm-2">Year Graduated:</label>
                                            <div class="col-md-4">
                                            <select class="input validate[required] form-control" name="year_graduated">

                                              <option value="">Select year</option>
                                              <script >
                                                let date = new Date();
                                                year = date.getFullYear();
                                               for(var i=1900;i<=year;i++){
                                                 document.write('<option value="'+i+'">'+i +'</option>');
                                               }
                                                 </script>
                                                 </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="race" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-plus"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>University</th>
                                        <th>Degree</th>
                                        <th>Course</th>
                                        <th>Year</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($educational as $edu)
                                        <tr>
                                            <td>{{ $edui++ }}</td>
                                            <td>{{ $edu->university_attended }}</td>
                                            <td>{{ $edu->degree_obtained }}</td>
                                            <td>{{ $edu->course }}</td>
                                            <td>{{ $edu->year_graduated }}</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $edu->id }}/delete-educational-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSecondry">
                                    Secondary Education
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseSecondry" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addSecondaySchoolData') }}" id="save-education-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="school" class="control-label col-sm-2">Secondary School:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="secondary_school" value="" placeholder="Secondary School">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tribe" class="control-label col-sm-2">Qualification:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="qualification" value="" placeholder="Qualification">
                                            </div>
                                            <label for="religion" class="control-label col-sm-2">Year:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="secondary_school_year">


                                              <option value="">Select year</option>
                                              <script >

                                               for(var i=1900;i<=year;i++){
                                                 document.write('<option value="'+i+'">'+i +'</option>');
                                               }
                                                 </script>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="race" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-plus"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>School</th>
                                        <th>Qualification</th>
                                        <th>Year</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($secondry as $sec)
                                        <tr>
                                            <td>{{ $seci++ }}</td>
                                            <td>{{ $sec->secondary_school }}</td>
                                            <td>{{ $sec->secondary_school_qualification }}</td>
                                            <td>{{ $sec->secondary_school_year }}</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $sec->id }}/delete-secondry-school'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                    Vocational/Technical Schools Attended
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@saveVacationalData') }}" id="save-vacation-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="schoolname" class="control-label col-sm-2">School Name:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="vacation_school_name" value="{{ $vacation->school }}" placeholder="School Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="schoolqualification" class="control-label col-sm-2">Qualification:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="vacation_qualification" value="{{ $vacation->qualification }}" placeholder="Qualification">
                                            </div>
                                            <label for="religion" class="control-label col-sm-2">Year:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="vacation_school_year">
                                                <option value="">select the year</option>
                                                <script >

                                                 for(var i=1900;i<=year;i++){
                                                   document.write('<option value="'+i+'">'+i +'</option>');
                                                 }
                                                   </script>
                                              </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit">
                                                    <span class="glyphicon glyphicon-pencil"></span> Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                    Professional Qualifications Obtained
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addProfessionalData') }}" id="save-professional-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="schoolname" class="control-label col-sm-2">Profession Name:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="profession_name" value="" placeholder="Profession Name">
                                            </div>
                                            <label for="schoolqualification" class="control-label col-sm-2">Qualification:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="profession_year_obtained" value="" placeholder="Year Obtained">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">Specialization:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="profession_specialization" value="" placeholder="Area of Specialization">
                                            </div>
                                            <label for="religion" class="control-label col-sm-2">Years of Experience:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="profession_experience">
                                                <option value="">select years</option>
                                                <script>
                                                let eyears = 5;
                                                for(z=1;z<=eyears;z++){
                                                document.write('<option value="'+z+'">'+z+'</option>');
                                              }

                                                </script>
                                                  <option value="6+">6+</option>
                                              </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-plus"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Profession</th>
                                        <th>Qualification</th>
                                        <th>Specialization</th>
                                        <th>Experience</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($profession as $prof)
                                        <tr>
                                            <td>{{ $profi++ }}</td>
                                            <td>{{ $prof->profession_name }}</td>
                                            <td>{{ $prof->profession_year_obtained }}</td>
                                            <td>{{ $prof->profession_specialization }}</td>
                                            <td>{{ $prof->profession_experience }} yrs</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $prof->id }}/delete-professional-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                    Work Experiences
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addWorkData') }}" id="save-work-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="schoolname" class="control-label col-sm-2">Place of work:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="work_place" value="" placeholder="Place of work">
                                            </div>
                                            <label for="schoolqualification" class="control-label col-sm-2">Position:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="work_position" value="" placeholder="Position">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">Sector:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="work_sector" value="" placeholder="Sector">
                                            </div>
                                            <label for="religion" class="control-label col-sm-2">Years of Experience:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="work_experience">

                                              <
                                              <option value="">select years</option>
                                              <script>

                                              for(z=1;z<=eyears;z++){
                                              document.write('<option value="'+z+'">'+z+'</option>');
                                            }
                                            //<input type="text" class="input validate[required] form-control" name="work_experience" value="" placeholder="Years of experience">
                                              </script>
                                                <option value="6+">6+</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2">Reasons for leaving:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="work_reasons" value="" placeholder="Reasons for leaving">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-plus"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Place of work</th>
                                        <th>Position</th>
                                        <th>Sector</th>
                                        <th>Experience</th>
                                        <th>Reasons</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($work as $wk)
                                        <tr>
                                            <td>{{ $wki++ }}</td>
                                            <td>{{ $wk->work_place }}</td>
                                            <td>{{ $wk->work_position }}</td>
                                            <td>{{ $wk->work_sector }}</td>
                                            <td>{{ $wk->work_experience }} yrs</td>
                                            <td>{{ $wk->work_reasons }}</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $wk->id }}/delete-work-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                    Skills
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addSkillData') }}" id="save-computer-data" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="skilltype" class="control-label col-sm-2">Skill type:</label>
                                            <div class="col-md-10">
                                                <select name="skill_type" class="input form-control validate[required]" id="skill_type">
                                                    <option value="">Choose skill type</option>
                                                    <option value="computer">Computer Skill</option>
                                                    <option value="managerial">Managerial Skill</option>
                                                    <option value="technical">Technical Skill</option>
                                                    <option value="sport">Sport Skill</option>
                                                    <option value="marketting">Marketting Skill</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="skillname"  class="control-label col-sm-2">Name:</label>
                                            <div class="col-md-4">
                                                <input type="text" id="skill" class="input validate[required] form-control" name="skill_name" value="" placeholder="skill name">
                                                 <div class="" id="skill_list">

                                                 </div>
                                            </div>
                                            <label for="schoolqualification" class="control-label col-sm-2">Year:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="skill_year" >

                                                <option value="">Select year</option>
                                                <script >

                                                 for(var i=1900;i<=year;i++){
                                                   document.write('<option value="'+i+'">'+i +'</option>');
                                                 }
                                                   </script>
                                                   </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button class="ibutton button2" type="submit"><span class="glyphicon glyphicon-plus"></span> Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Name</th>
                                        <th>Year</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($skills as $skill)
                                        <tr>
                                            <td>{{ $skilli++ }}</td>
                                            <td>{{ $skill->skill_type }}</td>
                                            <td>{{ $skill->skill_name }}</td>
                                            <td>{{ $skill->skill_year }}</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $skill->id }}/delete-skill-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                                    Jobs Applied for/Sectors/Country you seek the job in:
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@saveJobAppliedForData') }}" id="save-job_applied_for" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Country:</label>
                                            <div class="col-md-10">
                                                <select name="job_country" id="countries" class="input form-control  validate[required]" onchange="print_state('state', this.selectedIndex);">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Name of Job:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="job_name" value="{{ $job->job_name }}" placeholder="Name">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Job Type:</label>
                                            <div class="col-md-4">
                                                <select name="job_type" class="input validate[required] form-control">
                                                    @if($job->job_type == '' || $job->job_type == 'full time')
                                                        <option value="full time">Full time</option>
                                                        <option value="part time">Part time</option>
                                                        <option value="internship">Internship</option>
                                                        @elseif($job->job_type == 'part time')
                                                        <option value="part time">Part time</option>
                                                        <option value="full time">Full time</option>
                                                        <option value="internship">Internship</option>
                                                        @else
                                                        <option value="internship">Internship</option>
                                                        <option value="part time">Part time</option>
                                                        <option value="full time">Full time</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Sector:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="job_sector" value="{{ $job->job_sector }}" placeholder="Sector">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Job State:</label>
                                            <div class="col-md-4">
                                                <select name="job_state" class="input form-control  validate[required]" id="state">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jobtype" class="control-label col-sm-2">Job LGA:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="job_lga" value="{{ $job->job_lga }}" placeholder="Job LGA">
                                            </div>
                                            <label for="Jobname" class="control-label col-sm-2">Salary Expected:</label>
                                            <div class="col-md-4">
                                                <input type="number" class="input validate[required] form-control" name="job_salary" value="{{ $job->job_salary }}" placeholder="Job Salary Expected">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jobtype" class="control-label col-sm-2">City:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="job_city" value="{{ $job->job_city }}" placeholder="City">
                                            </div>
                                            <label for="Jobname" class="control-label col-sm-2">Town:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="job_town" value="{{ $job->job_town }}" placeholder="Town">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"> <span class="glyphicon glyphicon-pencil"></span>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                                    Journals and Monographs Published
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addJournalData') }}" id="save-job_applied_for" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="journal" class="control-label col-sm-2">Name:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="journal_name" value="" placeholder="Name">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Type:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="journal_type" value="" placeholder="Type">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Year Published:</label>
                                            <div class="col-md-4">
                                              <select  class="input validate[required] form-control" name="journal_published">
                                              <option value="">Select year</option>
                                              <script >

                                               for(var i=1900;i<=year;i++){
                                                 document.write('<option value="'+i+'">'+i +'</option>');
                                               }
                                                 </script>
                                               </select>
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Publisher:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="journal_publisher" value="" placeholder="Publisher">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-plus"></span> Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Publisher</th>
                                        <th>Year Published</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($journals as $journal)
                                        <tr>
                                            <td>{{ $ji++ }}</td>
                                            <td>{{ $journal->name }}</td>
                                            <td>{{ $journal->type }}</td>
                                            <td>{{ $journal->publisher }}</td>
                                            <td> {{ $journal->year_published }} </td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $journal->id }}/delete-journal-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                                    Work Habits
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@saveWorkHabitData') }}" id="save-job_applied_for" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                Can you work at night?
                                                @if($wkh->night_reading == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="no"> No
                                                    </label>
                                                @elseif($wkh->night_reading == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_reading" value="no"> No
                                                    </label>
                                                @endif

                                            </div>
                                            <div class="col-md-4">
                                                Can you drive at night?
                                                @if($wkh->night_driving == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="no"> No
                                                    </label>
                                                @elseif($wkh->night_driving == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="night_driving" value="no"> No
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                Do you have an established business?
                                                @if($wkh->have_business == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="no"> No
                                                    </label>
                                                @elseif($wkh->have_business == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_business" value="no"> No
                                                    </label>
                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                Do you have a drivers’ licence?
                                                @if($wkh->drivers_license == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="no"> No
                                                    </label>
                                                @elseif($wkh->drivers_license == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="have_drivers_license" value="no"> No
                                                    </label>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                How fast can you type?

                                                @if($wkh->typing_speed == 'very fast')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="very fast" checked> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="not very fast"> Not Very Fast
                                                    </label>
                                                @elseif($wkh->typing_speed == 'not very fast')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="very fast"> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="not very fast" checked> Not Very Fast
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="very fast"> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="typing_speed" value="not very fast"> Not Very Fast
                                                    </label>
                                                @endif

                                            </div>
                                            <div class="col-md-4">
                                                How fast can you code?
                                                @if($wkh->coding_speed == 'very fast')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="very fast" checked> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="not very fast"> Not Very Fast
                                                    </label>
                                                @elseif($wkh->coding_speed == 'not very fast')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="very fast"> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="not very fast" checked> Not Very Fast
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="very fast"> Very Fast
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="coding_speed" value="not very fast"> Not Very Fast
                                                    </label>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                Have you ever been in the armed forces?
                                                @if($wkh->armed_force == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="no"> No
                                                    </label>
                                                @elseif($wkh->armed_force == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="armed_force" value="no"> No
                                                    </label>
                                                @endif

                                            </div>
                                            <div class="col-md-4">
                                                Have you ever been convicted of any crime?
                                                @if($wkh->convicted == 'yes')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="yes" checked> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="no"> No
                                                    </label>
                                                @elseif($wkh->convicted == 'no')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="no" checked> No
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="yes"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="convicted" value="no"> No
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                When are you available to start work?
                                                @if($wkh->availability == 'immediately')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="immediately" checked> Immediately
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 week notice"> 1 Week Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 Month notice"> 1 Month Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="2 Month notice"> 2 Month Notice
                                                    </label>
                                                @elseif($wkh->availability == '1 week notice')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="immediately"> Immediately
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 week notice" checked> 1 Week Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 Month notice"> 1 Month Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="2 Month notice"> 2 Month Notice
                                                    </label>
                                                @elseif($wkh->availability == '1 Month notice')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="immediately"> Immediately
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 week notice"> 1 Week Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 Month notice" checked> 1 Month Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="2 Month notice"> 2 Month Notice
                                                    </label>
                                                @elseif($wkh->availability == '2 Month notice')
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="immediately"> Immediately
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 week notice"> 1 Week Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 Month notice"> 1 Month Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="2 Month notice" checked> 2 Month Notice
                                                    </label>
                                                @else
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="immediately"> Immediately
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 week notice"> 1 Week Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="1 Month notice"> 1 Month Notice
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="availibility" value="2 Month notice"> 2 Month Notice
                                                    </label>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
                                    Volunteer/Non Profit activities involved in:
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addNonProfit') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="journal" class="control-label col-sm-2">Name:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="name" value="" placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Position:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="position" value="" placeholder="What was your position?">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Year:</label>
                                            <div class="col-md-4">
                                              <select class="input validate[required] form-control" name="year">


                                              <option value="">Select year</option>
                                              <script >

                                               for(var i=1900;i<=year;i++){
                                                 document.write('<option value="'+i+'">'+i +'</option>');
                                               }
                                                 </script>
                                                   </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-plus"></span> Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Year</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($nonprofits as $nonp)
                                        <tr>
                                            <td>{{ $noni++ }}</td>
                                            <td>{{ $nonp->name }}</td>
                                            <td>{{ $nonp->position }}</td>
                                            <td> {{ $nonp->year }} </td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $nonp->id }}/delete-nonprofit-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">
                                    What have you done you consider so spectacular in your life? (100 letters Max)
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseTwelve" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@saveSpectacular') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label for="journal" class="control-label col-sm-4">what spectacular thing have you done?:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="input validate[required] form-control" name="name" value="{{ $spectacular->name }}" placeholder="What is the most spectacular thing you have done?">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Jobname" class="control-label col-sm-2"></label>
                                        <div class="col-md-4">
                                            <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">
                                    Hobbies:
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addHobby') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="journal" class="control-label col-sm-2">Name:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="name" value="" placeholder="Hobby Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-plus"></span> Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($hobbies as $hobby)
                                        <tr>
                                            <td>{{ $hobbyi++ }}</td>
                                            <td>{{ $hobby->name }}</td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $hobby->id }}/delete-hobby-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box panel default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen">
                                    Referees:
                                </a><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i>
                            </h4>
                        </div>
                        <div id="collapseFourteen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('JobseekerController@addReferee') }}" id="save-job_applied_for" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="journal" class="control-label col-sm-2">Name:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="name" value="" placeholder="Name">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Occupation:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="occupation" value="" placeholder="Occupation">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2">Organisation:</label>
                                            <div class="col-md-4">
                                                <input type="text" class="input validate[required] form-control" name="organisation" value="" placeholder="Organisation">
                                            </div>
                                            <label for="jobtype" class="control-label col-sm-2">Years known:</label>
                                            <div class="col-md-4">
                                                <input type="number" class="input validate[required] form-control" name="years" value="" placeholder="Years Known">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jobtype" class="control-label col-sm-2">Relationship:</label>
                                            <div class="col-md-10">
                                                <input type="text" class="input validate[required] form-control" name="relationship" value="" placeholder="Relationship">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Jobname" class="control-label col-sm-2"></label>
                                            <div class="col-md-4">
                                                <button type="submit" class="ibutton button2"><span class="glyphicon glyphicon-plus"></span> Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Occupation</th>
                                        <th>Organisation</th>
                                        <th>Relationship</th>
                                        <th>Year Known</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($referees as $referee)
                                        <tr>
                                            <td>{{ $ri++ }}</td>
                                            <td>{{ $referee->name }}</td>
                                            <td>{{ $referee->occupation }}</td>
                                            <td>{{ $referee->organisation }}</td>
                                            <td>{{ $referee->relationship }}</td>
                                            <td> {{ $referee->years }} </td>
                                            <td>
                                                <button type="button" onclick="window.location.href='/jobseeker/{{ $referee->id }}/delete-referee-data'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END IMPROVE PROFILE -->
            </div>
            <div class="right-column pull-left">
                @include('partials.connections')
                @include('partials.suggested-connections')
				@include('partials.chat')
            </div>

        </div>
    </div>


@endsection
