@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="margin-top:20px;">
         <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      Bio Data
                    </a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="col-md-3">
                           
                            </div>
                            <div class="col-md-9">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <colgroup>
                                        <col class="col-xs-3">
                                        <col class="col-xs-7">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">
                                                <code>Fullnames </code>
                                                </th>
                                                <td> {{ $user->fullnames }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Country </code>
                                                </th>
                                                <td> {{ $biodata->country }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>State</code>
                                                </th>
                                                <td> {{ $biodata->state }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>L.G.A</code>
                                                </th>
                                                <td> {{ $biodata->lga }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Marital Status</code>
                                                </th>
                                                <td> {{ $biodata->marital_status }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Birth Day </code>
                                                </th>
                                                <td> {{ $biodata->dob }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Religion </code>
                                                </th>
                                                <td> {{ $biodata->religion }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Race </code>
                                                </th>
                                                <td> {{ $biodata->race }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <colgroup>
                                        <col class="col-xs-1">
                                        <col class="col-xs-7">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">
                                                <code>Language spoken </code>
                                                </th>
                                                <td> {{ $biodata->language_spoken }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Language proficiency </code>
                                                </th>
                                                <td> {{ $biodata->language_proficiency }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>complexion </code>
                                                </th>
                                                <td> {{ $biodata->complexion }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Complexion </code>
                                                </th>
                                                <td> {{ $biodata->complexion }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Height </code>
                                                </th>
                                                <td> {{ $biodata->height }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Physical Features </code>
                                                </th>
                                                <td> {{ $biodata->physical_features }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                <code>Tribe </code>
                                                </th>
                                                <td> {{ $biodata->tribe }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                   Educational Qualifications
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>University</th>
                                    <th>Degree</th>
                                    <th>Course</th>
                                    <th>Year</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($educational as $edu)
                                <tr>
                                    <td>{{ $edui++ }}</td>
                                    <td>{{ $edu->university_attended }}</td>
                                    <td>{{ $edu->degree_obtained }}</td>
                                    <td>{{ $edu->course }}</td>
                                    <td>{{ $edu->year_graduated }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                   Secondry School Education
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>School Name</th>
                                    <th>Qualification</th>
                                    <th>Year</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($secondary as $sec)
                                <tr>
                                    <td>{{ $seci++ }}</td>
                                    <td>{{ $sec->secondary_school }}</td>
                                    <td>{{ $sec->secondary_school_qualification }}</td>
                                    <td>{{ $sec->secondary_school_year }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                   Vocational/Technical Schools Attended
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>School Name</th>
                                    <th>Qualification</th>
                                    <th>Year</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $vacation->school }}</td>
                                    <td>{{ $vacation->qualification }}</td>
                                    <td>{{ $vacation->year }}</td>
                                </tr>
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                  Professional Qualifications Obtained
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Profession</th>
                                    <th>Qualification</th>
                                    <th>Specialization</th>
                                    <th>Experience</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($profession as $prof)
                                <tr>
                                    <td>{{ $profi++ }}</td>
                                    <td>{{ $prof->profession_name }}</td>
                                    <td>{{ $prof->profession_year_obtained }}</td>
                                    <td>{{ $prof->profession_specialization }}</td>
                                    <td>{{ $prof->profession_experience }} yrs</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                   Work Experiences
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Place of work</th>
                                    <th>Position</th>
                                    <th>Sector</th>
                                    <th>Experience</th>
                                    <th>Reasons</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($work as $wk)
                                <tr>
                                    <td>{{ $wki++ }}</td>
                                    <td>{{ $wk->work_place }}</td>
                                    <td>{{ $wk->work_position }}</td>
                                    <td>{{ $wk->work_sector }}</td>
                                    <td>{{ $wk->work_experience }} yrs</td>
                                    <td>{{ $wk->work_reasons }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                   Skills
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Name</th>
                                    <th>Year</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($skills as $skill)
                                <tr>
                                    <td>{{ $skilli++ }}</td>
                                    <td>{{ $skill->skill_type }}</td>
                                    <td>{{ $skill->skill_name }}</td>
                                    <td>{{ $skill->skill_year }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                   Jobs Applied for/Sectors/Country you seek the job in:
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Country</th>
                                    <th>State</th>
                                    <th>L.G.A</th>
                                    <th>Job title</th>
                                    <th>Sector</th>
                                    <th>Type</th>
                                    <th>Salary Expected</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $job->job_country }}</td>
                                    <td>{{ $job->job_state }}</td>
                                    <td>{{ $job->job_lga }}</td>
                                    <td>{{ $job->job_name }}</td>
                                    <td>{{ $job->job_sector }}</td>
                                    <td>{{ $job->job_type }}</td>
                                    <td>{{ $job->job_salary }}</td>
                                </tr>
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                   Journals and Monographs Published
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseNine" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Publisher</th>
                                <th>Year Published</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($journals as $journal)
                            <tr>
                                <td>{{ $ji++ }}</td>
                                <td>{{ $journal->name }}</td>
                                <td>{{ $journal->type }}</td>
                                <td>{{ $journal->publisher }}</td>
                                <td> {{ $journal->year_published }} </td>
                            </tr>
                        @endforeach
                        </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseHabit">
                   Work Habits
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseHabit" class="panel-collapse collapse">
              <div class="panel-body">
                  <form class="form-horizontal form-validate" role="form" method="POST" action="">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                       Can you work at night?
                       @if($wkh->night_reading == 'yes')
                            <label class="radio-inline">
                                <input type="radio" name="night_reading" value="yes" checked disabled=""> Yes
                            </label>
                       @elseif($wkh->night_reading == 'no')
                           <label class="radio-inline">
                                <input type="radio" name="night_reading" value="no" checked disabled=""> No
                           </label>
                       @endif

                    </div>
                    <div class="col-md-4">
                       Can you drive at night?
                       @if($wkh->night_driving == 'yes')
                            <label class="radio-inline">
                                <input type="radio" name="night_driving" value="yes" checked disabled=""> Yes
                            </label>
                       @elseif($wkh->night_driving == 'no')
                           <label class="radio-inline">
                                <input type="radio" name="night_driving" value="no" checked disabled=""> No
                           </label>
                       @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        Do you have an established business?
                        @if($wkh->have_business == 'yes')
                            <label class="radio-inline">
                                <input type="radio" name="have_business" value="yes" checked disabled=""> Yes
                            </label>
                        @elseif($wkh->have_business == 'no')
                           <label class="radio-inline">
                                <input type="radio" name="have_business" value="no" checked disabled=""> No
                           </label>
                        @endif
                    </div>
                    <div class="col-md-4">
                       Do you have a drivers’ licence?
                       @if($wkh->drivers_license == 'yes')
                           <label class="radio-inline">
                                <input type="radio" name="have_drivers_license" value="yes" checked disabled=""> Yes
                            </label>
                       @elseif($wkh->drivers_license == 'no')
                           <label class="radio-inline">
                                <input type="radio" name="have_drivers_license" value="no" checked disabled=""> No
                           </label>
                       @endif

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                       How fast can you type?

                       @if($wkh->typing_speed == 'very fast')
                           <label class="radio-inline">
                                <input type="radio" name="typing_speed" value="very fast" checked disabled=""> Very Fast
                            </label>

                       @elseif($wkh->typing_speed == 'not very fast')
                           <label class="radio-inline">
                                <input type="radio" name="typing_speed" value="not very fast" checked disabled=""> Not Very Fast
                           </label>
                       @endif

                    </div>
                    <div class="col-md-4">
                       How fast can you code?
                       @if($wkh->coding_speed == 'very fast')
                           <label class="radio-inline">
                                <input type="radio" name="coding_speed" value="very fast" checked disabled=""> Very Fast
                            </label>
                       @elseif($wkh->coding_speed == 'not very fast')
                           <label class="radio-inline">
                                <input type="radio" name="coding_speed" value="not very fast" checked disabled=""> Not Very Fast
                           </label>
                       @endif

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                       Have you ever been in the armed forces?
                       @if($wkh->armed_force == 'yes')
                           <label class="radio-inline">
                                <input type="radio" name="armed_force" value="yes" checked disabled=""> Yes
                            </label>
                       @elseif($wkh->armed_force == 'no')
                           <label class="radio-inline">
                                <input type="radio" name="armed_force" value="no" checked disabled=""> No
                           </label>
                       @endif

                    </div>
                    <div class="col-md-4">
                       Have you ever been convicted of any crime?
                       @if($wkh->convicted == 'yes')
                           <label class="radio-inline">
                                <input type="radio" name="convicted" value="yes" checked disabled=""> Yes
                            </label>
                       @elseif($wkh->convicted == 'no')
                       <label class="radio-inline">
                            <input type="radio" name="convicted" value="no" checked disabled=""> No
                       </label>
                       @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                       When are you available to start work?
                       @if($wkh->availability == 'immediately')
                           <label class="radio-inline">
                                <input type="radio" name="availibility" value="immediately" checked disabled=""> Immediately
                            </label>
                       @elseif($wkh->availability == '1 week notice')
                           <label class="radio-inline">
                                <input type="radio" name="availibility" value="1 week notice" checked disabled=""> 1 Week Notice
                           </label>
                       @elseif($wkh->availability == '1 Month notice')
                           <label class="radio-inline">
                                <input type="radio" name="availibility" value="1 Month notice" checked disabled=""> 1 Month Notice
                           </label>
                       @elseif($wkh->availability == '2 Month notice')
                           <label class="radio-inline">
                                <input type="radio" name="availibility" value="2 Month notice" checked disabled=""> 2 Month Notice
                           </label>
                       @endif

                    </div>
                </div>
            </div>
        </form>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                    Volunteer/Non Profit activities involved in:
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Year</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($nonprofits as $nonp)
                                <tr>
                                    <td>{{ $noni++ }}</td>
                                    <td>{{ $nonp->name }}</td>
                                    <td>{{ $nonp->position }}</td>
                                    <td> {{ $nonp->year }} </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
                    The most spectacular thing i have done is:
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <code>{{ $spectacular->name }}</code>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">
                    Hobbies
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseTwelve" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($hobbies as $hobby)
                                <tr>
                                    <td>{{ $hobbyi++ }}</td>
                                    <td>{{ $hobby->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">
                    Referees:
                </a><i class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
              </h4>
            </div>
            <div id="collapseThirteen" class="panel-collapse collapse">
              <div class="panel-body">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Occupation</th>
                                    <th>Organisation</th>
                                    <th>Relationship</th>
                                    <th>Year Known</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($referees as $referee)
                                <tr>
                                    <td>{{ $ri++ }}</td>
                                    <td>{{ $referee->name }}</td>
                                    <td>{{ $referee->occupation }}</td>
                                    <td>{{ $referee->organisation }}</td>
                                    <td>{{ $referee->relationship }}</td>
                                    <td> {{ $referee->years }} </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>


        </div>
        </div>
    </div>
@endsection
