@extends('layouts.app')

@section('content')
		<div class="advanced-search-container">
		@include('layouts.top')

			<div class="row">
				<div class="col-md-12">
				<div class="col-md-2">
						<div class="candidates-panel" style="overflow-y:auto;float:left;">
							<div class="advanced-search-titles ">
								<h4><strong>Search Results:</strong></h4>
								<div id="candidate-count"></div>
								<div class="adv-search-result-display">
									<div id="advanced-search-results">
										

										{{-- <div class="row" >
											<div class="col-sm-6">
												<img src="{{ asset('img/profile.jpg') }}" style="display:inline" class="img-responsive " style="padding-top:15px" alt="Cinque Terre">
											   <p>Agahiu Emmanuel</p>
											</div>
										  <div class="col-sm-6">
											  <input type="checkbox" id="invites" name="" class="" value="">
										  </div>
									  </div>
								<div class="row">
											<div class="col-sm-6">
												<img src="{{ asset('img/profile.jpg') }}" style="display:inline" class="img-responsive " style="padding-top:15px" alt="Cinque Terre">
											<p>Bekisu Jumai</p>
											</div>
									<div class="col-sm-6">
										<input type="checkbox" name="" class="" value="">
									</div>
								</div> --}}
					
									</div>
									
								</div>
							</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="candidates-pane2" >
						<div class="advanced-search-titles">
							<h4><strong>Candidate's Profile:</strong></h4>
							<div id="advanced-search-profile-display"></div>
						</div>



</div>
				</div>


				<div class="col-md-4">
					<div class="candidates-pane2">
						<div class="advanced-search-titles">
							<h4><strong>Search Criteria:</strong></h4>
						</div>
						<form class="form-horizontal" id="jobseeker-form" role="form" method="POST" action="{{ action('SearchController@findJobseekers') }}" id="save-job_applied_for" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<h5><strong>Search by place of origin:</strong></h5>
						<div class="form-group">
							<div class="col-md-6">
								<select name="country" id="countries" class="input form-control" onchange="print_state('state', this.selectedIndex);">
								<option value="">Choose Country</option>
								</select>
							</div>
							<div class="col-md-6">
								<select name="state" class="input form-control" id="state" onclick="print_state('state',0)" >
									<option value="">Choose State</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="input form-control" name="lga" value="{{ old('lga') }}" placeholder="LGA">
							</div>
							<div class="col-md-6">
								<input type="text" class="input form-control" name="tribe" value="{{ old('town') }}" placeholder="Tribe">
							</div>
						</div>

						<h5><strong>Search by place of residence:</strong></h5>
						<div class="form-group">
							<div class="col-md-6">
								<select name="country2" id="countries2" class="input form-control " onchange="print_state('state2', this.selectedIndex);">
								<option value="">Choose Country</option>
								</select>
							</div>
							<div class="col-md-6">
								<select name="state2" class="input form-control " id="state2">
									<option value="">Choose State</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="input form-control" name="city" value="{{ old('city') }}" placeholder="City">
							</div>
							<div class="col-md-6">
								<input type="text" class="input form-control" name="district" value="{{ old('district') }}" placeholder="District">
							</div>
						</div>
						<h5><strong>Others:</strong></h5>

						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="input form-control" name="age" value="{{ old('age') }}" placeholder="Age">
							</div>
							<div class="col-md-6">
								<select name="gender" class="input form-control">
									<option value="">Choose Gender</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="input form-control" name="years" value="{{ old('years') }}" placeholder="Years of experience">
							</div>
							<div class="col-md-6">
								<select name="marital_status" class="input form-control">
									<option value="">Marital Status</option>
									<option value="Married">Single</option>
									<option value="Single">Married</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="input  form-control" name="position" value="{{ old('position') }}" placeholder="Position">
							</div>
							 <div class="col-md-6">
								<input type="text" class="input form-control" name="salary" value="{{ old('salary') }}" placeholder="Avg salary eg. 150,000">
							</div>
						</div>
						<div class="form-group">
					 	 <div class="col-md-6">
							 <input type="text" id="university" class="input validate[required] form-control" name="university_attended" value="" placeholder="University Attended">
                  <div class="" id="university_list"> </div>
					 	 </div>
					 		<div class="col-md-6">
								<select class="input validate[required] form-control" name="language_proficiency">
								 <option value="">Select language proficiency</option>
								 <option value="beginner">Beginner</option>
								 <option value="intermediate">Intermediate</option>
								 <option value="advance">Advance</option>
								</select>
					 	 </div>
					  </div>
						<div class="form-group">
							<div class="col-md-6">
								<button type="submit" class="ibutton button2"> <span class="glyphicon glyphicon-search"></span> Search </button>
							</div>
						</div>

					</form>
				</div>
				</div>
			</div>
			</div>
		</div>




@endsection
