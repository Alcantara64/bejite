@if(Auth::user()->getConnectionCount() > 0)
    <div class="chat-wrapper">
        <div class="chat-box my-connections">
            <div class="box-title">
                <h5><span class="fa fa-comment"></span> Chat</h5>
            </div>
            <div id="connection-list" class="box-content" style="max-height: 10;overflow-y: scroll">

            </div>
        </div>
    </div>
	<div class="clearfix"></div>
  <br>
@endif
