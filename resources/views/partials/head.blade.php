<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token"content="{{csrf_token()}}">
    <meta name="description" content="job recruitment website">
    <meta name="keywords" content="job, recruit, employment">
    <meta name="author" content="Igwe Timothy">
    <meta name="google-site-verification" content="7XpbS3IGuC1OX-B-ZtgHYbcD5dLeXaCMt9vsvkQxX4Q" />
    <title>Bejite - One stop jobsite</title>
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/validationEngine.jquery.css') }}" rel="stylesheet">
    <!--<link href="{{ asset('/css/pnotify.custom.min.css') }}" media="all" rel="stylesheet">-->
    <link href="{{ asset('/css/animate.min.css') }}" media="all" rel="stylesheet">
    <link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-social.css') }}" rel="stylesheet">
    <link href="{{ asset('css/chat.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Fonts -->
    <!--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @if(Auth::check())
        <style type="text/css">
            /*
            .navbar-default {
                background-color: rgba(98, 0, 118, 0.8);
                border-color: #f10084;
                border-left: none;
                border-right: none;
            }
            .navbar-default .navbar-nav > li > a {
                color: #fff;
            }
            .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover{
                background-color: rgba(0,0,0,0.15);
                color: #fff;
            }
            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus,.navbar-default .navbar-nav > .open > a:hover{
                background-color: rgba(98, 0, 118, 0.8);
                color: #fff;
            }
            .dropdown-menu > li > a:focus,.dropdown-menu > li > a:hover {
                background-color: rgba(98, 0, 118, 0.8);
                color: #fff;
            }
            */
        </style>
    @endif
    @if(Auth::check())
       <script type="text/javascript">
            var Options = {
                userId : "<?= Auth::user()->id ?>",
                savedPosts : "<?= Auth::user()->saved_posts ?>"
            }
        </script>
    @else
        <script type="text/javascript">
            var Options = {
                userId : 0,
                savedPosts : 0
            }
        </script>
    @endif
</head>