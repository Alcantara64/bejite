
<div class="profile-cover box">
    <div id="cover" class="cover" style="">
        <div class="col-md-3">
            <!--<div class="action-button">
                @if(Auth::check() && strtolower($profile->username) == strtolower(Auth::user()->username))
                    <button id="change-cover-photo" class="custom-button file-upload" data-target="cover-photo-upload" type="button"><span class="fa fa-camera"></span> Add Cover Photo </button>
                @endif
            </div>-->
            <div class="profile-photo">
                <a href="" class="profile">
                    <img class="current-avatar" src="{{ asset('/users/'.$profile->photo) }}">
                </a>
                @if(Auth::check() && strtolower($profile->username) === strtolower(Auth::user()->username))
                    <form class="form-validate" id="change-cover-image-form" role="form" method="POST" action="{{ action('UploadController@coverImage') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="file" name="photo" data-target-form="#change-cover-image-form" data-target-div="#user-custom-bg" class="cover-photo-upload" id="cover-photo-upload" style="display: none">
                    </form>
                    <a href="#" id="change-profile-photo-button" data-target="photo-upload" class="file-upload" data-button-id="#change-profile-photo-button">
                        <span class="fa fa-camera"></span> Change Photo
                    </a>
                    <form class="form-validate" id="change-photo-form" role="form" method="POST" action="{{ action('UploadController@profilePhoto') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="file" name="photo" data-target-form="#change-photo-form" data-target-img=".current-avatar" class="profile-photo-upload" id="photo-upload" style="display: none">
                    </form>
                @endif
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-name">
                <h1><a href="/{{ $profile->username }}">{{ ucwords($profile->fullnames) }}</a></h1>
                <div class="item-links-container">
                    <div class="account-type">
                        <span class="label label-custom"><span class="fa fa-briefcase"></span> {{ ucfirst($profile->account_type) }}</span>
                    </div>
                    <div class="items-links">
                        <span class="title">Country : </span>
                        <span class="detail">{{ $profile->country }}</span>
                    </div>
                    @if($hasEducationData > 0)
                        <div class="items-links">
                            <span class="title">Studied : </span>
                            <span class="detail">{{ $education->course }}</span>
                        </div>
                        <div class="items-links">
                            <span class="title">Studied at : </span>
                            <span class="detail">{{ $education->university_attended }}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="profile-nav">
        <div class="clearfix">
            <ul class="pull-right">
                
                @if(strtolower($url) == $profile->username.'/connections')
                    <li class="active">
                        <a href="#">
                            Connections
                            @if(\App\User::getConnectionCount($profile->id) > 0)
                                <span class="badge">{{ \App\User::getConnectionCount($profile->id) }}</span>
                            @endif
                        </a>
                    </li>
                @else
                    <li>
                        <a href="#">
                            Connections
                            @if(\App\User::getConnectionCount($profile->id) > 0)
                                <span class="badge">{{ \App\User::getConnectionCount($profile->id) }}</span>
                            @endif
                        </a>
                    </li>
                @endif
               @if(!Auth::user()->isFollowing($profile->id) && $profile->id != Auth::user()->id)
				   <li>
				<div style="padding-right: 10px;padding-bottom: 10px;;">
					<button type="button" class="btn btn-primary btn-xs" id="connect-btn-{{ $profile->id }}" onclick="follow({{ $profile->id }},this);"><span class="glyphicon glyphicon-transfer"></span> Connect </button>
				</div>
			   </li>
			   @endif
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>