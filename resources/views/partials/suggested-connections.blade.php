<div class="box4">
    <div class="recent-connection-title"><h5>People you may know</h5></div>
    <div class="recent-connection-content">
        @foreach(Auth::user()->findFivePeople() as $connection)
                <div class="connection-item " >
                    <div class="pull-left" style="margin-right: 10px;">
                        <a href="/{{ $connection->username }}">
                            <img class="avatar-xx circle-img" src="{{ asset('/users/'.$connection->photo) }}">
                        </a>
                    </div>
                    <div class="pull-left fullnames">
                        <a href="/{{ $connection->username }}">
                            {{ ucwords($connection->fullnames) }}
                        </a><br>
						<a href="/{{ $connection->username }}">
                            {{ ucwords($connection->account_type) }}
                        </a>
                        <div style="padding-top: 5px;padding-bottom: 5px;;">
                            <button type="button" class="btn btn-primary btn-xs" id="connect-btn-{{ $connection->id }}" onclick="follow({{ $connection->id }},this);"><span class="glyphicon glyphicon-transfer"></span> Connect </button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

        @endforeach
    </div>
</div>
