<div class="box-feed panel panel-default noheading" style="margin-top: 0;">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-1">
                <img src="{{ asset('/users/'.Auth::user()->photo) }}" class="avatar-xs-circle change-avatar">
            </div>
            <div class="col-md-11">
                <form>
                    <!--<div contentEditable="true" class="form-control post-txt normal-txt" placeholder="Whats new with you?" id="post-txt"></div>-->
                    <textarea class="form-control post-txt normal-txt" placeholder="Whats new with you?" id="post-txt"></textarea>
                    <!--
                    <div class="controls-container hide" id="post-controls">
                    <div class="post-icons-container">
                    <span class="icon glyphicon glyphicon-camera" id="photo-dialog"></span>
                    <span class="icon glyphicon glyphicon-facetime-video"></span>
                </div>
                <div class="post-btn-container">
                    <button type="button" id="post_btn" style="padding: 5px;padding-right:10px;" class="btn btn-primary btn-xs pull-right">Post</button>
                </div>
                </div>
                    -->
                </form>
            </div>
        </div>
    </div>
</div>