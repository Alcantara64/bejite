<!-- Static navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    
        <!-- flash data markup -->
        <div id="flash-data" class="flash-wrapper bg">
            <p>Your post was shared successfully</p>
        </div>
        <!-- end flash data markup-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
			<span>
				<a class="navbar-brand" href="{{ url('/') }}" style="color:#d70076;">
					<img class="logo-img" src="{{ asset('logo/bejite_logo_red.png') }}" alt="Bejite Logo">
				</a>
			</span>
			
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            </ul>
            @if(Auth::check() && Auth::user()->reg_status == 'completed')
                <form class="navbar-form navbar-left search-form" role="search">
                    <div class="form-group">
                        <input type="text" id="search-query" onkeyup="javascript:search();" class="form-control" placeholder="I am looking for...">
                    </div>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </form>
            @endif
            <ul class="nav navbar-nav navbar-right user-icons" style="padding-right:20px">
                @if (Auth::guest())
                    <!--<li><a href="#" onclick="javascript:return false;">Already a member?</a></li>
                    <li>
                        <a data-toggle="modal" data-target="#myLogin" href="#" class="btn btn-default navbar-btn btn-radius btn-extra" style="margin-bottom:6px;margin-top:6px;padding-bottom:10px;padding-top:10px;padding-left:10px;padding-right:10px;">
                            <span class="fa fa-key"></span> Login
                        </a>
                    </li>-->
                @else
                    <li><a href="{{ url('/') }}"><span class="glyphicon glyphicon-home "></span></a></li>
                    @if(Auth::user()->getUreadMessagesCount() > 0)
                        <li>
                            <a href="#" onclick="javascript:void(0)" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="glyphicon glyphicon-comment"></span>
                                <span class="badge badge-notify"  id="notice-count">{{ Auth::user()->getUreadMessagesCount() }}  {{ App\Messages::markAsDelivered() }} </span>
                            </a>

                            <ul class="pending-requests dropdown-menu msg-cont " role="menu">
                                @foreach(Auth::user()->getUreadMessages() as $msg)
                                    <li>
                                        <div class="user media" id="msg-user-{{ $msg->id }}">
                                            <div class="media-object pull-left">
                                                <a href="{{ url($msg->username) }}">
                                                    <img class="avatar-xx circle-img" src="{{ asset('/users/'.$msg->photo) }}">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="{{ url($msg->username) }}">
                                                    <strong>
														<h5 class="media-heading">
                                                        {{ ucwords($msg->fullnames) }}
														</h5>
													</strong>
                                                </a>
												 <span> -{{ $msg->content }}</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                <li>
                                    <button type="button" class="btn btn-primary btn-block btn-xs" onclick="window.location='/messages'">View all messages</button>
                                </li>
                            </ul>

                        </li>
                    @else
                       <li>
							<a href="#" onclick="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-comment"></span>
								<span class="badge badge-notify" style="position:fixed; id="notice-count"></span>
							</a>
							<ul class="pending-requests dropdown-menu msg-cont" role="menu">
								<div class="user media notification-heading">
									No new messages.
								</div>
							</ul>
						</li>
                    @endif
                    @if(Auth::user()->connectionNotificationCount()>0)
						<li>
                        <a id="notified" href="#" onclick="notify();" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-bell"></span>
                            <span class="badge badge-notify" ">{{ App\User::connectionNotificationCount() }}</span>
                        </a>
						<ul class="pending-requests dropdown-menu msg-cont " role="menu">
							@foreach(Auth::user()->getConnectionNotifications() as $connect)
								<li>
									<div class="user media" id="user-{{$connect->id}}">
										<div class="media-body notification-heading">
											<span> You are now connected to</span>
										</div>
										<div class="media-object pull-left">
											<a href="{{$connect->username}}">
												<img class="avatar-xx circle-img" src="{{ asset('/users/'.$connect->photo) }}">
											</a>
										</div>
										<div class="media-body">
											<span>
												<a href="{{$connect->username}}">
													<strong>
														<h5 class="media-heading">
															{{ ucwords($connect->fullnames) }}
														</h5>
													</strong>
												</a>
											</span>
										</div>
									</div>
								</li>
							@endforeach
						</ul>
                    </li>
					@else
						<li>
							<a href="#" onclick="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-bell"></span>
								<span class="badge badge-notify" id="notice-count"></span>
							</a>
							<ul class="pending-requests dropdown-menu msg-cont" role="menu">
								<div class="user media notification-heading">
									You have no new notifications.
								</div>
							</ul>
						</li>
					@endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img src="{{ asset('users/'.Auth::user()->photo) }}" class="avatar-xs change-avatar current-avatar prof-photo"> &nbsp;{{ ucfirst(Auth::user()->fullnames) }} <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/{{ Auth::user()->username }}">My Profile</a></li>
                            <li><a href="{{ action('UserController@profileSettings') }}">My Settings</a></li>
                            @if(Auth::user()->account_type == 'jobseeker')
                                <li><a href="{{ action('JobseekerController@showUpdateData') }}">Update CV</a></li>
                            @endif
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    
</nav>