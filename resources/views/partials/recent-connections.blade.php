<div class="box4 ">
    <div class="recent-connection-title">
        <h5>Recent Connections</h5>
    </div>
    <div class="recent-connection-content">
        @foreach(\App\User::getFiveConnections() as $connection)
            <div class="connection-item" id="user-div-{{ $connection->id }}">
                <div class="pull-left" style="margin-right: 10px;">
                    <a href="/{{ $connection->username }}">
                        <img class="avatar-xx circle-img" src="{{ asset('/users/'.$connection->photo) }}">
                    </a>
                </div>
                <div class="pull-left fullnames">
                    <a href="/{{ $connection->username }}">
                        {{ ucfirst(explode(" ", $connection->fullnames)[0]) }}
                    </a>
					<p><a href="/{{ $connection->username }}">
                        {{  $connection->account_type }}
                    </a></p>

                </div>
                <div class="clearfix"></div>
            </div>
        @endforeach
    </div>
</div>
