<div class="list-group" id="sidebar">
    <a class="list-group-item" href="">
        <img class="avatar-custom change-avatar" src="{{ asset('users/'.Auth::user()->photo) }}"> &nbsp;
        <span class="username">{{ ucfirst(Auth::user()->fullnames) }}</span>
    </a>
    <a class="list-group-item" href="/home"> Newsfeed
        <span class="glyphicon glyphicon-chevron-right pull-right"></span>
    </a>
    <a class="list-group-item" href="/{{ Auth::user()->username }}">
        My Profile <span class="glyphicon glyphicon-chevron-right pull-right"></span>
    </a>
    <a class="list-group-item" href="/{{ Auth::user()->username }}/connections"> Connections
	<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	<span class="badge" id="user-saved-posts">{{ Auth::user()->getConnectionCount() }}</span></a>
    @if(Auth::user()->account_type =='jobseeker')
        <a class="list-group-item" href="{{ action('JobseekerController@showUpdateData') }}">
            Update CV <span class="glyphicon glyphicon-chevron-right pull-right"></span>
        </a>
    @elseif(Auth::user()->account_type =='employer')
        <a class="list-group-item" href="{{ action('SearchController@home') }}">
            Recruit Here <span class="glyphicon glyphicon-chevron-right pull-right"></span>
        </a>
    @endif
    <a class="list-group-item" href="{{ action('MessageController@index') }}"> Messages <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
    <a class="list-group-item" href=""> Saved Posts
        <span class="glyphicon glyphicon-chevron-right pull-right"></span>
		<span class="badge" id="user-saved-posts">{{ Auth::user()->getSavedPostsCount() }}</span>
    </a>

</div>
