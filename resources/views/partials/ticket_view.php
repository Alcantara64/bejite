<?php include 'head.php'?>
<?php include 'navigator.php'?>
<div class="page-wrapper" style="min-height: 406px;">
    <div class="container-fluid">
      <!-- Title -->
      <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h5 class="txt-dark">Ticket Message</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>/home/profile">Dashboard</a></li>
            <li><a href="<?php echo base_url();?>/home/profile/ticket"><span>tickets</span></a></li>
            <li class="active"><span>view message</span></li>
          </ol>
        </div>
        <!-- /Breadcrumb -->
      </div>
      <!-- /Title -->


      <!-- Row -->
      <br>


<?php $message = $this->db->limit(6,0)->order_by('time','desc')->get_where('ticket_message',array('ticket_id'=>$ticket))->result_array();
foreach ($message as $row) {
  if($row['from_where'] ==$this->session->userdata('user_id')){
     $from = 'Me';
  }else{
    $from ='Admin';
  }
    echo    '<div class="row"><div class="col-lg-12">
          <div class="well well-sm card-view">
            <h6 class="mb-15">'.$from.'</h6>
            <p>'.$row['message'].'</p>
          </div>
        </div>
      </div>';
      }
      ?>
      <!-- /Row -->
<div class="row">
  <div class=" col-sm-12 col-xs-12">
<?php echo form_open(base_url().'home/ticket_reply/'.$ticket,array('class'=>'form-horizontal','id'=>'reply','onsubmit'=>'return false'));?>
  <input type="hidden" name="" id="user" value="user">
    <div class="form-group">
      <textarea name="reply" class="form-control" rows="5" >

      </textarea>

    </div>
    <div class="text text-danger" id="error"></div>
    <div class="form-group">
      <button type="submit" onclick="tickets('reply')"class="btn btn-success pull-right">reply</button>
    </div>

  </div>
</div>


    </div>

<?php include 'footer.php'?>
