@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:20px;">
        <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal form-validate" role="form" method="POST" action="{{ url('/account/employer') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-offset-2 col-md-8 signup-step2">
                                <div class="panel panel-default panel-noheading">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-4">
                                                <button data-target="logo-file" class="btn btn-primary btn-sm dialog" type="button" style="margin-top: 20px;">
                                                    <span class="glyphicon glyphicon-cloud-upload"></span> Select file
                                                </button>
                                            </div>
                                            <div class="col-md-5" style="text-align: left;">
                                                <img class="company-logo" src="{{ asset('img/company_logo.png') }}" alt="Company logo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-10 ">
                                                <label class="radio-inline">
                                                    <input type="radio" name="account_type" value="company" checked="checked"> We are a company
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="account_type" value="individual"> I am an Individual
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-10 ">
                                                <input type="text" class="input form-control validate[required]" name="company" value="{{ old('company') }}" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-10 ">
                                                <input type="text" class="input form-control validate[required]" name="address1" value="{{ old('address1') }}" placeholder="Company Address Line 1">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-10 ">
                                                <input type="text" class="input form-control" name="address2" value="{{ old('address2') }}" placeholder="Company Address Line 2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-5 col-md-offset-1">
                                                <input type="text" class="input form-control" name="website" value="{{ old('website') }}" placeholder="Website">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" min-value="1" class="input form-control validate[required]" name="support_email" value="{{ old('support_email') }}" placeholder="Company Support email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-10 ">
                                                <input type="text" class="input form-control validate[required]" name="position" value="{{ old('position') }}" placeholder="what is your position in the company?">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-1">
                                                <button type="submit" class="ibutton button2">
                                                    <span class="glyphicon glyphicon-pencil"></span> Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            <form class="form-horizontal form-validate" role="form" method="POST" action="{{ action('EmployerController@saveEmployerLogo') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="file" id="logo-file" name="logo" style="display: none;">
                            </form>
        </div>
    </div>
@endsection
