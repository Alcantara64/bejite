@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:20px;">
    <div class="row">

                    <form class="form-horizontal form-validate" id="signup-form-step2" role="form" method="POST" action="{{ action('AccountController@saveSetup') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-offset-2 col-md-8 signup-step2">
                        <div class="panel panel-default panel-noheading">
                            <div class="panel-body">
                            <div class="heading col-md-offset-1 col-md-10">
							<div id="signup-error"></div>
                               <h3>Hi, {{ explode(' ',Auth::user()->fullnames)[0] }}</h3>
                               <p>Tell Us a Little More About Yourself</p>
                            </div>
                            <div class="form-group">
							
                                <div class="col-md-offset-1 col-md-10 ">
                                    <label class="radio-inline">
                                      <input type="radio" name="account_type" value="jobseeker" checked="checked"> I am Jobseeker
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="account_type" value="employer"> I am an Employer
                                    </label>
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-10">
                                <input type="text" class="input form-control validate[required]" name="address" value="{{ old('address') }}" placeholder="Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-5">
                                <select name="country" id="country" class="input form-control  validate[required]" onchange="print_state('states', this.selectedIndex);">

                                </select>
                            </div>
                            <div class="col-md-5">
                                <select name="state" class="input form-control validate[required]" id="states">
                                <option value="">Choose state</option>}
                                option
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-5">
                                <input type="text" class="input form-control validate[required]" name="city" value="{{ old('city') }}" placeholder="City">
                            </div>
                            <div class="col-md-5">
                                <input type="text" min-value="1" class="input form-control validate[required]" name="phone" value="{{ old('phone') }}" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-10">
                                <input type="text" class="input form-control" name="hear_about_us" value="{{ old('hear_about_us') }}" placeholder="How did you hear about us? (optional)">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-10">
                                <button type="submit" class="save btn btn-primary btn-block">
                                    <span class="glyphicon glyphicon-pencil"></span> Save & Continue
                                </button>
                            </div>
                        </div>
                        </div>
                    </div>
                        </div>
                    </form>

    </div>
    </div>
@endsection
