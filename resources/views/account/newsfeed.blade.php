@extends('layouts.app')

@section('content')

<div class="container-fluid" style="margin-top:20px;">

        <div class="row" style="">

				<div class="col-md-4 col-sm-3 hidden-sm hidden-xs" >
          <div class="row">
            <div class="col-sm-5 hidden-xs  hidden-sm ">

                @include('partials.join')

            </div>
            <div class="col-sm-7" >
					<div class="user-nav">
						@include('partials.usernav')
						@include('partials.recent-connections')
					</div>
          </div>
				</div>
        </div>
        <!--inner row ends here -->
				<div class="col-md-5">
					<div>
						@include('partials.postbox')
						<div id="feeds-container"></div>
					</div>
				</div>
				<div class="col-md-3 .d-none .d-sm-block">
          <div class="row">
            <div class="col-sm-6">

              <div class="user-nav-static ">
    					@include('partials.chat')
    					 @include('partials.suggested-connections')
    					</div>

            </div>
            <div class="col-sm-6">
  @include('partials.advert')
            </div>
          </div>

				</div>




 <div tabindex="-1" class="post-box-container sweet-alert  showSweetAlert visible" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: none; margin-top: -300px;">
     <div class="post-box">
         <form class="form-horizontal form-validate" id="post-box-form" role="form" method="POST" action="{{ action('PostController@savePost') }}">
         <div class="box-content">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="file" onchange="javascrpt:PostBox.addPhotos(this);" id="feed-photo" style="display: none;" name="photos[]" multiple>
                 <div class="form-group">
                     <div class="control-label col-md-2">
                         <img src="{{ asset('/users/'.Auth::user()->photo) }}" class="img-circle post-box-avatar change-avatar">
                     </div>
                     <div class="col-md-10">
                         <textarea id="post" name="post" class="post-content auto-grow" placeholder="What's on your mind?"></textarea>
                         <div class="media-selected hide" id="media-photo-selected">
                             <span class="fa fa-camera"></span>
                             <span class="badge img-count"></span>
                             Photo(s) Selected
                             <a href="#" class="pull-right" onclick="javascript:PostBox.removePhotos(); return false;">
                                 <span class="glyphicon glyphicon-remove"></span>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-2">
                         Attach:
                     </label>
                     <div class="col-md-10">
                         <ul class="post-icons">
                             <li>
                                 <a href="#" onclick="javascript:fileDialog('feed-photo'); return false;">
                                     <span class="fa fa-camera primary"></span> <span class="txt"> Photos</span>
                                     <span class="badge badge-notify badge-special img-count"></span>
                                 </a>
                             </li>
                             <!--
                             <li><a href="#"><span class="fa fa-video-camera green"></span> <span class="txt"> Videos</span> </a> </li>
                             <li><a href="#" onclick="javacript:toggleShowTag(); return false;"><span class="fa fa-user-plus"></span></a> </li>
                             -->
                         </ul>
                     </div>
                 </div>

         </div>
         <div class="box-footer">
             <button class="btn btn-primary share" type="submit">Post</button>
             <button class="btn btn-default cancel" type="button">Cancel</button>
         </div>
         </form>
     </div>
     </div>
   </div>

</div>





@endsection
