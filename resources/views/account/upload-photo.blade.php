@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="col-md-offset-1 col-md-10">
        <div class="getstarted-left">
            <div class="box upload-photo-container padded">
                <div class="change-photo">
                    <div class="col-md-5 profile-img">
                        <img class="avatar-lg circle-img current-avatar" src="{{ asset('users/'.Auth::user()->photo) }}">
                    </div>
                    <div class="col-md-7">
                        <button type="button" data-target="photo-upload" class="file-upload btn btn-danger btn-xs"><span class="glyphicon glyphicon-cloud"></span> Change Photo</button>
                        <p class="help-block">Max size : 2M, jpg,png or gif only</p>
                        <form class="form-validate" id="change-photo-form" role="form" method="POST" action="{{ action('UploadController@profilePhoto') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="file" name="photo" data-target-form="#change-photo-form" data-target-img=".current-avatar" class="profile-photo-upload" id="photo-upload" style="display: none">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="user-data">
                    <form id="step-two-form" class="form-validate" role="form" method="POST" action="{{ action('AccountController@saveStepTwo') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div id="signup-error"></div>
                        <div class="form-group">
                            <label class="control-label">Username:</label>
                            <input type="text" class="input form-control  validate[required]" name="username" value="{{ Auth::user()->username }}" maxlength="100" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <button id="save-step1-btn" class="save btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-pencil"></span> Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div id="find-people" class="getstarted-right shadow">
            <div class="">
                <h2 class="title">
                    See who's here
                    <span>Find and follow at least 2 people to get started </span>
                </h2>
                <div class="member-container" id="member-container"></div>
                <div class="member-footer">
                    <button id="continue-btn" class="btn btn-primary btn-block" type="button" onclick="window.location='/getting-started/finish'" disabled="">Continue <span class="glyphicon glyphicon-menu-right"></span><span class="glyphicon glyphicon-menu-right"></span>
                    </button>
                </div>
            </div>
        </div>

    </div>
    </div>

    <form class="form-horizontal form-validate" id="profile-photo-form" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/account/upload-photo') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="file" id="photo-btn" class="hide input form-control" name="photo">
    </form>
@endsection
