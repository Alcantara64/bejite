@extends('layouts.app')
@section('content')
<div class="container white red-line">
    <div class="page-container padded">
        <div class="page-title">
          <h4>{{$page->title}}</h4>
        </div>
        <div class="page-content">
          {!! $page->content !!}
        </div>
    </div>
</div>
@endsection