@extends('layouts.app')

@section('content')

		@include('layouts.top')
<h4 class="text-center">Store</h4>
<hr/>
		<div class="row">
      <div class="col-sm-3">
        <div class="panel bejite-color ">
        <div class="panel-heading default-padding"></div>
        <div class="panel-body">
         <ul class="list-group">
           <a class="list-group-item"href="/home"><li>Newsfeed</li></a>
           <a class="list-group-item"href="#"><li>Clubs</li></a>
           <a class="list-group-item"href="#"><li>Photos</li></a>
           <a class="list-group-item"href="#"><li>Videos</li></a>
         </ul>
        </div>


      </div>

      </div>
      <div class="col-sm-6">
     <form class="form" action="index.html" method="post">
       <div class="form-group">
         <label for="products" class="col-sm-3">Products:</label>
          <div class="col-sm-8">
         <input type="text" name="" class="form-control" value="">
       </div>
     </div>
       <div class="form-group">
         <label class="col-sm-3" for="price">Price:</label>
           <div class="col-sm-8">
                <input type="text" name="" class="form-control" value="">
               </div>
       </div>
       <div class="form-group">
         <label for="description" class="col-sm-3">Description:</label>
          <div class="col-sm-8">
         <input type="text" name="" class="form-control" value="">
       </div>
     </div>
     <div class="form-group">
       <label for="picture" class="col-sm-3">Picture:</label>
        <div class="col-sm-8">
       <input type="text" name="" class="form-control" value="">
     </div>
   </div>
   <br>
   <div class="form-group " style="margin-top:10px">
     <div class="col-sm-8 col-sm-offset-3">
     <button type="button" class="btn btn-primary pull-right" name="button">Submit</button>
   </div>
 </div>
     </form>
      </div>
      <div class="col-sm-3">
        @include('partials.advert')

      </div>
    </div>


@endsection
