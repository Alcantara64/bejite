<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Educationaldata extends Model {

    protected $table = 'educationaldatas';
	protected $fillable = ['*'];
    protected $hidden = ['id'];

}
