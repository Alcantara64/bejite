<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Common extends Model
{
    public function __construct(){}

    public static function getMonths()
    {
        return [
            '1' => 'January',
            '2' => 'Febuary',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
    }
}
