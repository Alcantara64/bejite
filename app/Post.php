<?php

namespace App;
use App\Comments;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'image',
        'content',
        'user_id',
        'type',
    ];

    public static function getAuthorName($id)
    {
        $author = User::find($id);
        return ucwords($author->firstname.' '.$author->lastname);
    }
    public function user()
    {
        $this->belongsTo('App\User');
    }
    public static function getPost($postId)
    {
        $post = self::where('posts.id',$postId)
            ->join('users','posts.user_id','=','users.id')
            ->select('users.fullnames','users.username','users.account_type','users.photo','users.status','posts.id','posts.user_id','users.saved_posts','posts.content','posts.image','posts.created_at')
            ->first();
        $post['comments'] = Post::getComments($postId);
        $post['likes'] = Post::getTenLikes($postId);
        return $post;
    }
    public static function getComments($postId)
    {
        $comments = Comments::where('post_id',$postId)
            ->join('users','comments.user_id','=','users.id')
            ->select('users.fullnames','users.username','users.photo','comments.id','comments.content','comments.created_at')
            ->get();
        return $comments;
    }
    public static function getComment($id)
    {
        $comment = Comments::where('comments.id',$id)
            ->join('users','comments.user_id','=','users.id')
            ->select('users.fullnames','users.photo','comments.id','comments.content','comments.created_at')
            ->first();
        return $comment;
    }
    public static function getTenLikes($postId)
    {
        $likes = Like::where('post_id',$postId)
            ->join('users','likes.user_id','=','users.id')
            ->select('users.fullnames','users.username','users.photo','users.id as userId','likes.id','likes.post_id','likes.type')
            ->limit(10)
            ->get();
        return $likes;
    }
    public static function getLikes($postId)
    {
        $likes = Like::where('post_id',$postId)
            ->join('users','likes.user_id','=','users.id')
            ->select('users.fullnames','users.username','users.status','users.photo','likes.id','likes.type')
            ->get();
        return $likes;
    }
    public static function getTotalPostsCount()
    {
        return count(Post::all());
    }
    public static function getTotalPostsToday()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Post::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 1)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalPostsThisWeek()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Post::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 7)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalPostsThisMonth()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Post::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 30)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalPostsThisYear()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Post::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 365)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function isOwner($postId)
    {
        $count = self::where('id',$postId)->where('user_id',Auth::user()->id)->count();
        if($count == 1)
        {
            return true;
        }
    }
}
