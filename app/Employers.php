<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Employers extends Model {

	protected $fillable = ['address1','address2','user_id','website','support_email','position','user_id'];

    protected $hidden = ['id'];

}
