<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model {

    protected $fillable = ["*"];
    protected $hidden = ["id"];

    public static function deleteComments($id)
    {
        self::where('post_id',$id)->delete();
    }
}
