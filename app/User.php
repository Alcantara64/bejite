<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullnames',
        'username',
        'email',
        'profession_id',
        'time',
        'bday',
        'bmonth',
        'byear',
        'status',
        'account_status',
        'country',
        'state',
        'city',
        'photo',
        'phone',
        'password',
        'is_online',
        'chats',
        'account_type',
        'reg_status',
        'cover_photo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password','remember_token'];

    public function isAdmin($userId = null){
        $user = null;
        if($userId == null){
            if(Auth::check()){
                $user = self::find(Auth::user()->id);
            }
        }else{
            $user = self::find($userId);
        }
        if($user!==null){
            if($user->account_type == 'admin'){
                return true;
            }
        }
    }
    function getEmployerLogo($id,$class='small-avatar'){
        $em = Employers::find($id);
        if(is_file(storage_path()."/app/logos/".$em->logo))
            return "<img src='/images/logo-image/".$em->logo."' class='".$class."'>";
        return "<img src='".asset('img/company_logo.png')."' class='".$class."'>";
    }
    public function isFollowing($id)
    {
        $user = Auth::user()->id;
        $count = Connections::where('user_id',$user)
            ->where('follower',$id)
            ->orWhere(function($query) use ($id){
                $query->where('user_id',$id)
                    ->where('follower',Auth::user()->id);
            })
            ->count();
        if($count > 0)
        {
            return true;
        }
    }

    public function post()
    {
        $this->hasMany('App/Post');
    }
    public static function getConnections($userId = 0)
    {
        if($userId == 0)
        {
            $userId = Auth::user()->id;
        }
        $connections = Connections::where('user_id',$userId)
            ->join('users','connections.follower','=','users.id')
            ->select('users.*','connections.follower')
			->orderBy('connections.id','desc')
            ->get();
        return $connections;
    }
    public static function getFiveConnections($userId = 0)
    {
        if($userId == 0)
        {
            $userId = Auth::user()->id;
        }
        $connections = Connections::where('user_id',$userId)
            ->join('users','connections.follower','=','users.id')
            ->select('users.*','connections.follower')
            ->limit(5)
            ->orderBy('connections.id','desc')
            ->get();
        return $connections;
    }
    public static function getConnectionCount($userId = 0)
    {
        if($userId == 0)
        {
            $userId = Auth::user()->id;
        }
        $count = Connections::where('user_id',$userId)->count();
        return $count;
    }

    public function hasConnection($requester)
    {
        $count = Connections::where('user_id',Auth::user()->id)->where('user',$requester)->count();
        if($count > 0)
        {
            return true;
        }
    }
    public function hasPendingRequest()
    {
        $count = $this->getPendingRequestCount();
        if($count > 0)
        {
            return true;
        }
    }
    public function isPendingRequest($id)
    {
        $count = Request::where('receiver_id',Auth::user()->id)
            ->where('requester_id',$id)
            ->count();
        if($count == 1)
        {
            return true;
        }
    }
    public function getPendingRequestCount()
    {
        $count = Request::where('receiver_id',Auth::user()->id)->count();
        return $count;
    }
    public function getTenPendingRequests()
    {
        $requesters = Request::where('receiver_id',Auth::user()->id)
            ->join('users','requests.requester_id','=','users.id')
            ->select('users.*','requests.requester_id')
            ->limit(10)
            ->get();
        return $requesters;
    }
    public function getPendingRequests()
    {
        $requesters = Request::where('receiver_id',Auth::user()->id)
            ->join('users','requests.requester_id','=','users.id')
            ->select('users.*','requests.requester_id')
            ->get();
        return $requesters;
    }
    public function profession($id = null)
    {
        if($id == null)
        {
            $id = Auth::user()->profession_id;
        }
        $profession = Profession::find($id);
        return $profession;
    }
    public function findFivePeople()
    {
        $connections = Connections::where('user_id',Auth::user()->id)
            ->select('follower')
            ->get();

        $pending = [];
        $data  = [];
        foreach ($connections as $connection)
        {
            $data[] = $connection->follower;
        }
        $data = array_merge($pending,$data);
        //->where('profession_id',Auth::user()->profession_id)
        $people = self::where('users.id','!=',Auth::user()->id)
            ->whereNotIn('users.id', $data)
			/*->orwhere(function($query) use($data){
				$query->where("jobs.job_name","LIKE",Auth::user()->job_name);
					->where('users.id','!=',Auth::user()->id)
					->whereNotIn('users.id',$data);
					
			})
			->orwhere(function($query2) use($data){
				$query2->where("educationaldatas.course","LIKE",Auth::user()->course)
					->where('users.id','!=',Auth::user()->id)
					->whereNotIn('users.id',$data);
			})
			->join('jobs','jobs.user_id','=','users.id')
			->leftJoin('educationaldatas','educationaldatas.user_id','=','users.id')*/
            ->select('users.*')
			->inRandomOrder()
			->limit(5)
            ->get();
        return $people;
    }
	
    public function findPeople()
    {
        $connections = Connections::where('user_id',Auth::user()->id)
            ->select('follower')
            ->get();
        $pendingRequests = Request::where('receiver_id',Auth::user()->id)
            ->select('requester_id')
            ->get();
        $pending = [];
        $data  = [];
        foreach ($connections as $connection)
        {
            $data[] = $connection->user;
        }
        foreach ($pendingRequests as $pend)
        {
            $pending[] = $pend->requester_id;
        }
        $data = array_merge($pending,$data);
        //->where('profession_id',Auth::user()->profession_id)
        $people = self::where('country',Auth::user()->country)
            ->whereNotIn('users.id', $data)
            ->where('users.id','!=',Auth::user()->id)
            ->join('professions','users.profession_id','=','professions.id')
            ->select('users.*','professions.name')
            ->get();
        return $people;
    }
    public function hasSentRequest($receiver)
    {
        $count = Request::where('requester_id',Auth::user()->id)
            ->where('receiver_id',$receiver)
            ->count();
        if($count == 1)
        {
            return true;
        }
    }
    public function hasReceivedRequest($requester)
    {
        $count = Request::where('receiver_id',Auth::user()->id)
            ->where('requester_id',$requester)
            ->count();
        if($count == 1)
        {
            return true;
        }
    }
    public function getMessage($receiver)
    {
        $message = Messages::where('sender_id',$receiver)
            ->where('receiver_id',Auth::user()->id)
            ->orWhere(function($query) use ($receiver){
                $query->where('receiver_id',$receiver)
                    ->where('sender_id',Auth::user()->id);
            })
            ->join('users','messages.sender_id','=','users.id')
            ->select('users.*','messages.sender_id','messages.receiver_id','messages.content','messages.created_at')
            ->orderBy('messages.id','desc')
            ->first();
        return $message;
    }
    public function getFeed($offset = 1)
    {
        $connections = Connections::where('user_id',Auth::user()->id)
            ->select('follower')
            ->get();
        $data  = [];
        foreach ($connections as $connection)
        {
            $data[] = $connection->follower;
        }
        $hiddenPosts = explode(',',Auth::user()->hidden_posts);
        $data[] = Auth::user()->id;
        $take = 5;
        $count = Post::whereIn('posts.user_id',$data)
            ->whereNotIn('posts.id',$hiddenPosts)
            ->join('users','posts.user_id','=','users.id')
            ->select('users.fullnames','users.username','users.account_type','users.photo','users.status','posts.user_id','users.saved_posts','posts.content','posts.image','posts.created_at')
            ->orderBy('posts.id','desc')
            ->count();
        if($offset < 2){
            $feed = Post::whereIn('posts.user_id',$data)
                ->whereNotIn('posts.id',$hiddenPosts)
                ->join('users','posts.user_id','=','users.id')
                ->select('users.fullnames','users.username','users.account_type','users.photo','users.status','posts.id','posts.user_id','users.saved_posts','posts.content','posts.image','posts.created_at')
                ->orderBy('posts.id','desc')
                ->take($take)
                ->get();
        }
        else
        {
            $skip = ($offset * $take) - $take;
            if($skip > $count){
                return "";
            }
            if(($skip + $take) > $count){
                $take = $count - $take;
            }
            $feed = Post::whereIn('posts.user_id',$data)
                ->whereNotIn('posts.id',$hiddenPosts)
                ->join('users','posts.user_id','=','users.id')
                ->select('users.fullnames','users.username','users.account_type','users.photo','users.status','posts.id','posts.user_id','users.saved_posts','posts.content','posts.image','posts.created_at')
                ->orderBy('posts.id','desc')
                ->skip($skip)
                ->take($take)
                ->get();
        }
        $feeds = [];
        foreach ($feed as $post){
            $id = $post->id;
            $post['comments'] = Post::getComments($id);
            $post['likes'] = Post::getTenLikes($id);
            $feeds[] = $post;
        }
        return $feeds;
    }
    public function getMessages($receiver,$sender)
    {
        $message = Messages::where('sender_id',$receiver)
            ->where('receiver_id',$sender)
            ->orWhere(function($query) use ($receiver,$sender){
                $query->where('sender_id',$sender)
                    ->where('receiver_id',$receiver);
            })
            ->join('users','messages.sender_id','=','users.id')
            ->select('users.*','messages.sender_id','messages.receiver_id','messages.content','messages.created_at')
            ->orderBy('messages.id','asc')
            ->get();
        return $message;
    }
    public function getUreadMessagesCount()
    {
        $count = Messages::where('receiver_id',Auth::user()->id)->where('read','no')->count();
        return $count;
    }
    public function getUreadMessages()
    {
        $messages = Messages::where('receiver_id',Auth::user()->id)
            ->where('read','no')
            ->join('users','messages.sender_id','=','users.id')
            ->select('users.*','messages.sender_id','messages.receiver_id','messages.content','messages.created_at')
            ->get();
        return $messages;
    }
    public static function isUser($username)
    {
        $count = self::where('username',$username)->count();
        if($count == 1)
        {
            return true;
        }
    }
    public static function findByUsername($username)
    {
        $user = self::where('username',$username)
            ->select('*')
            ->first();
        return $user;
    }
    public static function getUserPosts($username)
    {
        $user = self::findByUsername($username);
        $feed = Post::where('posts.user_id',$user->id)
            ->join('users','posts.user_id','=','users.id')
            //->join('professions','users.profession_id','=','professions.id')
            ->select('users.fullnames','users.username','users.photo','users.status','posts.id','posts.content','posts.created_at')
            ->orderBy('posts.id','desc')
            ->get();
        return $feed;
    }
    public static function isOnline($id)
    {
        $count = self::where('id',$id)
            ->where('is_online','yes')
            ->count();
        if($count == 1)
        {
            return true;
        }
    }
    public static function addChatRequest($id)
    {
        $user = self::find($id);
        if($user->chats == '' || $user->chats == 'NULL')
        {
            $user->chats = Auth::user()->id;
        }
        else
        {
            $chats = explode(',',$user->chats);
            if(!in_array(Auth::user()->id,$chats))
            {
                $user->chats = $user->chats.','.Auth::user()->id;
            }
        }
        $user->save();
    }
    public static function removeChatRequests()
    {
        $user = self::find(Auth::user()->id);
        $user->chats = '';
        $user->save();

    }
    public static function getChatRequests()
    {
        $user = self::find(Auth::user()->id);
        $requesters = explode(',',$user->chats);
        $rows = self::whereIn('id',$requesters)
            ->select('id','fullnames')
            ->get();
        $boxes = [];
        foreach ($rows as $row)
        {
            $boxes[] = ['id'=>$row->id,'title'=>ucwords($row->firstname.' '.$row->lastname)];
        }
        return $boxes;
    }
    public function settings($id=0)
    {
        if($id == 0)
        {
            $id = Auth::user()->id;
        }
        $s = UserSetting::where('user_id',$id)->first();
        return UserSetting::find($s->id);
    }
    public static function getOnlineUsersCount()
    {
        $count = self::where('is_online','yes')->count();
        return $count;
    }
    public static function getActiveUsersCount()
    {
        $count = self::where('account_status','active')->count();
        return $count;
    }
    public static function getInactiveUsersCount()
    {
        $count = self::where('account_status','inactive')->count();
        return $count;
    }
    public static function getUsersCount()
    {
        $count = self::all()->count();
        return $count;
    }
    public static function getRegisteredUsersToday()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $users = User::where('account_status','active')->get();
        $interval = 0;
        foreach ($users as $user)
        {
            $d2 = new \DateTime($user->created_at);
            $interval = $d1->diff($d2);
            if($interval->days <= 1)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getRegisteredUsersThisWeek()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $users = User::where('account_status','active')->get();
        $interval = 0;
        foreach ($users as $user)
        {
            $d2 = new \DateTime($user->created_at);
            $interval = $d1->diff($d2);
            if($interval->days <= 7)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getRegisteredUsersThisMonth()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $users = User::where('account_status','active')->get();
        $interval = 0;
        foreach ($users as $user)
        {
            $d2 = new \DateTime($user->created_at);
            $interval = $d1->diff($d2);
            if($interval->days <= 30)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getRegisteredUsersThisYear()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $users = User::where('account_status','active')->get();
        $interval = 0;
        foreach ($users as $user)
        {
            $d2 = new \DateTime($user->created_at);
            $interval = $d1->diff($d2);
            if($interval->days <= 365)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getNewestUsers()
    {
        $users = self::select('photo','fullnames')
            ->limit(20)
            ->orderBy('id','desc')
            ->get();
        return $users;
    }
    public static function getSavedPostsCount()
    {
        $savedPost = Auth::user()->saved_posts?Auth::user()->saved_posts:[];
        $count = count($savedPost);
        if($count > 0)
        {
            return $count;
        }
        return "";
    }
	public static function connectionNotificationCount(){
		$count = Connections::where('user_id',Auth::user()->id)
		->where('notified','no')
		->count();
		return $count;
	}
	public static function getConnectionNotifications(){
		$connections = Connections::where('user_id',Auth::user()->id)
		->where('notified','no')
		->join('users','connections.follower','=','users.id')
		->select('users.*','connections.follower','connections.notified','connections.created_at')
		->orderBy('connections.id','desc')
		->get();
		return $connections;
	}
	public static function markAsNotified(){
		
		$notifications = Connections::where('user_id',Auth::user()->id)
		->where('notified','no')
		->get();
		
		foreach($notifications as $notify){
			$notify = Connections::find($notify->id);
			$notify->notified = 'yes';
			$notify->save();
		}
	}
	public static function getPostDetails(){
		$posts = Posts::where('id','>','0')
		->join('users','posts.user_id','=','users.id')
		->select('posts.*','users.*')
		->get();
		return $posts;
	}
}
