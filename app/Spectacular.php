<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Spectacular extends Model {

	protected $table = 'spectaculars';
    protected $fillable = ['*'];
    protected $hidden = ['id'];

}
