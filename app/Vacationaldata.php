<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacationaldata extends Model {

	//
   protected $table = 'vacationaldatas';
   protected $fillable = ['*'];
   protected $hidden = ['id'];

}
