<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class IsRegComplete {

	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth =  $auth;
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if($this->auth->check())
		{
			if($this->auth->user()->reg_status != 'completed' && $this->auth->user()->account_type!='admin')
			{
				return redirect($this->auth->user()->reg_status);
			}
		}
		return $next($request);
	}

}
