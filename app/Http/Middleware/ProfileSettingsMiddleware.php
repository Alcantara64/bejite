<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\UserSetting;
use Auth;

class ProfileSettingsMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->auth->check())
        {
            $count = UserSetting::where('user_id',Auth::user()->id)->count();
            if($count != 1)
            {
                $setting = new UserSetting();
                $setting->user_id = Auth::user()->id;
                $setting->profile_view = 'everyone';
                $setting->profile_birthday = 'everyone';
                $setting->profile_msg = 'connections';
                $setting->profile_notification = 'yes';
                $setting->profile_connect_with_me = 'yes';
                $setting->profile_post_comment = 'yes';
                $setting->profile_like = 'yes';
                $setting->profile_post_share = 'yes';
                $setting->save();
            }
        }
        return $next($request);
    }
}
