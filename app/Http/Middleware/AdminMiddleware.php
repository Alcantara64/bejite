<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next){

		if(Auth::check()){
			$id = Auth::user()->id;
			$user = User::find($id);
			if($user->account_type!='admin')
			{
				flash()->overlay('You are not Autorize to view this page!','Access Denied');
				return redirect('home');
			}
		}else{
			return redirect()->guest('auth/login');
		}
		return $next($request);
	}

}
