<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Closure;
use DB;
use App\User;
use Auth;

class UsersOnlineMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $time=time();
        $time_check = $time-600; //SET TIME 10 Minute
        DB::table('users')
            ->where('time','<',$time_check)
            ->update(['is_online'=>'no']);
        if($this->auth->check())
        {
            $user = User::find(Auth::user()->id);
            $user->is_online = 'yes';
            $user->time = time();
            $user->save();
        }
        return $next($request);
    }
}
