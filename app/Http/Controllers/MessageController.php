<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Auth;
use App\Messages;

class MessageController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('reg');
        $this->request = $request;
    }
    public function index()
    {
        return view('messages.index');
    }
    public static function setCssClass($key)
    {
        return $cls = $key == 0 ? 'selected' : '';
    }
    public function addMessage()
    {
        $this->validate($this->request,[
            'msg' => 'required|max:500',
            'receiver' => 'required|numeric',
            'sender' => 'required|numeric',
        ]);
		
		$user_message = $this->request->input('msg');
        $message = new Messages();
        $message->content = htmlentities($user_message, ENT_QUOTES, "UTF-8");
        $message->sender_id = $this->request->input('sender');
        $message->receiver_id = $this->request->input('receiver');
        $message->delivered = 'no';
        $message->read = 'no';
        if(User::isOnline($this->request->input('receiver')))
        {
           User::addChatRequest($this->request->input('receiver'));
            $message->delivered = 'yes';
            $message->read = 'yes';
        }
        $message->save();
        return response()->json(['messages'=>Auth::user()->getMessage($this->request->input('receiver')),'success'=>'ok']);
    }
    public function getMessages($receiver)
    {
        return response()->json(['messages'=>Auth::user()->getMessages($receiver,Auth::user()->id)]);
    }
    public function getConversation($receiver,$sender)
    {
        \App\Messages::markAsRead($receiver,$sender);
        return response()->json(['messages'=>Auth::user()->getMessages($receiver,$sender)]);
    }
}
