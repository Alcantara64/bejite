<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page as Pager;
use Illuminate\Http\Request;

class Page extends Controller {

    public function __construct()
    {
    }
	public function show($slug)
    {
        $page = Pager::where('slug',$slug)->first();
        return view('page.index',['page'=>$page]);
    }

}
