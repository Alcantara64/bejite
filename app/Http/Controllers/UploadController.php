<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;

class UploadController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    public function profilePhoto()
    {
        $this->validate($this->request,[
            'photo' => 'required|image|mimes:jpeg,jpg,png,gif'
        ]);
        if($this->request->hasFile('photo'))
        {
            $user = User::find(Auth::user()->id);
            $currentImage = public_path().'/users'.DIRECTORY_SEPARATOR.$user->photo;
            if(is_file($currentImage))
            {
                if($user->photo !='avatar.jpg')
                {
                    @unlink($currentImage);
                }
            }
            $file = $this->request->file('photo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('users/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/users'.DIRECTORY_SEPARATOR.$img,public_path().'/users'.DIRECTORY_SEPARATOR.$img);
            $user->photo = $img;
            $user->save();
            return response()->json(['success'=>'yes','img'=>$file->getFilename().'.'.$extension]);
        }
    }
    public function coverImage()
    {
        $this->validate($this->request,[
            'photo' => 'required|image|mimes:jpeg,jpg,png,gif'
        ]);
        if($this->request->hasFile('photo'))
        {
            $user = User::find(Auth::user()->id);
            $currentImage = public_path().'/uploads/cover'.DIRECTORY_SEPARATOR.$user->cover_photo;
            if(is_file($currentImage))
            {
                if($user->cover_photo !='cover.jpg')
                {
                    @unlink($currentImage);
                }
            }
            $file = $this->request->file('photo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('users/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/users'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/cover'.DIRECTORY_SEPARATOR.$img);
            $user->cover_photo = $img;
            $user->save();
            return response()->json(['success'=>'yes','img'=>$file->getFilename().'.'.$extension]);
        }
    }
}
