<?php

namespace App\Http\Controllers;

use App\Messages;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class ChatController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
        if(!$this->request->session()->has('chatHistory'))
        {
            $this->request->session()->put('chatHistory',[]);
        }
        if(!$this->request->session()->has('openChatBoxes'))
        {
            $this->request->session()->put('openChatBoxes',[]);
        }
        if(!$this->request->session()->has('openChatBoxesIndex'))
        {
            $this->request->session()->put('openChatBoxesIndex',[]);
        }
    }
    private function chatBoxSession($chatbox)
    {
        $item = '';
        if($this->request->session()->has('chatHistory'))
        {
            $item = $this->request->session()->get('chatHistory')[$chatbox];
        }
        return $item;
    }
    public function startSession()
    {
        return response()->json([
            'userId'=>Auth::user()->id,
            'chatboxes'=>$this->request->session()->get('openChatBoxes')
        ]);
    }
    public function closeChat()
    {
        if($this->request->has('chatbox'))
        {
            $chatbox = $this->request->input('chatbox');
            $boxes = $this->request->session()->get('openChatBoxes');
            $boxindexes = $this->request->session()->get('openChatBoxesIndex');
            $index = $boxindexes[$chatbox];
            unset($boxes[$index]);
            $this->request->session()->put('openChatBoxes',$boxes);
            unset($boxindexes[$chatbox]);
            $this->request->session()->put('openChatBoxesIndex',$boxindexes);
            return '1';
        }
    }
    public function createChatbox()
    {
        if($this->request->has('chatbox'))
        {
            $openChatboxes = $this->request->session()->get('openChatBoxes');
            $chatboxids = [];
            foreach ($openChatboxes as $box)
            {
                $chatboxids[] = $box['id'];
            }
            if(empty($chatboxids) || !in_array($this->request->input('chatbox'),$chatboxids))
            {
                $openChatboxes[] = [
                    'id'=> $this->request->input('chatbox'),
                    'title'=> ucwords($this->request->input('title'))
                ];
                $this->request->session()->put('openChatBoxes',$openChatboxes);
                $openBoxesArray = $this->request->session()->get('openChatBoxes');
                end($openBoxesArray);
                $lastIndex = key($openBoxesArray);
                $openboxindexes = $this->request->session()->get('openChatBoxesIndex');
                $openboxindexes[$this->request->input('chatbox')] = $lastIndex;
                $this->request->session()->put('openChatBoxesIndex',$openboxindexes);
            }
            return response()->json(['success'=>'yes','openbox'=>$this->request->session()->get('openChatBoxesIndex')]);
        }
    }
    public function saveMessage()
    {
        $this->validate($this->request,[
            'sender' => 'required',
            'receiver' => 'required',
            'msg' => 'required'
        ]);

        $message = new Messages();
        $message->sender_id = $this->request->input('sender');
        $message->receiver_id = $this->request->input('receiver');
    }
    public function chatRequests()
    {
        return response()->json(['chat_requests' => User::getChatRequests()]);
    }
    public function addChatRequest($id)
    {
        User::addChatRequest($id);
    }
    public function removeChatRequests()
    {
        User::removeChatRequests();
    }
}