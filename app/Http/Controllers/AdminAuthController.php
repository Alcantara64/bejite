<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class AdminAuthController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->request = $request;
    }
    public function getLogin()
    {
        return view('admin.auth.login');
    }
    public function postLogin()
    {
        $this->validate($this->request,[
            'email' => 'required|max:255|email',
            'password' => 'required|max:15'
        ]);
        $login = [
            'email' => $this->request->input('email'),
            'password' => $this->request->input('password'),
            'account_type' => 'admin',
            'status' => 'active'
        ];
        if(Auth::attempt($login))
        {
            if($this->request->ajax() || $this->request->wantsJson())
            {
                return response()->json(['status' => 'success']);
            }
            else
            {
                return redirect()->intended('admin');
            }
        }
        else
        {
            return response()->json(['status' => 'fail', 'feedback' => 'Your email or password is incorrect']);
        }
    }
}
