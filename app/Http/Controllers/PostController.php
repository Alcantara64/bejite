<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Comments;
use App\Like;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PostController extends Controller {
    //request object
	protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    public function savePost()
    {
        $photos = '';
        $images = '';
        if($this->request->hasFile("photos"))
        {
            $files = $this->request->file('photos');
            //$file_count = count($files);
            $upload_count = 0;
            foreach ($files as $file)
            {
                if($upload_count == 5)
                {
                    break;
                }
                $extension = $file->getClientOriginalExtension();
                Storage::disk('local')->put('posts/'.$file->getFilename().'.'.$extension, File::get($file));
                $img = $file->getFilename().'.'.$extension;
                if($upload_count == 0)
                {
                    $images = $img;
                }
                else
                {
                    $images .=','.$img;
                }
                @rename(storage_path().'/app/posts/'.$img,public_path().'/posts/images/'.$img);
                //$photos .='<img class="post-img-preview" src="/uploads/posts/images/'.$img.'">';
                $upload_count++;
            }
        }
		
		$user_post = $this->request->input('post');
        
		$post = new Post();
        $post->user_id = Auth::user()->id;
        $post->content = nl2br(htmlentities($user_post, ENT_QUOTES, "UTF-8"));
        $post->image = $images;
        $post->save();
        //return response()->json(['success'=>'yes','file'=>'<img class="post-img-preview" src="/uploads/posts/temp/'.$file->getFilename().'.'.$extension.'">']);
        return response()->json(['status'=>'ok','post'=>Post::getPost($post->id)]);
    }
    public function updatePost()
    {
        $this->validate($this->request,[
            'id' => 'required|numeric',
            'content' => 'required'
        ]);
        $id = $this->request->input('id');
        $post = Post::find($id);
        $post->content = $this->request->input('content');
        $post->save();
        return response()->json(['status'=>'ok','post'=>Post::getPost($post->id)]);
    }
    public function saveComment()
    {
        $this->validate($this->request,[
            'post_id' => 'required|numeric',
            'user_id' => 'required|numeric', 
            'content' => 'required'
        ]);
		
		$user_comment = $this->request->input('content'); 
        $comment = new Comments();
        $comment->post_id = $this->request->input('post_id');
        $comment->user_id = $this->request->input('user_id');
        $comment->content = htmlentities($user_comment, ENT_QUOTES, "UTF-8");
        $comment->save();
        return response()->json(['success'=>'ok','comment'=>Post::getComment($comment->id)]);
    }
    public function getComments($id)
    {
        return ['comments'=>Post::getComments($id)];
    }
    public function TagPeople()
    {
        $connections = Connection::where('user_id',Auth::user()->id)
            ->select('user')
            ->get();
        $data  = [];
        foreach ($connections as $connection)
        {
            $data[] = $connection->user;
        }
        $result = '';
        if($this->request->has('keyword'))
        {
            $keyword = $this->request->input('keyword');
            $result = User::where("firstname","LIKE","%$keyword%")
                ->orWhere(function($query) use ($keyword){
                    $query->orWhere("lastname","LIKE","%$keyword%")
                        ->orWhere("email","LIKE","%$keyword%")
                        ->orWhere("phone","LIKE","%$keyword%")
                        ->orWhere("city","LIKE","%$keyword%")
                        ->orWhere("state","LIKE","%$keyword%")
                        ->orWhere("country","LIKE","%$keyword%");
                })
                ->whereIn('id',$data)
                ->get();
        }
        return response()->json(['result'=>$result]);
    }
    public function like()
    {
        $this->validate($this->request,[
            'post_id' => 'required|numeric',
            'type' => 'required'
        ]);
        if(Like::hasLiked($this->request->input('post_id')))
        {
            return response()->json(['status'=>'failed']);
        }
        $like = new Like();
        $like->user_id = Auth::user()->id;
        $like->post_id = $this->request->input('post_id');
        $like->type = $this->request->input('type');
        $like->save();
        return response()->json(['status'=>'ok','user'=>[
            'fullnames' => ucfirst(Auth::user()->fullnames),
            'username' => Auth::user()->username,
            'photo' => Auth::user()->photo,
            'type' => $this->request->input('type')
        ]]);
    }
    public function share()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            $count = Post::where('user_id',Auth::user()->id)->where('id',$id)->count();
            if($count == 1)
            {
                return response()->json(['status'=>'failed']);
            }
            $post = Post::find($id);
            $share = new Post();
            $share->user_id = Auth::user()->id;
            $share->content = $post->content;
            $share->image = $post->image;
            $share->shared_from = $post->user_id;
            $share->save();
            return response()->json(['status'=>'ok','post'=>Post::getPost($share->id)]);
        }
    }
    public function hide()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            $hiddenPosts = Auth::user()->hidden_posts;
            if(!in_array($id,explode(',',$hiddenPosts)))
            {
                if($hiddenPosts == '' || $hiddenPosts == null){
                    $hiddenPosts = $id;
                }
                else
                {
                    $hiddenPosts = rtrim($hiddenPosts,',').','.$id;
                }
                $user = User::find(Auth::user()->id);
                $user->hidden_posts = $hiddenPosts;
                $user->save();
                return response()->json(['status'=>'ok']);
            }
        }
    }
    public function save()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            $savedPosts = Auth::user()->saved_posts;
            if(!in_array($id,explode(',',$savedPosts)))
            {
                if($savedPosts == '' || $savedPosts == null){
                    $savedPosts = $id;
                }
                else
                {
                    $savedPosts = rtrim($savedPosts,',').','.$id;
                }
                $user = User::find(Auth::user()->id);
                $user->saved_posts = $savedPosts;
                $user->save();
                return response()->json(['status'=>'ok']);
            }
        }
    }
    public function unsave()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            $savedPosts = explode(',',Auth::user()->saved_posts);
            if(in_array($id,$savedPosts))
            {
                if(($key = array_search($id,$savedPosts)) !== false)
                {
                    unset($savedPosts[$key]);
                }
                $user = User::find(Auth::user()->id);
                $user->saved_posts = rtrim(implode(',',$savedPosts),',');
                $user->save();
                return response()->json(['status'=>'ok']);
            }
        }
    }
    public function edit()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            if(Post::isOwner($id))
            {
                return response()->json(['status'=>'ok','post'=>Post::getPost($id)]);
            }
        }
    }
    public function delete()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            if(Post::isOwner($id))
            {
                $post = Post::find($id);
                if($post->image !='' || $post->image != null)
                {
                    $images = explode(',',$post->image);
                    foreach ($images as $img)
                    {
                        if(is_file(public_path().'/uploads/posts/images/'.$img))
                        {
                            @unlink(public_path().'/uploads/posts/images/'.$img);
                        }
                    }
                }
                Comment::deleteComments($id);
                $post->delete();
                return response()->json(['status'=>'ok']);
            }
        }
    }
    public function report()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            $count = ReportedPost::where('post_id',$id)->where('user_id',Auth::user()->id)->count();
            if($count > 0)
            {
                return response()->json(['feedback'=>'This post has already been reported by you.']);
            }
            $report = new ReportedPost();
            $report->post_id = $id;
            $report->user_id = Auth::user()->id;
            $report->reason = $this->request->input('reason');
            $report->save();
            return response()->json(['status'=>'ok']);
        }
    }
    public function getLikes()
    {
        if($this->request->has('id'))
        {
            $id = $this->request->input('id');
            return response()->json(['likes'=>Post::getLikes($id)]);
        }
    }
}
