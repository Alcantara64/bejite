<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class UserAuthController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('guest', ['except' => 'logOut']);
        $this->request = $request;
    }
    public function join()
    {
        $this->validate($this->request,[
		//	'g-recaptcha-response' => 'required|captcha',
            'fullnames' => 'required|max:255',
            //'username' => 'required|max:250|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            //'city' => 'required|max:255',
            //'state' => 'required|max:255',
            //'country' => 'required|max:255',
            //'phone' => 'required|max:15',
            'gender' => 'required|max:6',
        ]);
		
		//spam control
		//$bot_input = $this->request->input('usermail');
		//$check_fullnames = $this->request->input('fullnames');
		//if (strlen($bot_input) > 0 || preg_match('#[0-9]#', $check_fullnames)){
			//return response()->json(['success'=>'no']);
		//}
		//else{
        $user = new User();
        $user->fullnames = $this->request->input('fullnames');
        //$user->username = $this->request->input('username');
        $user->email = $this->request->input('email');
        $user->password = bcrypt($this->request->input('password'));
        //$user->city = $this->request->input('city');
        //$user->state = $this->request->input('state');
        //$user->country = $this->request->input('country');
        //$user->phone = $this->request->input('phone');
        $user->gender = $this->request->input('gender');
        $user->reg_status = '/account/setup';
        $user->photo = 'avatar.jpg';
		$user->ip_address = $this->request->ip();
        //$user->verification_code = bcrypt($this->request->input('email'));
        $user->save();
        $usr = User::find($user->id);
        Mail::send('emails.welcome',['user'=>$usr],function($m) use ($usr){
            $m->from('noreply@bejite.com','Bejite');
            $m->to($usr->email,$usr->firstname)->subject("Welcome to Bejite");
        });
        $credentials = ['email'=>$this->request->input('email'),'password'=>$this->request->input('password')];
        if(Auth::attempt($credentials))
        {
            return response()->json(['success'=>'yes']);
        }
   // }
	}
    public function signIn()
    {
        if(Auth::check())
        {
            return redirect('/');
        }
        $this->validate($this->request,[
            'email' => 'required|max:255|email',
            'password' => 'required|max:255'
        ]);
        $login = [
            'email' => $this->request->input('email'),
            'password' => $this->request->input('password'),
        ];
        if(Auth::attempt($login))
        {
            if($this->request->ajax() || $this->request->wantsJson())
            {
                return response()->json(['status' => 'success']);
            }
            else
            {
                return redirect()->intended('/home');
            }
        }
        else
        {
            return response()->json(['status' => 'fail', 'feedback' => 'Your email or password is incorrect']);
        }
    }
    public function logOut()
    {
        $user = User::find(Auth::user()->id);
        $user->is_online = 'no';
        $user->save();
        Auth::logout();
        return redirect('/');
    }
    public function showPasswordRecovery()
    {
        return view('auth.passwords.email');
    }
    public function resetPassword()
    {
        $this->validate($this->request,[
            'email' => 'required|email'
        ]);
        $email =  $this->request->input('email');
        $count = User::where('email',$email)->count();
        if($count != 1)
        {
            return response()->json(['status'=>'failed','feedback'=>'This email address does not exists in our system']);
        }
        $usr = User::where('email',$email)->first();
        $user = User::find($usr->id);
        $pass = User::generateRandomString(10);
        $user->password = bcrypt($pass);
        $user->save();
        Mail::raw('Your new password is : '.$pass.' . We encourage you to change your password as soon as you are able to login',function($m) use ($usr){
            $m->from(Option::get('notification_email'),Option::get('site-name'));
            $m->to($usr->email,$usr->firstname)->subject(Option::get('site-name')." - Password Reset");
        });
        return response()->json(['status'=>'ok']);
    }
}
