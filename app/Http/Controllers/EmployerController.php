<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Employers;
use Auth;
use App\Biodata;
use App\Educationaldata;
use App\Secondryschool;
use App\Vacationaldata;
use App\Professionaldata;
use App\Workdata;
use App\Job;
use App\Skills;
use App\Journal;
use App\Nonprofit;
use App\Spectacular;
use App\Hobby;
use App\Workhabit;
use App\Referee;
use App\User;

class EmployerController extends Controller {

    protected $request;

	public function __construct(Request $request)
    {
        $this->middleware('auth');
        //$this->middleware('employer');
        $this->request = $request;
    }
    public function showUpdateData()
    {
        $employer = Employers::where('user_id',Auth::user()->id)->first();
        $logo = '/img/company_logo.png';
        if(is_file(storage_path('app/logos/'.$employer->logo)))
        {
            $logo = '/images/logo-image/'.$employer->logo;
        }
        $data = ['logo'=>$logo,'employer'=>$employer];
        return view('employer.update-data',$data);
    }
    public function saveData()
    {
        $user = Employers::where('user_id',Auth::user()->id)->first();
        $this->validate($this->request,[
                            'account_type' => 'required',
                            'address1' => 'required',
                            'support_email' => 'required',
                            'position' => 'required',
                        ]);
        $employer = Employers::find($user->id);
        $employer->company = $this->request->input('company');
        $employer->address1 = $this->request->input('address1');
        $employer->address2 = $this->request->input('address2');
        $employer->type = $this->request->input('account_type');
        $employer->website = $this->request->input('website');
        $employer->support_email = $this->request->input('support_email');
        $employer->position = $this->request->input('position');
        if($this->request->hasFile('logo')){
            $file = $this->request->file('logo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('logos/'.$file->getFilename().'.'.$extension, File::get($file));
            $employer->logo = $file->getFilename().'.'.$extension;
        }
        $employer->save();
        flash()->overlay('Your company information was updated successfully','Updated Successfully');
        return redirect()->back();
    }
    public function jobseeker($id)
    {
        if(!is_numeric($id))
        {
            return redirect()->back();
        }
        $user = User::find($id);
        $u = new User;
        //$photo = $u->getUserPhoto($user->id,'profile-avatar');
        $biodata = Biodata::where('user_id',$user->id)->first();
            $edu = Educationaldata::where('user_id',$user->id)->get();
            $sec = Secondryschool::where('user_id',$user->id)->get();
            $profession = Professionaldata::where('user_id',$user->id)->get();
            $vacation = Vacationaldata::where('user_id',$user->id)->first();
            $work = Workdata::where('user_id',$user->id)->get();
            $skills = Skills::where('user_id',$user->id)->get();
            $job = Job::where('user_id',$user->id)->first();
            $journals = Journal::where('user_id',$user->id)->get();
            $nonprofits = Nonprofit::where('user_id',$user->id)->get();
            $spectacular = Spectacular::where('user_id',$user->id)->first();
            $hobbies = Hobby::where('user_id',$user->id)->get();
            $referees = Referee::where('user_id',$user->id)->get();
            $workhabit = Workhabit::where('user_id',$user->id)->first();
            $data = [
                        'user'=>$biodata,
                        'biodata'=>$biodata,
                        'educational'=>$edu,
                        'secondary'=>$sec,
                        'profession' => $profession,
                        'vacation' => $vacation,
                        'work' => $work,
                        'skills' => $skills,
                        'job'=> $job,
                        'journals' => $journals,
                        'nonprofits' => $nonprofits,
                        'spectacular' => $spectacular,
                        'hobbies' => $hobbies,
                        'referees' => $referees,
                        'wkh' => $workhabit,
                        //'photo'=> $photo,
                        'edui'=> 1,
                        'seci'=>1,
                        'profi'=>1,
                        'wki' => 1,
                        'skilli' => 1,
                        'ji' => 1,
                        'noni' => 1,
                        'hobbyi' => 1,
                        'ri' => 1,
                        ];
        return view('jobseeker.details',$data);
    }

}
