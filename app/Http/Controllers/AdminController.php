<?php

namespace App\Http\Controllers;

use App\Page;
use App\Slider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use App\Option;
use Auth;

class AdminController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->request = $request;
    }
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function showSiteSettings()
    {
        return view('admin.site-settings');
    }
    public function saveSiteSettings()
    {
        $counter = 0;
        $img = '';
        if($this->request->hasFile('logo'))
        {
            $currentLogo = public_path().'/uploads/logo/'.Option::get('logo');
            if(is_file($currentLogo))
            {
                @unlink($currentLogo);
            }
            $file = $this->request->file('logo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/logo/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/logo'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/logo'.DIRECTORY_SEPARATOR.$img);
            Option::set('logo',$img);
            $counter += 1;
        }
        if($this->request->has('option'))
        {
            $options = $this->request->input('option');
            foreach ($options as $key => $value) {
                Option::set($key,$value);
                $counter++;
            }
            return response()->json(['status'=>'success','feedback' => $counter.' Settings were updated successfully','img'=>$img]);
        }
    }
    public function showPages()
    {
        $pages = Page::all();
        $menus = Page::where('parent_menu',0)->where('status','published')->get();
        return view('admin.pages.index',['pages'=>$pages,'menus'=>$menus]);
    }
    public function savePage()
    {
        $this->validate($this->request,[
            'title' => 'required|max:255',
            'name' => 'required|max:100',
            'page_order' => 'required|numeric',
            'status' => 'required',
            'parent'=> 'required|numeric',
            'position' => 'required|max:20',
            'content' => 'required'
        ]);
        $page = new Page();
        $page->title = $this->request->input('title');
        $page->slug = str_replace(' ','-',strtolower($this->request->input('title')));
        $page->name = $this->request->input('name');
        $page->page_order = $this->request->input('page_order');
        $page->status = $this->request->input('status');
        $page->parent_menu = $this->request->input('parent');
        $page->position = $this->request->input('position');
        $page->content = $this->request->input('content');
        $page->save();
        return response()->json(['feedback'=>'Page Created Successfully','status' => 'success']);
    }
    public function updatePage($id)
    {
        $this->validate($this->request,[
            'title' => 'required|max:255',
            'name' => 'required|max:100',
            'page_order' => 'required|numeric',
            'status' => 'required',
            'position' => 'required|max:20',
            'content' => 'required'
        ]);
        $page = Page::find($id);
        $page->title = $this->request->input('title');
        $page->name = $this->request->input('name');
        $page->slug = str_replace(' ','-',strtolower($this->request->input('title')));
        $page->page_order = $this->request->input('page_order');
        $page->status = $this->request->input('status');
        $page->parent_menu = $this->request->input('parent');
        $page->position = $this->request->input('position');
        $page->content = $this->request->input('content');
        $page->save();
        return response()->json(['feedback'=>'Page Updated Successfully','status' => 'success']);
    }
    public function showUpdatePage($id)
    {
        return view('admin.pages.edit',[
            'page'=>Page::find($id),
            'menus'=>Page::where('parent_menu',0)->where('status','published')->get(),
            'pages' => Page::all()
        ]);
    }
    public function deletePage($id)
    {
        $page = Page::where('id',$id);
        $page->delete();
    }
    public function ajaxListPages()
    {
        return response()->json(['pages'=>Page::all()]);
    }
    public function showSliders()
    {
        return view('admin.sliders.index',['carousels'=>Slider::all()]);
    }
    public function saveSlider()
    {
        $this->validate($this->request,[
            'img' => 'required|image|mimes:jpg,jpeg,gif,png',
            'heading' => 'required|max:255',
            'text' => 'required',
            'order' => 'required|numeric',
            'status' => 'required'
        ]);
        $slider = new Slider();
        $slider->heading = $this->request->input('heading');
        $slider->text = $this->request->input('text');
        $slider->content = $this->request->input('content');
        $slider->display_order = $this->request->input('order');
        $slider->status = $this->request->input('status');
        if($this->request->hasFile('img'))
        {
            $file = $this->request->file('img');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/sliders/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/sliders'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/sliders'.DIRECTORY_SEPARATOR.$img);
            $slider->img = $img;
        }
        $slider->save();
        return response()->json(['feedback'=>'Slider Added Successfully','status' => 'success']);
    }
    public function showUpdateSlider($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.edit',['slider'=>$slider,'carousels'=>Slider::all()]);
    }
    public function updateSlider($id)
    {
        $this->validate($this->request,[
            'heading' => 'required|max:255',
            'text' => 'required',
            'order' => 'required|numeric',
            'status' => 'required'
        ]);
        $slider = Slider::find($id);
        $slider->heading = $this->request->input('heading');
        $slider->text = $this->request->input('text');
        $slider->content = $this->request->input('content');
        $slider->display_order = $this->request->input('order');
        $slider->status = $this->request->input('status');
        $currentImage = $slider->img;
        if($this->request->hasFile('img'))
        {
            $currentImg = public_path().'/uploads/sliders/'.$currentImage;
            if(is_file($currentImg))
            {
                @unlink($currentImg);
            }
            $file = $this->request->file('img');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/sliders/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/sliders'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/sliders'.DIRECTORY_SEPARATOR.$img);
            $slider->img = $img;
        }
        $slider->save();
        return response()->json(['feedback'=>'Slider Added Successfully','status' => 'success']);
    }
    public function ajaxDisplaySliders()
    {
        return response()->json(['sliders'=>Slider::all()]);
    }
    public function deleteSlider($id)
    {
        $slider = Slider::find($id);
        $currentImg = public_path().'/uploads/sliders/'.$slider->img;
        if(is_file($currentImg))
        {
            @unlink($currentImg);
        }
        $slider->delete();
    }
    public function showUsers()
    {
        return view('admin.users.index',['users'=>User::all()]);
    }
    public function showProfileSettings()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.users.settings',['user'=>$user]);
    }
    public function saveProfileSettings($id)
    {
        $this->validate($this->request,[
            'fullnames'=> 'required|max:120',
            'email'=> 'required|max:150|email',
            'city'=> 'required|max:255',
            'state'=> 'required|max:255',
            'country'=> 'required|max:255',
            'phone'=> 'required|max:15',
            'address'=> 'required|max:255',
        ]);

        $user = User::find($id);
        $user->fullnames = $this->request->input('fullnames');
        $user->email = $this->request->input('email');
        $user->city = $this->request->input('city');
        $user->state = $this->request->input('state');
        $user->country = $this->request->input('country');
        $user->phone = $this->request->input('phone');
        $user->address = $this->request->input('address');
        $user->save();
        return response()->json(['status'=>'success']);
    }
    public function changeProfilePhoto($id)
    {
        $this->validate($this->request,[
            'photo' => 'required|image|max:5000'
        ]);
        if($this->request->hasFile('photo'))
        {
            $user = User::find($id);
            $img = $user->photo;
            $currentImg = public_path().'/uploads/users/'.$img;
            if(is_file($currentImg))
            {
                if($user->photo != 'avatar.jpg')
                {
                    @unlink($currentImg);
                }
            }
            $file = $this->request->file('photo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/users/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/users'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/users'.DIRECTORY_SEPARATOR.$img);
            $user->photo = $img;
            $user->save();
            return response()->json(['status' => 'success','img'=>$img]);
        }
    }
    public function showChangePassword()
    {
        return view('admin.users.password');
    }
    public function changePassword()
    {
        $this->validate($this->request,[
            'pass' => 'required|min:6',
            'password' => 'required|min:6|confirmed',
        ]);
        $count = User::where('id',Auth::user()->id)
            ->count();
        if($count == 1)
        {
            $user = User::find(Auth::user()->id);
            if(Hash::check($this->request->input('pass'),$user->password))
            {
                $user->password = bcrypt($this->request->input('password'));
                $user->save();
                return response()->json(['status'=>'success']);
            }
        }

    }
    public function logout()
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
