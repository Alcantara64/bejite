<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Response;

class ImagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        
    }
	public function getIndex()
	{
	}
	public function getProfilePhoto($file)
    {
        $file = "/img/{$file}";
         return $this->imageUrl($file);
    }
    public function getLogoImage($file)
    {
        $file = "app/logos/{$file}";
         return $this->image($file);
    }
    public function imageUrl($file)
    {
        return asset($file);
    }
    private function image($file)
    {
    	$path = storage_path() . '/' . $file;
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
}
