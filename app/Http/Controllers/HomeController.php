<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use App\Employers;
use Auth;
use App\Biodata;
use App\Educationaldata;
use App\Secondryschool;
use App\Vacationaldata;
use App\Professionaldata;
use App\Workdata;
use App\Job;
use App\Skills;
use App\Journal;
use App\Nonprofit;
use App\Spectacular;
use App\Hobby;
use App\Workhabit;
use App\Referee;
use App\Connections;
use App\User;
use App\Posts;
use App\Comments;

class HomeController extends Controller
{
    protected $request;

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    public function savePostImage()
    {
        $images = Posts::where('last_id',Auth::user()->id)->get();
        foreach ($images as $img) {
            $file = base_path('public/posts/photos/'.$img->image);
            @unlink($file);
        }
        DB::table('posts')->where('last_id',Auth::user()->id)->delete();
        $this->validate($this->request,[
            'photo' => 'image|max:5000'
        ]);
        if(!$this->request->hasFile('photo'))
        {
            throw new Exception("photo image is required", 1);
        }
        $post = new Posts;
        $file = $this->request->file('photo');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put('posts/photos/'.$file->getFilename().'.'.$extension, File::get($file));
        $post->image = $file->getFilename().'.'.$extension;
        $photo = $file->getFilename().'.'.$extension;
        rename(storage_path().'/app/posts/photos/'.$file->getFilename().'.'.$extension,base_path('public/posts/photos/'.$file->getFilename().'.'.$extension));
        $post->user_id = Auth::user()->id;
        $post->last_id = Auth::user()->id;
        $post->save();
        return '/posts/photos/'.$file->getFilename().'.'.$extension;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $connections = Connections::where('user_id',Auth::user()->id)
            ->orWhere('follower',Auth::user()->id)
            ->join('users',function($join){
                $join->on('connections.user_id','=','users.id');

            })
            ->get();
        $posts = Posts::join('users','posts.user_id','=','users.id')
            ->select('posts.*','users.fullnames','users.account_type','users.photo')
            ->orderBy('posts.id','desc')->get();
        $comments = new Comments;
        //return $posts;
        return view('account.newsfeed',['connections' => $connections,'posts' => $posts,'comments'=>$comments]);
    }
    public function profile()
    {
        if(Auth::user()->account_type =='jobseeker')
        {
            $biodata = Biodata::where('user_id',Auth::user()->id)->first();
            $edu = Educationaldata::where('user_id',Auth::user()->id)->get();
            $sec = Secondryschool::where('user_id',Auth::user()->id)->get();
            $profession = Professionaldata::where('user_id',Auth::user()->id)->get();
            $vacation = Vacationaldata::where('user_id',Auth::user()->id)->first();
            $work = Workdata::where('user_id',Auth::user()->id)->get();
            $skills = Skills::where('user_id',Auth::user()->id)->get();
            $job = Job::where('user_id',Auth::user()->id)->first();
            $journals = Journal::where('user_id',Auth::user()->id)->get();
            $nonprofits = Nonprofit::where('user_id',Auth::user()->id)->get();
            $spectacular = Spectacular::where('user_id',Auth::user()->id)->first();
            $hobbies = Hobby::where('user_id',Auth::user()->id)->get();
            $referees = Referee::where('user_id',Auth::user()->id)->get();
            $workhabit = Workhabit::where('user_id',Auth::user()->id)->first();

            $percent = 5;
            if($biodata->lga !='' && $biodata->race!='')
            {
                $percent += 10;
            }
            if(count($edu))
            {
                $percent += 5;
            }
            if(count($sec))
            {
                $percent += 5;
            }
            if($vacation->qualification!='' && $vacation->year!='')
            {
                $percent +=5;
            }
            if(count($work))
            {
                $percent += 5;
            }
            if(count($profession))
            {
                $percent += 5;
            }
            if(count($skills))
            {
                $percent += 5;
            }
            if($job->job_city !='' && $job->job_town !='')
            {
                $percent += 5;
            }
            if($workhabit->convicted !='' && $workhabit->availability!='')
            {
                $percent += 20;
            }
            if(count($hobbies))
            {
                $percent += 10;
            }
            if(count($referees))
            {
                $percent += 10;
            }
            if($spectacular->name !='')
            {
                $percent += 10;
            }
            $data = [
                'biodata'=>$biodata,
                'educational'=>$edu,
                'secondary'=>$sec,
                'profession' => $profession,
                'vacation' => $vacation,
                'work' => $work,
                'skills' => $skills,
                'job'=> $job,
                'journals' => $journals,
                'nonprofits' => $nonprofits,
                'spectacular' => $spectacular,
                'hobbies' => $hobbies,
                'referees' => $referees,
                'wkh' => $workhabit,
                'edui'=> 1,
                'seci'=>1,
                'profi'=>1,
                'wki' => 1,
                'skilli' => 1,
                'ji' => 1,
                'noni' => 1,
                'hobbyi' => 1,
                'ri' => 1,
                'progress'=>$percent,
            ];
            return view('jobseeker/home',$data);
        }
        else if(Auth::user()->account_type =='employer')
        {
            $employer = Employers::where('user_id',Auth::user()->id)->first();
            $data = ['employer'=>$employer];
            return view('home',$data);
        }
        else
        {
            return redirect('admin');
        }
    }
}
