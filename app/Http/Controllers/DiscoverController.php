<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Connections;

class DiscoverController extends Controller {

    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
	public function index()
    {
        $result = User::where('id','!=',Auth::user()->id)
                        ->where('country',Auth::user()->country)
                        ->get();
        return view('discover.index',['users'=>$result]);
    }
    public function follow()
    {
        $this->validate($this->request,[
                        'id' => 'required'
                        ]);
        $id = $this->request->input('id');
        $user = new User;
        if($user->isFollowing($id))
        {
            return "";
        }
        $con =  new Connections;
        $con->user_id = Auth::user()->id;
        $con->follower = $id;
        $con->save();
        $con2 = new Connections;
        $con2->user_id = $id;
        $con2->follower = Auth::user()->id;
        $con2->save();
        $count = DB::table('connections')
                        ->where('user_id',Auth::user()->id)
                        ->orWhere('follower',$id)
                        ->count();
        return response()->json(['status'=>'ok','count'=>$count]);
    }
    function finish()
    {
        $user = User::find(Auth::user()->id);
        $user->reg_status = 'completed';
        $user->save();
        return redirect('/');
    }
}
