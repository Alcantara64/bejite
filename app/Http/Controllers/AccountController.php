<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Employers;
use App\User;
use App\Job;
use App\Biodata;
use App\Vacationaldata;
use Auth;
use App\Spectacular;
use App\Workhabit;
use DB;

class AccountController extends Controller {

    protected $request;

	public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    public function setup()
    {
        if(Auth::user()->reg_status == 'completed')
        {
            return redirect('home');
        }
        return view('account.setup');
    }
    public function saveSetup()
    {
        $this->validate($this->request,[
                            'account_type' => 'required',
                            'address' => 'required|min:6',
                            'state' => 'required|min:3',
                            'city' => 'required|min:3',
                            'phone' => 'required|min:11|numeric',
                        ]);
        $reg_status = '/account/upload-photo';
        $user = Auth::user()->find(Auth::user()->id);
        $user->account_type = $this->request->input('account_type');
        $user->address = $this->request->input('address');
        $user->country = $this->request->input('country');
        $user->state = $this->request->input('state');
        $user->city = $this->request->input('city');
        $user->phone = $this->request->input('phone');
        $user->hear_about_us = $this->request->input('hear_about_us');

        if($this->request->input('account_type') == 'employer')
        {
            $reg_status = '/account/employer';
        }
        $user->reg_status = $reg_status;
        $user->save();
        if($this->request->input('account_type') == 'jobseeker')
        {
            $vac = new Vacationaldata;
            $vac->user_id = Auth::user()->id;
            $vac->save();
            $bio = new Biodata;
            $bio->user_id = Auth::user()->id;
            $bio->save();
            $job = new Job;
            $job->user_id = Auth::user()->id;
            $job->save();
            $spectacular = new Spectacular;
            $spectacular->user_id = Auth::user()->id;
            $spectacular->save();
            $habit = new Workhabit;
            $habit->user_id = Auth::user()->id;
            $habit->save();
        }
        return response()->json(['success'=>'yes','redirect'=>$reg_status]);
    }
    public function saveStepTwo()
    {
        $this->validate($this->request,[
            'username'=> 'required|max:255|unique:users',
        ]);
		
		//Checks if the logged in user is using the username already
		/*$user_name = Auth::user()->username;
		if($user_name = $this->request->input('username')){
			return response()->json(['success'=>'yes']);
		}*/
     

	 $count = User::where('username',$this->request->input('username'))->count();
        if($count > 0)
        {
            return response()->json(['success'=>'no']);
        }
        $user = User::find(Auth::user()->id);
        $user->username = $this->request->input('username');
        $user->save();
        return response()->json(['success'=>'yes']);
    }
    public function photoUpload()
    {
        if(Auth::user()->reg_status == 'completed')
        {
            return redirect('home');
        }
        return view('account.upload-photo');
    }
    public function photoContinue()
    {
        $user = User::find(Auth::user()->id);
        $user->reg_status = '/discover';
        $user->save();
        return redirect('/discover');
    }
    public function savePhotoUpload()
    {
        if(!$this->request->hasFile('photo'))
        {
            throw new Exception("photo image is required", 1);
        }
        $user = User::find(Auth::user()->id);
        $photo = $user->photo;
        $file = $this->request->file('photo');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put('users/'.$file->getFilename().'.'.$extension, File::get($file));
        $user->photo = $file->getFilename().'.'.$extension;
        rename(storage_path().'/app/users/'.$file->getFilename().'.'.$extension,base_path('public/users/'.$file->getFilename().'.'.$extension));
        if(is_file(base_path('public/users/'.$photo)))
        {
            @unlink(base_path('public/users/'.$photo));
        }
        $user->save();
        return '/users/'.$file->getFilename().'.'.$extension;
    }
    public function employer()
    {
        if(Auth::user()->account_type =='jobseeker')
        {
            return redirect('home');
        }
        return view('account.employer');
    }
    public function saveEmployerLogo()
    {
        $this->validate($this->request,[
            'logo' => 'image|required|mimes:jpg,jpeg,png,gif|max:5000',
        ]);
        $count  =  Employers::where('user_id',Auth::user()->id)->count();
        if($count != 1){
            $employer = new Employers();
        }
        else
        {
            $e = Employers::where('user_id',Auth::user()->id)->first();
            $employer = Employers::find($e->id);
        }
        if($this->request->hasFile('logo')){
            $file = $this->request->file('logo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('logos/'.$file->getFilename().'.'.$extension, File::get($file));
            $employer->logo = $file->getFilename().'.'.$extension;
        }
        $employer->save();
        return response()-json(['status'=>'ok','logo'=>$file->getFilename().'.'.$extension]);
    }
    public function saveEmployer()
    {
        $this->validate($this->request,[
                            'account_type' => 'required',
                            'address1' => 'required',
                            'support_email' => 'required',
                            'position' => 'required',
                        ]);
        $employer = new Employers;
        $employer->user_id = Auth::user()->id;
        $employer->company = $this->request->input('company');
        $employer->address1 = $this->request->input('address1');
        $employer->address2 = $this->request->input('address2');
        $employer->type = $this->request->input('account_type');
        $employer->website = $this->request->input('website');
        $employer->support_email = $this->request->input('support_email');
        $employer->position = $this->request->input('position');
        $user = User::find(Auth::user()->id);
        $user->reg_status = '/account/upload-photo';
        $user->save();
        $employer->save();
        //return response()->json(['status'=>'ok']);
        return redirect('/');
    }

}
