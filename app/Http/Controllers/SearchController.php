<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\University;
use Illuminate\Http\Request;
use DB;

class SearchController extends Controller {

	protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        //$this->middleware('employer');
        $this->request = $request;
    }
    public function index(Request $request)
    {
        $result = '';
        if($request->has('keyword'))
        {
            $keyword = $request->input('keyword');
            $result = User::where("fullnames","LIKE","%$keyword%")
                ->get();
        }
        return response()->json(['result'=>$result]);
    }
    public function home()
    {
        return view('search.index');
    }
		public function history()
		{

				return view('user.history');
		}
        public function search()
		{


				return view('search.index');
		}
		public function store()
		{
				return view('page.store');
		}
		public function interviewInvites(){
				return view('employer.interview-invite');
		}
			public function review(){
	return view('page.review');
			}
			public function cv(){

	return view('jobseeker.cv');
			}
			public function recommendation(){
	return view('page.recommendation');
			}
			public function adpro(){
	return view('page.adpro');
			}
			public function findJobseekers()
			{
					/*$this->validate($this->request,[
															'country' => 'required',
															'state' => 'required',
															'lga' => 'required',
															'city' => 'required',
															'town' => 'required',
															'position' => 'required',
															'age' => 'required',
															'gender' => 'required',
															'marital_status' => 'required',
															'salary' => 'required',
													]);*/
					$country = $this->request->input('country');
					$country2 = $this->request->input('country2');
					$state = $this->request->input('state');
					$state2 = $this->request->input('state2');
					$lga = $this->request->input('lga');
					$city = $this->request->input('city');
					$tribe = $this->request->input('tribe');
					$district = $this->request->input('district');
					$position = $this->request->input('position');
					$university = $this->request->input('university_attended');
					$age = $this->request->input('age');
					$gender = $this->request->input('gender');
					$marital_status = $this->request->input('marital_status');
					$salary = $this->request->input('salary');

					$data = DB::table('jobs')
										 ->where('country_of_origin','=',$country)
										 ->orwhere('country','=',$country2)
											->orwhere('state_of_origin','=',$state)
											->orwhere('job_state','=',$state2)
											->orwhere('lga_of_origin','=',$lga)
										   ->orwhere("job_city","LIKE","%$city%")
						                   ->orwhere('tribe','=',$tribe)
											->orwhere("address","LIKE","%$district%")
											->orwhere("job_name","LIKE","%$position%")
											->orwhere('dob','=',$age)
											->orwhere('gender','=',$gender)
											->orwhere('marital_status','=',$marital_status)
    										->orwhere('job_salary','=',$salary)
											->Join('users','jobs.user_id','=','users.id')
											->leftJoin('educationaldatas','jobs.user_id','=','educationaldatas.user_id')
											->leftJoin('skills','jobs.user_id','=','skills.user_id')
											->Join('biodatas','jobs.user_id','=','biodatas.user_id')
											->select('jobs.*','users.*','educationaldatas.*','biodatas.*','skills.*')
											->orderByRaw("RAND()")
                                            ->limit(20)
											->get();
				 return response()->json($data);

			}
}
