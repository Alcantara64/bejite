<?php namespace App\Http\Controllers;

use App\Educationaldata;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Skills;
use App\Workdata;
use App\Workhabit;
use App\Job as Jobs;
use Auth;
use Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class UserController extends Controller {

    protected $request;

	public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('ps');
        $this->request = $request;
    }
    public function getChangePassword()
    {
        return view('user.changepass');
    }
    public function saveChangedPassword()
    {
        //$password = bcrypt($this->request->input('current_password'));
        $this->validate($this->request,[
                            'current_password' => 'required|min:6',
                            'password' => 'required|min:6|confirmed',
                         ]);
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->password = bcrypt($this->request->input('password'));
        $user->save();
        flash()->overlay('Your password has been changed successfully','Password Changed');
        return redirect()
                    ->back();
    }
    public function settings()
    {
        $names = explode(' ',Auth::user()->fullnames);
        return view('user.settings',['names' => $names]);
    }
    public function saveSettings()
    {
        $this->validate($this->request,[
                            'fullnames' => 'required|min:2',
                            'email' => 'required|min:5',
                            'city' => 'required|min:5',
                            'address' => 'required|min:5',
                            'phone' => 'required|min:5',
                        ]);
        $id = Auth::user()->id;
        $user = Auth::user()->find($id);
        $photo = $user->photo;
        $user->fullnames = $this->request->input('firstname').' '.$this->request->input('lastname');
        $user->email = $this->request->input('email');
        $user->address = $this->request->input('address');
        $user->city = $this->request->input('city');
        $user->phone = $this->request->input('phone');
        $user->newsletter = $this->request->input('newsletter');
        $user->connect_notify = $this->request->input('connect_notify');
        if(!empty($this->request->file('photo'))){
            $file = $this->request->file('photo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('users/'.$file->getFilename().'.'.$extension, File::get($file));
            $user->photo = $file->getFilename().'.'.$extension;
            rename(storage_path().'/app/users/'.$file->getFilename().'.'.$extension,base_path('public/users/'.$file->getFilename().'.'.$extension));
            if(is_file(base_path('public/users/'.$photo)))
            {
                @unlink(base_path('public/users/'.$photo));
            }
        }
        $user->save();
        flash()->overlay('Settings has been updated successfully','Settings Updated');
        return redirect()->back();
    }

    public function getFeed($page = 1)
    {
        return ['feed'=>(new User())->getFeed($page)];
    }
    public function getUserFeed($username)
    {
        return ['feed'=>User::getUserPosts($username)];
    }
	public function getPostDetails(){
		return response()->json(['posts'=>getPostDetails()]);
	}
    public function profile($username)
    {
        if(!User::isUser($username))
        {
            return response()->make('Page not found',404);
        }
        $user = User::findByUsername($username);
        $education = Educationaldata::where('user_id',$user->id)->first();
        $hasEducationData = Educationaldata::where('user_id',$user->id)->count();
        $hasSkills = Skills::where('user_id',$user->id)->count();
        $skills = Skills::where('user_id',$user->id)->get();
        $habbit = Workhabit::where('user_id',$user->id)->first();
        $work = Workdata::where('user_id',$user->id)->get();
        $hasWork = Workdata::where('user_id',$user->id)->count();
        $job = Jobs::where('user_id',$user->id)->first();
        $hasJob = Jobs::where('user_id',$user->id)->count();
        return view('user.profile',[
            'profile'=>$user,
            'url'=>$this->request->path(),
            'education' => $education,
            'skills' => $skills,
            'hasSkills' => $hasSkills,
            'habbit' => $habbit,
            'work' => $work,
            'hasWork' => $hasWork,
            'hasEducationData' => $hasEducationData,
            'hasJob' => $hasJob,
            'job' => $job,
        ]);
    }
    public function userConnections($username)
    {
        if(!User::isUser($username))
        {
            return response()->make('Page not found',404);
        }
        $user = User::findByUsername($username);
        return view('user.connections',['profile'=>$user,'url'=>$this->request->path()]);
    }
    public function getConnections($id)
    {
        return response()->json(['connections'=>User::getConnections($id)]);
    }
    public function profileSettings($flag='')
    {
        return view('user.settings',['flag'=>$flag]);
    }
    public function updateUserData()
    {
        $this->validate($this->request,[
            'fullnames' => 'required|max:120',
            'summary' => 'required|max:1500',
            'phone' => 'required|numeric',
            'city' => 'required|max:255',
            'state' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|max:255',
            'bday' => 'required|max:2',
            'bmonth' => 'required|max:2',
            'byear' => 'required|max:4',
        ]);
        if(strtolower($this->request->input('email')) != Auth::user()->email)
        {
            $count = User::where('email',$this->request->input('email'))->count();
            if($count == 1)
            {
                return response()->json(['status'=>'fail','feedback' => 'This email address is already registered in our system.']);
            }
        }
        if(strtolower($this->request->input('username')) != Auth::user()->username)
        {
            $count = User::where('username',$this->request->input('username'))->count();
            if($count == 1)
            {
                return response()->json(['status'=>'fail','feedback' => 'This Username is already registered in our system.']);
            }
        }
        $user = User::find(Auth::user()->id);
        $user->fullnames = $this->request->input('fullnames');
        $user->phone = $this->request->input('phone');
        $user->city = $this->request->input('city');
        $user->state = $this->request->input('state');
        $user->country = $this->request->input('country');
        $user->summary = $this->request->input('summary');
        $user->username = $this->request->input('username');
        $user->email = $this->request->input('email');
        $user->bday = $this->request->input('bday');
        $user->bmonth = $this->request->input('bmonth');
        $user->byear = $this->request->input('byear');
        $user->save();
        return response()->json(['status'=>'success']);
    }
    public function changePassword()
    {
        $this->validate($this->request,[
            'pass' => 'required',
            'password' => 'required|confirmed'
        ]);
        if(Hash::check($this->request->input('pass'),Auth::user()->password))
        {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($this->request->input('password'));
            $user->save();
            return response()->json(['status'=>'success']);
        }
    }
    public function updatePrivacy()
    {
        $this->validate($this->request,[
            'profile-view' => 'required',
            'profile-birthday' => 'required',
            'profile-msg' => 'required',
            'profile-notification' => 'required',
        ]);
        $s = UserSetting::where('user_id',Auth::user()->id)->first();
        $setting = UserSetting::find($s->id);
        $setting->profile_view = $this->request->input('profile-view');
        $setting->profile_birthday = $this->request->input('profile-birthday');
        $setting->profile_msg = $this->request->input('profile-msg');
        $setting->profile_notification = $this->request->input('profile-notification');
        $setting->save();
        return response()->json(['status'=>'success']);
    }
    public function updateNotifications()
    {
        $this->validate($this->request,[
            'profile-connect-with-me' => 'required',
            'profile-post-comment' => 'required',
            'profile-like' => 'required',
            'profile-post-share' => 'required',
        ]);
        $s = UserSetting::where('user_id',Auth::user()->id)->first();
        $setting = UserSetting::find($s->id);
        $setting->profile_connect_with_me = $this->request->input('profile-connect-with-me');
        $setting->profile_post_comment = $this->request->input('profile-post-comment');
        $setting->profile_like = $this->request->input('profile-like');
        $setting->profile_post_share = $this->request->input('profile-post-share');
        $setting->save();
        return response()->json(['status'=>'success']);
    }
    public function deleteAccount()
    {
        $this->validate($this->request,[
            'password' => 'required'
        ]);
        if(Hash::check($this->request->input('password'),Auth::user()->password))
        {
            $count = AdminRequest::where('user_id',Auth::user()->id)->count();
            if($count == 0)
            {
                $ar = new AdminRequest();
                $ar->user_id = Auth::user()->id;
                $ar->name = 'Delete Account';
                $ar->type = 'delete_account';
                $ar->status = 'pending';
                $ar->save();
            }
            return response()->json(['status'=>'success']);
        }
    }
}
