<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use App\Employers;
use App\Option;
use App\Page;
use Auth;

class AdminController extends Controller {

    protected $request;

    public function __construct(Request $request){
        $this->middleware('admin');
        $this->request = $request;
    }
	public function index(){
        $options = Option::find(1);
        $d1 = new \DateTime('now');
        $d2 = new \DateTime("2016-01-15 12:39:01");
        $diff = $d2->diff($d1)->format("%h");
        return view('admin.dashboard',['option'=>$options,'d'=>$diff]);
    }
    public function getJobseekers(){
        $jobseekers = User::where('account_type','jobseeker')->get();
        return view('admin.users.jobseeker',['jobseekers'=>$jobseekers,'i'=> 0]);
    }
    public function getEmployers(){
        $employers = User::where('account_type','employer')
                            ->join('employers','users.id','=','employers.user_id')
                            ->select('employers.*','users.*')
                            ->get();
        return view('admin.users.employer',['employers'=>$employers,'i'=> 0]);
    }
    public function getActivateUser($id){
        $user = User::find($id);
        $user->account_status = 'active';
        $user->save();
        flash()->overlay('User was Activated Successfully','User Activated');
        return redirect('admin/jobseekers');
    }
    public function getDeactivateUser($id){
        $user = User::find($id);
        $user->account_status = 'inactive';
        $user->save();
        flash()->overlay('User was Deactivated Successfully','User Deactivated');
        return redirect('admin/jobseekers');
    }
    public function postSaveSiteSettings()
    {
        $this->validate($this->request,[
                            'descriptions'=> 'required',
                            'keywords' => 'required',
                        ]);
        $option = Option::findOrNew(1);
        $option->keywords = $this->request->input('keywords');
        $option->descriptions = $this->request->input('descriptions');
        $logo = $option->logo;
        if($this->request->hasFile('logo'))
        {
            $file = $this->request->file('logo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('logos/'.$file->getFilename().'.'.$extension, File::get($file));
            $option->logo = $file->getFilename().'.'.$extension;
            rename(storage_path().'/app/logos/'.$file->getFilename().'.'.$extension,base_path('public/logo/'.$file->getFilename().'.'.$extension));
            if(is_file(storage_path().'/app/logos/'.$logo))
            {
                @unlink(storage_path().'/app/logos/'.$logo);
            }

        }
        $option->save();
        flash()->overlay('Your Website informations was updated successfully','Updated Successfully');
        return redirect()->back();
    }
    public function getSaveSiteSettings()
    {
        return redirect('admin');
    }
    public function getAddPage()
    {
        return view('admin.pages.add');
    }
    public function postAddPage()
    {
        $this->validate($this->request,[
                         'title'=> 'required|max:255|min:3',
                         'content'=>'required|min:3',
                         'location'=>'required',
                         'name'=>'required',
                         'order'=>'required|numeric',
                        ]);
        $page = new Page();
        $page->title = $this->request->input('title');
        $page->name = $this->request->input('name');
        $page->location = $this->request->input('location');
        $page->page_order = $this->request->input('order');
        $page->content = $this->request->input('content');
        $page->slug = strtolower(str_slug($this->request->input('title')));
        $page->save();
        flash()->overlay('Page Created Successfully','Page Created');
        return redirect('admin/pages');
    }
    public function postUpdatePage($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid page url');
            return redirect()->back();
        }
        $this->validate($this->request,[
                         'title'=> 'required|max:255|min:3',
                         'content'=>'required|min:3',
                         'location'=>'required',
                         'name'=>'required',
                         'order'=>'required|numeric',
                        ]);
        $page = Page::find($id);
        $page->title = $this->request->input('title');
        $page->name = $this->request->input('name');
        $page->location = $this->request->input('location');
        $page->page_order = $this->request->input('order');
        $page->content = $this->request->input('content');
        $page->slug = strtolower(str_slug($this->request->input('title')));
        $page->save();
        flash()->overlay('Page Update Successfully','Page Update');
        return redirect('admin/pages');
    }
    public function getPages()
    {
        $pages = Page::all();
        return view('admin.pages.list',['pages'=>$pages,'i'=>1]);
    }
    public function getEditPage($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid page url');
            return redirect()->back();
        }
        $page = Page::find($id);
        return view('admin.pages.edit',['page'=>$page]);
    }
    public function getDeletePage($id)
    {
        $page = Page::find($id);
        $page->delete();
        flash()->overlay("Page Deleted Successfully");
        return redirect()->back();
    }
    public function getSiteSettings()
    {
        $options = Option::find(1);
        return view('admin.index',['option'=>$options]);
    }
}
