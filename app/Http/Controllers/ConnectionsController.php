<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Connections;
use App\User;
use Auth;
//use DB;

class ConnectionsController extends Controller {

	protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->middleware('auth');
    }
    public function follow()
    {
        $this->validate($this->request,[
                        'id' => 'required|numeric'
                        ]);
        $id = $this->request->input('id');
        $count = Connections::where('user_id',$id)
            ->where('follower',Auth::user()->id)
            ->orWhere(function($query) use ($id){
                $query->where('user_id',Auth::user()->id)
                    ->where('follower',$id);
            })
            ->count();
        if($count < 1)
        {
            $con =  new Connections;
            $con->user_id = Auth::user()->id;
            $con->follower = $id;
			$con->notified = 'no';
            $con->save();
            $con2 = new Connections;
            $con2->user_id = $id;
            $con2->follower = Auth::user()->id;
			$con2->notified = 'no';
            $con2->save();
        }
        return response()->json(['status'=>'ok']);
    }
    public function unfollow()
    {
        $this->validate($this->request,[
                        'id' => 'required|numeric'
                        ]);
        $id = $this->request->input('id');
        $user1 = Connections::where('user_id',Auth::user()->id)
			->where('follower',$id);	
		$user1->delete();
		$user2 = Connections::where('user_id',$id)
		->where('follower',Auth::user()->id);
		$user2->delete();
        return response()->json(['status'=>'ok']);
    }
    public function findPeople()
    {
        $people = User::where('id','!=',Auth::user()->id)
            //->where('country',Auth::user()->country)
            ->get();
        return response()->json($people);
    }
	public function notify(){
		\App\User::markAsNotified();
		return response()->json(['status'=>'ok']);
	}
}
