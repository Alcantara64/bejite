<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Post;

use App\Http\Requests;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Page::where('slug',trim($slug))->where('status','published')->first();
        $posts = Post::where('status','published')->take(5)->get();
        return view('pages.index',['page'=>$page,'posts' => $posts]);
    }
}
