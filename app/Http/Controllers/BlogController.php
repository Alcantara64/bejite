<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class BlogController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function index($slug)
    {
        $post = Post::where('slug',$slug)->first();
        $posts = Post::where('status','published')->take(5)->get();
        return view('blog.index',['p'=>$post,'posts' => $posts]);
    }
    public function showAddNew()
    {
        return view('admin.blog.index',['posts'=>Post::all()]);
    }
    public function addNew()
    {
        $this->validate($this->request,[
            'img' => 'required|max:5000|image|mimes:jpg,jpeg,gif,png',
            'title' => 'required|max:255',
            'status' => 'required|max:30',
            'content' => 'required',
        ]);
        $post = new Post();
        $post->title = $this->request->input('title');
        $post->slug = str_replace(' ','-',strtolower($this->request->input('title')));
        $post->user_id = Auth::user()->id;
        $post->status = $this->request->input('status');
        $post->content = $this->request->input('content');
        if($this->request->hasFile('img'))
        {
            $file = $this->request->file('img');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/posts/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/posts'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/posts'.DIRECTORY_SEPARATOR.$img);
            $post->thumb = $img;
        }
        $post->save();
        return response()->json(['status'=>'success','feedback'=>'Post was successfully Created']);
    }
    public function showUpdatePost($id)
    {
        return view('admin.blog.edit',['p'=>Post::find($id),'posts'=>Post::all()]);
    }
    public function updatePost($id)
    {
        $this->validate($this->request,[
            'title' => 'required|max:255',
            'status' => 'required|max:30',
            'content' => 'required',
        ]);
        $post = Post::find($id);
        if($this->request->hasFile('img'))
        {
            $currentImg = public_path().'/uploads/posts/'.$post->thumb;
            if(is_file($currentImg))
            {
                @unlink($currentImg);
            }
            $file = $this->request->file('img');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('public/posts/'.$file->getFilename().'.'.$extension, File::get($file));
            $img = $file->getFilename().'.'.$extension;
            @rename(storage_path().'/app/public/posts'.DIRECTORY_SEPARATOR.$img,public_path().'/uploads/posts'.DIRECTORY_SEPARATOR.$img);
            $post->thumb = $img;
        }
        $post->title = $this->request->input('title');
        $post->slug = str_replace(' ','-',strtolower($this->request->input('title')));
        $post->status = $this->request->input('status');
        $post->content = $this->request->input('content');
        $post->save();
        return response()->json(['status'=>'success','feedback'=>'Post was successfully Updated']);
    }
    public function deletePost($id)
    {
        $post = Post::find($id);
        $post->delete();
        return response()->json(['status'=>'success','feedback'=>'Post was successfully Deleted']);
    }
    public function ajaxBlogPosts()
    {
        return response()->json(['posts'=>Post::all()]);
    }
}
