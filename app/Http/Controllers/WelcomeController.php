<?php namespace App\Http\Controllers;
use Auth;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');s
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check())
		{
			return redirect('/home');
		}
		return view('welcome');
	}
	
	public function terms_and_conditions()
	{
		return view('other_pages.terms_and_conditions');
	}
	
	public function privacy_policy()
	{
		return view('other_pages.privacy_policy');
	}
	
	public function cookie_policy()
	{
		return view('other_pages.cookie_policy');
	}
	
	public function how_to_use_bejite()
	{
		return view('other_pages.how_to_use_bejite');
	}
	
	public function about()
	{
		return view('other_pages.about');
	}
	
	public function security_advice()
	{
		return view('other_pages.security_advice');
	}
	

}
