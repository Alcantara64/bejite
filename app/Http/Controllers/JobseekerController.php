<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Biodata;
use App\Educationaldata;
use App\Secondryschool;
use App\Vacationaldata;
use App\Professionaldata;
use App\Workdata;
use App\Job;
use App\Skills;
use App\Journal;
use App\Nonprofit;
use App\Spectacular;
use App\Hobby;
use App\Workhabit;
use App\University;
use App\Referee;


class JobseekerController extends Controller {

	protected $request;

    public function __construct(Request $request){
        $this->middleware('auth');
        //$this->middleware('jobseeker');
        $this->request = $request;
    }
    public function showUpdateData(){
        $names = explode(' ',Auth::user()->fullnames);
        $biodata = Biodata::where('user_id',Auth::user()->id)->first();
        $edu = Educationaldata::where('user_id',Auth::user()->id)->get();
        $sec = Secondryschool::where('user_id',Auth::user()->id)->get();
        $profession = Professionaldata::where('user_id',Auth::user()->id)->get();
        $work = Workdata::where('user_id',Auth::user()->id)->get();
        $skills = Skills::where('user_id',Auth::user()->id)->get();
        $journals = Journal::where('user_id',Auth::user()->id)->get();
        $spectacular = Spectacular::where('user_id',Auth::user()->id)->first();
        $hobbies = Hobby::where('user_id',Auth::user()->id)->get();
        $nonprofits = Nonprofit::where('user_id',Auth::user()->id)->get();
        $referees = Referee::where('user_id',Auth::user()->id)->get();
        $vacation = Vacationaldata::where('user_id',Auth::user()->id)->first();
        $job = Job::where('user_id',Auth::user()->id)->first();
        $workhabit = Workhabit::where('user_id',Auth::user()->id)->first();
				$university =University::get();
        return view('jobseeker.update',
                    [
                        'names' => $names,
												'university'=>	$university,
                        'biodata' => $biodata,
                        'vacation' => $vacation,
                        'profession' => $profession,
                        'educational'=>$edu,
                        'secondry'=> $sec,
                        'work' => $work,
                        'skills' => $skills,
                        'nonprofits' => $nonprofits,
                        'spectacular' => $spectacular,
                        'wkh' => $workhabit,
                        'hobbies' => $hobbies,
                        'referees' => $referees,
                        'job'=> $job,
                        'journals' => $journals,
                        'edui'=>1,
                        'seci'=>1,
                        'profi' => 1,
                        'wki' => 1,
                        'skilli' => 1,
                        'ji' => 1,
                        'hobbyi' => 1,
                        'ri' => 1,
                        'noni' => 1,
                    ]);
    }
    public function saveBioData(){
        $user = Biodata::where('user_id',Auth::user()->id)->first();
        $this->validate($this->request,[
                            'country' => 'required',
                            'state' => 'required',
                            'lga_of_origin' => 'required|min:3|max:255',
                            'marital_status' => 'required',
                            'date_of_birth' => 'required',
                            'language_spoken' => 'required|max:255',
                            'language_proficiency' => 'required|min:2|max:255',
                            'complexion' => 'required|min:3|max:255',
                            'height' => 'required|min:3|max:255',
                            'physical_features' => 'required|min:3|max:255',
                            'religion' => 'required|min:3|max:255',
                            'tribe' => 'required|min:3|max:255',
                            'race' => 'required|min:3|max:255',
                        ]);
        $biodata = Biodata::find($user['id']);
        $biodata->country_of_origin = $this->request->input('country');
        $biodata->state_of_origin = $this->request->input('state');
        $biodata->lga_of_origin = $this->request->input('lga_of_origin');
        $biodata->dob = $this->request->input('date_of_birth');
        $biodata->marital_status = $this->request->input('marital_status');
        $biodata->language_spoken = $this->request->input('language_spoken');
        $biodata->language_proficiency = $this->request->input('language_proficiency');
        $biodata->complexion = $this->request->input('complexion');
        $biodata->physical_features = $this->request->input('physical_features');
        $biodata->religion = $this->request->input('religion');
        $biodata->tribe = $this->request->input('tribe');
        $biodata->height = $this->request->input('height');
        $biodata->race = $this->request->input('race');
        $biodata->save();
        flash()->overlay("Your bio data was updated successfully","Updated Successfully");
        return redirect()->back();
    }
    public function addEductionalData(){
        $this->validate($this->request,[
                            'university_attended' => 'required|min:3|max:255',
                            'degree_obtained' => 'required|min:3|max:255',
                            'course' => 'required|min:3|max:255',
                            'year_graduated' => 'required|min:3|max:255',
                        ]);
        $edu = new Educationaldata;
        $edu->user_id = Auth::user()->id;
        $edu->university_attended = $this->request->input('university_attended');
        $edu->degree_obtained = $this->request->input('degree_obtained');
        $edu->course = $this->request->input('course');
        $edu->year_graduated = $this->request->input('year_graduated');
        $edu->save();
        flash()->overlay('Your Educational Qualification data was added successfully','Added Successfully');
        return redirect()->back();
    }
    public function deleteEductionalData($id){
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $edu = Educationaldata::find($id);
        $edu->delete();
        flash()->overlay('Your Educational Qualification data was deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function addSecondaySchoolData(){
        $this->validate($this->request,[
                            'secondary_school' => 'required|min:3|max:255',
                            'qualification' => 'required|min:3|max:255',
                            'secondary_school_year' => 'required|min:3|max:255',
                        ]);
        $edu = new Secondryschool;
        $edu->user_id = Auth::user()->id;
        $edu->secondary_school = $this->request->input('secondary_school');
        $edu->secondary_school_qualification = $this->request->input('qualification');
        $edu->secondary_school_year = $this->request->input('secondary_school_year');
        $edu->save();
        flash()->overlay('Your Secondry school education data was added successfully','Added Successfully');
        return redirect()->back();
    }
    public function deleteSecondaySchoolData($id){
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $edu = Secondryschool::find($id);
        $edu->delete();
        flash()->overlay('Your Secondry school education data was deleted successfully','deleted Successfully');
        return redirect()->back();
    }
    public function saveVacationalData(){
        $user = Vacationaldata::where('user_id',Auth::user()->id)->first();
        $this->validate($this->request,[
                            'vacation_school_name' => 'required|min:3|max:255',
                            'vacation_qualification' => 'required|min:3|max:255',
                            'vacation_school_year' => 'required|min:2|max:4',
                        ]);
        $vacation = Vacationaldata::find($user->id);
        $vacation->school = $this->request->input('vacation_school_name');
        $vacation->qualification = $this->request->input('vacation_qualification');
        $vacation->year = $this->request->input('vacation_school_year');
        $vacation->save();
        flash()->overlay('Your Vacational education data was updated successfully','Updated Successfully');
        return redirect()->back();
    }
    public function addProfessionalData(){
        $this->validate($this->request,[
                            'profession_name' => 'required|max:255',
                            'profession_year_obtained' => 'required|min:4|max:255',
                            'profession_specialization' => 'required|max:255',
                            'profession_experience' => 'required|max:255',
                        ]);
        $prof = new Professionaldata;
        $prof->user_id = Auth::user()->id;
        $prof->profession_name = $this->request->input('profession_name');
        $prof->profession_year_obtained = $this->request->input('profession_year_obtained');
        $prof->profession_specialization = $this->request->input('profession_specialization');
        $prof->profession_experience = $this->request->input('profession_experience');
        $prof->save();
        flash()->overlay('Your Professional Qualification data was added successfully','Added Successfully');
        return redirect()->back();
    }
    public function deleteProfessionalData($id){
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $prof = Professionaldata::find($id);
        $prof->delete();
        flash()->overlay('Your Professional Qualification data was deleted successfully','Updated Successfully');
        return redirect()->back();
    }
    public function addWorkData(){
        $this->validate($this->request,[
                            'work_place' => 'required|max:255',
                            'work_position' => 'required|max:255',
                            'work_sector' => 'required|max:255',
                            'work_experience' => 'required|max:255',
                        ]);
        $work = new Workdata;
        $work->user_id = Auth::user()->id;
        $work->work_place = $this->request->input('work_place');
        $work->work_position = $this->request->input('work_position');
        $work->work_sector = $this->request->input('work_sector');
        $work->work_experience = $this->request->input('work_experience');
        $work->work_reasons = $this->request->input('work_reasons');
        $work->save();
        flash()->overlay('Your Work data was deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function deleteWorkData($id){
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $work = Workdata::find($id);
        $work->delete();
        flash()->overlay('Your Work data was deleted successfully','Updated Successfully');
        return redirect()->back();
    }
    public function addSkillData(){
            $this->validate($this->request,[
                                'skill_type' => 'required',
                                'skill_name' => 'required|min:3|max:255',
                                'skill_year' => 'required|min:3|max:255',
                            ]);
            $skill = new Skills();
            $skill->user_id = Auth::user()->id;
            $skill->skill_type = $this->request->input('skill_type');
            $skill->skill_name = $this->request->input('skill_name');
            $skill->skill_year = $this->request->input('skill_year');
            $skill->save();
            flash()->overlay('Your Skill data was added successfully','Added Successfully');
            return redirect()->back();
    }
    public function deleteSkillData($id){
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $skill = Skills::find($id);
        $skill->delete();
        flash()->overlay('Your Skill data was deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function saveJobAppliedForData(){
        $user = Job::where('user_id','=',Auth::user()->id)->first();
        $this->validate($this->request,[
                                'job_country' => 'required',
                                'job_name' => 'required|min:3|max:255',
                                'job_type' => 'required|min:3|max:255',
                                'job_sector' => 'required|min:3|max:255',
                                'job_state' => 'required|min:3|max:255',
                                'job_lga' => 'required|min:3|max:255',
                                'job_salary' => 'required|min:3|max:255',
                            ]);
        $job = Job::find($user->id);
        $job->job_country = $this->request->input('job_country');
        $job->job_state = $this->request->input('job_state');
        $job->job_name = $this->request->input('job_name');
        $job->job_sector = $this->request->input('job_sector');
        $job->job_type = $this->request->input('job_type');
        $job->job_salary = $this->request->input('job_salary');
        $job->job_lga = $this->request->input('job_lga');
        $job->job_city = $this->request->input('job_city');
        $job->job_town = $this->request->input('job_town');
        $job->save();
        flash()->overlay('Your Job data was Saved successfully','Saved Successfully');
        return redirect()->back();
    }
    public function addJournalData(){
        $this->validate($this->request,[
                                'journal_name' => 'required|min:3|max:255',
                                'journal_type' => 'required|min:3|max:255',
                                'journal_published' => 'required|min:3|max:255',
                                'journal_publisher' => 'required|min:3|max:255',
                            ]);
            $journal = new Journal();
            $journal->user_id = Auth::user()->id;
            $journal->name = $this->request->input('journal_name');
            $journal->type = $this->request->input('journal_type');
            $journal->year_published = $this->request->input('journal_published');
            $journal->publisher = $this->request->input('journal_publisher');
            $journal->save();
            flash()->overlay('Your Journal data was Added successfully','Added Successfully');
            return redirect()->back();
    }
    public function deleteJournal($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $journal = Journal::find($id);
        $journal->delete();
        flash()->overlay('Your Journal data was Deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function saveWorkHabitData(){
        $user = Workhabit::where('user_id',Auth::user()->id)->first();
        $this->validate($this->request,[
                                'night_reading' => 'required|max:255',
                                'night_driving' => 'required|max:255',
                                'have_business' => 'required|max:255',
                                'have_drivers_license' => 'required|max:255',
                                'typing_speed' => 'required|max:255',
                                'coding_speed' => 'required|max:255',
                                'armed_force' => 'required|max:255',
                                'convicted' => 'required|max:255',
                                'availibility' => 'required|max:255',
                            ]);
            $habit = Workhabit::find($user->id);
            $habit->night_reading = $this->request->input('night_reading');
            $habit->night_driving = $this->request->input('night_driving');
            $habit->have_business = $this->request->input('have_business');
            $habit->drivers_license = $this->request->input('have_drivers_license');
            $habit->typing_speed = $this->request->input('typing_speed');
            $habit->coding_speed = $this->request->input('coding_speed');
            $habit->armed_force = $this->request->input('armed_force');
            $habit->convicted = $this->request->input('convicted');
            $habit->availability = $this->request->input('availibility');
            $habit->save();
            flash()->overlay('Your Work Habit data was Saved successfully','Saved Successfully');
            return redirect()->back();
    }
    public function addNonProfit(){
        $this->validate($this->request,[
                                'name' => 'required|max:255',
                                'position' => 'required|max:255',
                                'year' => 'required|min:4|max:255',
                            ]);
            $nonp = new Nonprofit();
            $nonp->user_id = Auth::user()->id;
            $nonp->name = $this->request->input('name');
            $nonp->position = $this->request->input('position');
            $nonp->year = $this->request->input('year');
            $nonp->save();
            flash()->overlay('Your Non Profit data was Added successfully','Added Successfully');
            return redirect()->back();
    }
    public function deleteNonProfit($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $nonp = Nonprofit::find($id);
        $nonp->delete();
        flash()->overlay('Your Non Profit data was Deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function saveSpectacular(){
        $user = Spectacular::where('user_id','=',Auth::user()->id)->first();
        $this->validate($this->request,[
                                'name' => 'required|max:255',
                            ]);
            $spec = Spectacular::find($user->id);
            $spec->name = $this->request->input('name');
            $spec->save();
            flash()->overlay('Your data was Saved successfully','Saved Successfully');
            return redirect()->back();
    }
    public function addHobby(){
        $this->validate($this->request,[
                                'name' => 'required|max:255',
                            ]);
            $hobby = new Hobby();
            $hobby->user_id = Auth::user()->id;
            $hobby->name = $this->request->input('name');
            $hobby->save();
            flash()->overlay('Your Hobby was Added successfully','Added Successfully');
            return redirect()->back();
    }
    public function deleteHobby($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $hobby = Hobby::find($id);
        $hobby->delete();
        flash()->overlay('Your Hobby was Deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
    public function addReferee(){
        $this->validate($this->request,[
                                'name' => 'required|max:255',
                                'occupation' => 'required|max:255',
                                'organisation' => 'required|max:255',
                                'relationship' => 'required|max:255',
                                'years' => 'required|max:255',
                            ]);
            $referee = new Referee();
            $referee->user_id = Auth::user()->id;
            $referee->name = $this->request->input('name');
            $referee->occupation = $this->request->input('occupation');
            $referee->organisation = $this->request->input('organisation');
            $referee->relationship = $this->request->input('relationship');
            $referee->years = $this->request->input('years');
            $referee->save();
            flash()->overlay('Your Referee was Added successfully','Added Successfully');
            return redirect()->back();
    }
    public function deleteReferee($id)
    {
        if(!is_numeric($id))
        {
            flash()->overlay('Invalid url. please try again','Invalida url');
            return redirect()->back();
        }
        $hobby = Referee::find($id);
        $hobby->delete();
        flash()->overlay('Your Referee was Deleted successfully','Deleted Successfully');
        return redirect()->back();
    }
}
