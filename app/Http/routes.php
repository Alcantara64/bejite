<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('page/{slug}','PageController@index');
Route::get('blog/posts/{slug}','BlogController@index');

//OTHER PAGES
Route::get('terms-and-conditions','WelcomeController@terms_and_conditions');
Route::get('privacy-policy','WelcomeController@privacy_policy');
Route::get('cookie-policy','WelcomeController@cookie_policy');
Route::get('how-to-use-bejite','WelcomeController@how_to_use_bejite');
Route::get('security-advice','WelcomeController@security_advice');
Route::get('about','WelcomeController@about');

Route::group(['middleware' => 'reg'],function(){
    Route::get('my-cv', 'HomeController@myCV');
    Route::get('home', 'HomeController@index');
    //USER
    Route::get('user/change-password','UserController@changePassword');
    Route::post('user/change-password','UserController@saveChangedPassword');
    Route::get('user/settings','UserController@settings');
    Route::post('user/settings','UserController@saveSettings');

});
//ACCOUNT
Route::post('account/setup/save-step-2', 'AccountController@saveStepTwo');
Route::get('account/setup', 'AccountController@setup');
Route::post('account/setup', 'AccountController@saveSetup');
Route::get('account/upload-photo', 'AccountController@photoUpload');
Route::post('account/upload-photo', 'AccountController@savePhotoUpload');
Route::get('account/employer', 'AccountController@employer');
Route::post('account/employer', 'AccountController@saveEmployer');
Route::post('account/photo-continue', 'AccountController@photoContinue');
//finish registration
Route::get('/getting-started/finish', 'DiscoverController@finish');
//discover people
Route::get('discover','DiscoverController@index');

//MESSAGES
Route::get('messages','MessageController@index');
Route::post('messages/add','MessageController@addMessage');
Route::get('messages/user/{receiver}/{sender}','MessageController@getConversation');

Route::post('discover/follow','DiscoverController@follow');
Route::post('discover/finish','DiscoverController@finish');

//CONNECTIONS
Route::post('connection/follow','ConnectionsController@follow');
Route::post('connection/unfollow','ConnectionsController@unfollow');
Route::post('notify','ConnectionsController@notify');

Route::get('user/{id}/connections','UserController@getConnections');

//FIND PEOPLE
Route::get('connection/find-plople','ConnectionsController@findPeople');
//ADMIN
//UPLOADS
Route::post('/upload/profile-photo','UploadController@profilePhoto');
Route::post('/upload/cover-photo','UploadController@coverImage');


//CHAT
Route::get('chat/init','ChatController@startSession');
Route::post('chat/create','ChatController@createChatbox');
Route::post('chat/close','ChatController@closeChat');
Route::post('chat/push','ChatController@saveMessage');
Route::get('chat/requests','ChatController@chatRequests');
Route::get('chat/request/remove','ChatController@removeChatRequests');




Route::group(['prefix'=>'admin'], function() {

    Route::get('/','AdminController@dashboard');
    Route::get('site/settings','AdminController@showSiteSettings');
    Route::post('site/settings','AdminController@saveSiteSettings');

    //PAGES
    Route::get('pages/new','AdminController@showPages');
    Route::post('pages/new','AdminController@savePage');
    Route::get('pages/{id}/edit','AdminController@showUpdatePage');
    Route::post('pages/{id}/edit','AdminController@updatePage');
    Route::get('pages/{id}/delete','AdminController@deletePage');
    Route::get('pages/ajax/list-pages','AdminController@ajaxListPages');
    Route::get('pages/ajax/list-pages','AdminController@ajaxListPages');

    //SLIDERS
    Route::get('sliders','AdminController@showSliders');
    Route::post('sliders/save','AdminController@saveSlider');
    Route::get('sliders/{id}/edit','AdminController@showUpdateSlider');
    Route::post('sliders/{id}/edit','AdminController@updateSlider');
    Route::get('sliders/{id}/delete','AdminController@deleteSlider');
    Route::get('sliders/ajax/list','AdminController@ajaxDisplaySliders');
    Route::get('users','AdminController@showUsers');

    //LOGIN
    Route::get('/login','AdminAuthController@getlogin');
    Route::post('/login','AdminAuthController@postLogin');
    //User
    Route::get('/profile/settings','AdminController@showProfileSettings');
    Route::post('/profile/{id}/update','AdminController@saveProfileSettings');
    Route::post('/profile/{id}/save-photo','AdminController@changeProfilePhoto');
    //CHANGE PASSWORD
    Route::get('/profile/change-password','AdminController@showChangePassword');
    Route::post('/profile/change-password','AdminController@changePassword');
    //LOG OUT
    Route::get('logout','AdminController@logout');

    //Blog
    Route::get('blog','BlogController@showAddNew');
    Route::get('blog/ajax/posts','BlogController@ajaxBlogPosts');
    Route::post('blog/new','BlogController@addNew');
    Route::get('blog/posts/{id}/edit','BlogController@showUpdatePost');
    Route::post('blog/posts/{id}/edit','BlogController@updatePost');
    Route::get('blog/posts/{id}/delete','BlogController@deletePost');
});

//SEARCH
Route::get('search','SearchController@index');
Route::get('search-home','SearchController@home');
Route::get('history','SearchController@history');
Route::get('interview-invites','SearchController@interviewInvites');
Route::get('review','SearchController@review');
Route::get('store','SearchController@store');
Route::get('recommendation','SearchController@recommendation');
Route::get('add-pro','SearchController@adpro');
Route::get('cv','SearchController@cv');

/*POSTS*/

Route::post('/post/feed','PostController@savePost');
Route::post('/post/edit/feed','PostController@updatePost');
Route::post('/post/like','PostController@like');
Route::post('/post/get-likes','PostController@getLikes');
Route::post('/post/share','PostController@share');
Route::post('/post/report','PostController@report');
Route::post('/post/save','PostController@save');
Route::post('/post/unsave','PostController@unsave');
Route::post('/post/hide','PostController@hide');
Route::post('/post/edit','PostController@edit');
Route::post('/post/delete','PostController@delete');

//COMMENTS
Route::post('/post/addcomment','PostController@saveComment');
Route::get('/post/{id}/comments','PostController@getComments');

//FEEDS
Route::get('/user/feed/{id?}','UserController@getFeed');
Route::get('/{user}/feed','UserController@getUserFeed');
Route::get('/postdetails','UserController@getPostDetails');

/* LEGACY ROUTES
Route::get('admin','AdminController@index');
Route::get('admin/jobseekers','AdminController@getJobseekers');
Route::get('admin/employers','AdminController@getEmployers');
Route::get('admin/user/{id}/activate','AdminController@getActivateUser');
Route::get('admin/user/{id}/deactivate','AdminController@getDeactivateUser');
Route::get('admin/site/save-settings','AdminController@getSaveSiteSettings');
Route::post('admin/site/save-settings','AdminController@postSaveSiteSettings');
Route::get('admin/pages/add','AdminController@getAddPage');
Route::post('admin/pages/add','AdminController@postAddPage');
Route::post('admin/page/{id}/update','AdminController@postUpdatePage');
Route::get('admin/pages','AdminController@getPages');
Route::get('admin/page/{id}/edit','AdminController@getEditPage');
Route::post('admin/page/{id}/delete','AdminController@getDeletePage');
Route::post('admin/site/settings','AdminController@getSiteSettings');
//Route::controller('admin','AdminController');
*/
//IMAGES
//Route::controller('images','ImagesController');
Route::post('upload/save-post-image','HomeController@savePostImage');
//PAGES
//Route::get('page/{slug}', 'Page@show');
//Jobseeker
Route::post('jobseeker/add-hobby','JobseekerController@addHobby');
Route::get('jobseeker/update-data','JobseekerController@showUpdateData');
Route::post('jobseeker/save-basic-info','JobseekerController@saveBasicInfo');
Route::post('jobseeker/save-bio-data','JobseekerController@saveBioData');
Route::get('jobseeker/{id}/delete-educational-data','JobseekerController@deleteEductionalData');
Route::post('jobseeker/add-educational-data','JobseekerController@addEductionalData');
Route::get('jobseeker/{id}/delete-secondry-school','JobseekerController@deleteSecondaySchoolData');
Route::post('jobseeker/add-secondry-school','JobseekerController@addSecondaySchoolData');
Route::post('jobseeker/save-vacational-data','JobseekerController@saveVacationalData');
Route::post('jobseeker/add-professional-data','JobseekerController@addProfessionalData');
Route::get('jobseeker/{id}/delete-professional-data','JobseekerController@deleteProfessionalData');
Route::post('jobseeker/add-work-data','JobseekerController@addWorkData');
Route::get('jobseeker/{id}/delete-work-data','JobseekerController@deleteWorkData');
Route::post('jobseeker/add-skill-data','JobseekerController@addSkillData');
Route::get('jobseeker/{id}/delete-skill-data','JobseekerController@deleteSkillData');
Route::post('jobseeker/save-job-applied-for-data','JobseekerController@saveJobAppliedForData');
Route::post('jobseeker/add-journal-data','JobseekerController@addJournalData');
Route::get('jobseeker/{id}/delete-journal-data','JobseekerController@deleteJournal');
Route::post('jobseeker/add-nonprofit-data','JobseekerController@addNonProfit');
Route::get('jobseeker/{id}/delete-nonprofit-data','JobseekerController@deleteNonProfit');
Route::post('jobseeker/save-spectacular','JobseekerController@saveSpectacular');
Route::post('jobseeker/add-hobby-data','jobseekercontroller@addHobby');
Route::get('jobseeker/{id}/delete-hobby-data','JobseekerController@deleteHobby');
Route::post('jobseeker/save-work-habit-data','JobseekerController@saveWorkHabitData');
Route::post('jobseeker/add-referee-data','JobseekerController@addReferee');
Route::get('jobseeker/{id}/delete-referee-data','JobseekerController@deleteReferee');
Route::get('jobseeker/profile','HomeController@profile');

Route::get('employer/update-data','EmployerController@showUpdateData');
Route::post('employer/update-data','EmployerController@saveData');
Route::get('employer/{id}/jobseeker-details','EmployerController@jobseeker');
Route::get('employer/save-logo','EmployerController@saveEmployerLogo');

//Route::get('search','SearchController@index');
Route::post('search','SearchController@findJobseekers');

//POSTS
Route::post('post/new', 'PostController@addPost');
Route::get('posts/newsfeed', 'PostController@newsfeed');
Route::post('posts/addcomment', 'PostController@addComment');
Route::get('posts/comments/{id}', 'PostController@getComments');

//Route::auth();
//Authentications
Route::post('/join','UserAuthController@join');
Route::post('/login','UserAuthController@signIn');
Route::get('/logout','UserAuthController@logOut');

//Password Reset

Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('profile/settings/{flag?}','UserController@profileSettings');
Route::post('profile/save-data','UserController@updateUserData');
Route::post('profile/change-password','UserController@changePassword');
Route::post('profile/save-privacy','UserController@updatePrivacy');
Route::post('profile/save-notification-settings','UserController@updateNotifications');
Route::post('profile/delete-account','UserController@deleteAccount');

Route::get('{username}/connections','UserController@userConnections');
Route::get('{username}','UserController@profile');

Route::get('/university','UniversitiesController@index');
Route::post('/university/fetch','UniversitiesController@fetch')->name('university.fetch');
Route::post('/skill/fetch','listofSkillsController@fetch')->name('skill.fetch');
Route::post('/job/fetch','listofjobsController@fetch')->name('job.fetch');
