<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public static function add($key,$value)
    {
        $option = new Option();
        $option->option_key = $key;
        $option->option_value = $value;
        $option->save();
    }
    public static function get($key)
    {
        $count = self::where('option_key',$key)->count();
        if(!$count == 1)
        {
            return "";
        }
        $option = self::where('option_key',$key)->first();
        return $option->option_value;
    }
    public static function set($key,$value)
    {
        $op = self::where('option_key',$key)->first();
        $option = Option::find($op->id);
        $option->option_value = $value;
        $option->save();
    }
    public static function remove($key)
    {
        $option = self::where('option_key',$key);
        $option->delete();
    }

}
