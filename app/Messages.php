<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Messages extends Model {

	protected $table = 'messages';
    protected $fillable = ['*'];

    protected $dates = ['created_at','updated_at'];

    protected function getLastMsg($receiver)
    {
        $message = self::where('sender_id',$receiver)
            ->where('receiver_id',Auth::user()->id)
            ->orWhere(function($query) use ($receiver){
                $query->where('receiver_id',$receiver)
                    ->where('sender_id',Auth::user()->id);
            })
            ->orderBy('id','desc')
            ->first();
        return $message;
    }
    public static function getLastMessage($receiver)
    {
        return (new Messages())->getLastMsg($receiver)->content;
    }
    public static function getLastMessagetime($receiver)
    {
        return (new Messages())->getLastMsg($receiver)->created_at->toDateTimeString();
    }
    public static function hasLastMessage($receiver)
    {
        $count = self::where('sender_id',$receiver)
            ->where('receiver_id',Auth::user()->id)
            ->orWhere(function($query) use ($receiver){
                $query->where('receiver_id',$receiver)
                    ->where('sender_id',Auth::user()->id);
            })
            ->join('users','messages.sender_id','=','users.id')
            ->select('users.*','messages.sender_id','messages.receiver_id','messages.content','messages.created_at')
            ->orderBy('messages.id','desc')
            ->count();
        if($count > 0)
        {
            return true;
        }
    }
    public static function getTotalMessageCount()
    {
        return count(Messages::all());
    }
    public static function getTotalMessageToday()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Messages::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 1)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalMessageThisWeek()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Messages::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 7)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalMessageThisMonth()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Messages::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 30)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function getTotalMessageThisYear()
    {
        $counter = 0;
        $d1 = new \DateTime("now");
        $posts = Messages::all();
        $interval = 0;
        foreach ($posts as $post)
        {
            $d2 = new \DateTime($post->created_at->toDayDateTimeString());
            $interval = $d1->diff($d2);
            if($interval->days <= 365)
            {
                $counter++;
            }
        }
        return $counter;
    }
    public static function markAsRead($user1,$user2)
    {
        $conversation= Messages::where('sender_id',$user1)
            ->where('receiver_id',$user2)
            ->where('read','no')
            /*->orWhere(function($query) use ($user1,$user2){
                $query->where('receiver_id',$user1)
                    ->where('sender_id',$user2)
                    ->where('read','no');
            })*/
            ->get();
        foreach ($conversation as $msg)
        {
            $msg = self::find($msg->id);
            $msg->read = 'yes';
            $msg->save();
        }
    }
	public static function markAsDelivered($userId=0){
		if($userId ==0){
			$userId = Auth::user()->id;
		}
		$conversations = Messages::where('receiver_id',$userId)
		->where('delivered','no')
		->get();
		foreach($conversations as $conv){
			$conv = self::find($conv->id);
			$conv->delivered = 'yes';
			$conv->save();
		}
	}
}
