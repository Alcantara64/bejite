<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Nonprofit extends Model {

	protected $table = 'nonprofits';
    protected $fillable = ['*'];
    protected $hidden = ['id'];

}
