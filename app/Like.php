<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['post_id','type','user_id'];

    public static function hasLiked($postId,$userId=0)
    {
        if($userId == 0)
        {
            $userId = Auth::user()->id;
        }
        $count = self::where('post_id',$postId)
            ->where('user_id',$userId)
            ->count();
        if($count == 1){
            return true;
        }
    }

}
